<?php

return [
    'current' => '<li class="page-item active"><a class="active page-link" href="{{url}}">{{text}}</a></li>',
    'first' => '<li class="page-item"><a class="{{class}} page-link" href="{{url}}">{{text}}</a></li>',
    'last' => '<li class="page-item"><a class="{{class}} page-link" href="{{url}}">{{text}}</a></li>',
    'number' => '<li class="page-item"><a class="{{class}} page-link" href="{{url}}">{{text}}</a></li>',
];