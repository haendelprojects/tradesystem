module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {

            images: {
                files: ['img/src/**/*.{png,jpg,gif}'],
                tasks: ['newer:imagemin']
            }, // watch images added to src

            deleting: {
                files: ['img/src/*.{png,jpg,gif}'],
                tasks: ['delete_sync']
            }, // end of delete sync

            scripts: {
                files: ['js/libs/*.js', 'js/src/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    spawn: false,
                }
            }, //end of watch scripts

            css: {
                files: ['css/sass/**/*.scss'],
                tasks: ['sass'],
                options: {
                    spawn: false,
                }
            }, //end of sass watch

            grunt: {
                files: ['gruntfile.js']
            }
        }, //end of watch

        /* ====================================================================================================================================================
         * ====================================================================================================================================================
         Tasks
         ====================================================================================================================================================
         ====================================================================================================================================================
         */

        delete_sync: {
            dist: {
                cwd: 'img/dist',
                src: ['**'],
                syncWith: 'img/src'
            }
        }, // end of delete sync

        imagemin: {
            dynamic: {
                files: [{
                    expand: true, // Enable dynamic expansion
                    cwd: 'img/src/', // source images (not compressed)
                    src: ['**/*.{png,jpg,gif}'], // Actual patterns to match
                    dest: 'img/dist/' // Destination of compressed files
                }]
            }
        }, //end imagemin

        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                '<%= grunt.template.today("yyyy-mm-dd") %> */',
            },
            dist: {
                src: ['js/src/*.js'],
                dest: 'js/build/production.js'
            }
        }, //end concat

        uglify: {
            dist: {
                src: 'js/build/production.js',
                dest: 'js/build/production.min.js'
            }
        }, //end uglify

        sass: {
            dist: {
                options: {
                    //style: 'nested', //no need for config.rb
                    compass: 'true'
                },
                files: {
                    'css/build/main.css': 'css/sass/main.scss'
                }
            }
        }, //end of sass
        bower_concat: {
            all: {
                dest: {
                    'js': 'js/build/libs.js',
                    'css': 'css/build/libs.css'
                },
                exclude: [
                    'jquery',
                    'modernizr'
                ],
                dependencies: {
                    'underscore': 'jquery',
                    'backbone': 'underscore',
                    'jquery-mousewheel': 'jquery'
                },
                bowerOptions: {
                    relative: false
                }
            }
        }
    });

    // load npm tasks
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-delete-sync');
    grunt.loadNpmTasks('grunt-penthouse');
    grunt.loadNpmTasks('grunt-bower-concat');

    // define default task
    grunt.registerTask('default', ["bower_concat", "watch"]);
};