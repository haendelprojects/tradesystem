/*
 *
 * QuantumPro
 * Author: http://authenticgoods.co
 *
 */
// -----------------------------------------------------------------------------
// 1. GLOBAL CONSTANTS
// -----------------------------------------------------------------------------
(function (window, document, $, undefined) {
    "use strict";
    var QuantumPro = window.QuantumPro || (window.QuantumPro = {});
    if (Modernizr.touchevents) {
        QuantumPro.APP_TOUCH = true;
    } else {
        QuantumPro.APP_TOUCH = false;
    }
    QuantumPro.APP_MEDIAQUERY = {
        XLARGE: "1280px",
        LARGE: "992px",
        MEDIUM: "768px",
        SMALL: "576px",
        XSMALL: "0px"
    };
    QuantumPro.APP_COLORS = {
        primary: "#7468bd",
        secondary: "#8da6c3",
        accent: "#F64A91",
        info: "#42a4f4",
        warning: "#FFCE67",
        danger: "#ff5c75",
        success: "#2fbfa0",
        grey50: "#f0f6ff",
        grey100: "#dde9f5",
        grey200: "#cbdaea",
        grey300: "#b6cade",
        grey400: "#a4bad1",
        grey500: "#93acc6",
        grey600: "#839bb3",
        grey700: "#718599",
        grey800: "#617182",
        grey900: "#4d5a68"
    };

    // Option to persist settings
    // ----------------------------------
    var persistSettings = true;
    var $html = $("html"),
        $body = $("body");
    if (persistSettings) {
        //Setup some default layout options on app start.
        //Let's check if localStorage is available and persist our settings,
        if (typeof localStorage !== "undefined") {
            //Global namespace for sessionStorage,localStorage, and cookieStorage
            window.appConfig = Storages.initNamespaceStorage("appConfig");
        }
    }
    window.app = {
        persist: persistSettings,
        config: {
            isTouch: function isTouch() {
                return $html.hasClass("touch");
            }
        }
    };

    // debounce
    // --------------------
    window.debounce = function (func, wait, immediate) {
        var timeout;
        return function () {
            var context = this,
                args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };


    // Switching theme color demo
    // ----------------------------------------

    $('[data-load-css]').on('click', function (e) {
        var element = $(this);
        if (element.is('a'))
            e.preventDefault();
        var uri = element.data('loadCss'),
            link;
        if (uri) {
            link = swapStyleSheet(uri);
            if (!link) {
                $.error('Error creating stylesheet link element.');
            }
        } else {
            $.error('No stylesheet location defined.');
        }
    });

    var swapStyleSheet = function (uri) {
        var linkId = 'autoloaded-stylesheet',
            oldLink = $('#' + linkId).attr('id', linkId + '-old');
        $('head').append($('<link/>').attr({
            'id': linkId,
            'rel': 'stylesheet',
            'href': uri
        }));
        if (oldLink.length) {
            oldLink.remove();
        }
        return $('#' + linkId);
    }

    // Switching layout mode demo
    // ----------------------------------------

    $('input[name=layoutMode]').on('click', function () {
        if ($('body').hasClass('layout-fixed')) {
            $('body').removeClass('layout-fixed');
        }
        let getVal = $(this).val();
        $('body').addClass(getVal);
    });


    var $openCtrl = $('#button-search'),
        $closeCtrl = $('#button-search-close'),
        $searchContainer = $('.fullpage-search-wrapper'),
        $inputSearch = $('.search__input');

    function init() {
        initEvents();
    }

    function initEvents() {

        $openCtrl.on('click', function () {
            openSearch();
        });
        $closeCtrl.on('click', function () {
            closeSearch();
        });
        $(document).on('keyup', function (ev) {
            // escape key.
            if (ev.keyCode == 27) {
                closeSearch();
            }
        });
    }

    function openSearch() {
        $searchContainer.addClass('search--open');
        $inputSearch.focus();
    }

    function closeSearch() {
        $searchContainer.removeClass('search--open');
        $inputSearch.blur();
        $inputSearch.val('');
    }

    init();


})(window, document, window.jQuery);


// -----------------------------------------------------------------------------
// 2. App Sidebars
// -----------------------------------------------------------------------------

(function (window, document, $, undefined) {
    "use strict";
    $(function () {
        // init sidebars
        // --------------------
        $(".nav.metismenu").metisMenu();
        $("#navbar-nav").metisMenu();
        // switch sidebar state mobile/desktop based on breakpoints
        // ----------------------------------------------------------
        var switchMenuState = function () {
            var $body = $("body"),
                $menuHeaderControls = $(".header-controls");
            if ($(window).width() < 992) {
                $body.removeClass("mini-sidebar");
                $menuHeaderControls.hide();
            } else if ($(window).width() > 992) {
                $body.removeClass("aside-left-open");
                $menuHeaderControls.show();
            }
        };
        $(window).on("resize", function () {
            debounce(switchMenuState, 300, false)();
        });

        // Toggle menu states
        // ----------------------------------
        var $toggleElement = $("[data-toggle-state]");
        $("[data-toggle-state]").on("click", function (e) {
            e.stopPropagation();
            var $body = $("body"),
                element = $(this),
                className = element.data("toggleState"),
                //key = element.data('key'),
                $target = $body;
            if (className) {
                if ($target.hasClass(className)) {
                    $target.removeClass(className);
                } else {
                    $target.addClass(className);
                }
            }
            menuIconState(className);
            backdropState(className);
        });

        // Toggle menu icon on Default Menu open/close
        // ----------------------------------
        var menuIconState = function (className) {
            if (className === "mini-sidebar") {
                if ($("body.mini-sidebar").length > 0) {
                    $('[data-toggle-state="mini-sidebar"] > i')
                        .removeClass("la-dot-circle-o")
                        .addClass("la-circle");
                } else {
                    $('[data-toggle-state="mini-sidebar"] > i')
                        .removeClass("la-circle")
                        .addClass("la-dot-circle-o");
                }
            }
        };
        // Load backdrop when sidebar is open
        // ----------------------------------
        var backdropState = function (className) {
            var backDrop =
                '<div class="aside-overlay-fixed" data-aos="fade-in" data-aos-duration="300"></div>';
            if (
                $("body.aside-right-open").length > 0 ||
                $("body.aside-left-open").length > 0 ||
                $("body.mail-compose-open").length > 0
            ) {
                $("body").append(backDrop);
                $(".aside-overlay-fixed").on("click", function () {
                    $(this)
                        .fadeOut("fast")
                        .remove();
                    $("body").removeClass(
                        "aside-right-open aside-left-open mail-compose-open"
                    );
                });
            }
        };
    });
})(window, document, window.jQuery);

// -----------------------------------------------------------------------------
// 3. General actions
// -----------------------------------------------------------------------------

(function (window, document, $, undefined) {
    "use strict";
    $(function () {
        $("[data-q-action]").on("click", function (e) {
            e.stopPropagation();
            var $this = $(this),
                action = $(this).data("q-action"),
                $card = $(this).parents(".card");

            switch (action) {
                /*-----------------------------------------------------------------------
                Site Search Open/Close
                -----------------------------------------------------------------------*/
                /**
                 * Action: Open
                 **/
                case "open-site-search":
                    var $target = $(".top-toolbar.navbar-desktop .navbar-form");
                    $target.find(".navbar-search").focus();
                    $target.addClass("open");

                    break;

                /**
                 * Action: Close
                 **/
                case "close-site-search":
                    var $target = $(".top-toolbar .navbar-form");
                    $target.find(".navbar-search").val("");
                    $target.removeClass("open");

                    break;

                /*-----------------------------------------------------------------------
                Notifications Open/Close Config
                -----------------------------------------------------------------------*/
                /**
                 * Action: Open
                 **/
                case "open-notifi-config":
                    $this
                        .children(".icon")
                        .toggleClass("dripicons-gear dripicons-arrow-thin-left");
                    $this.data("q-action", "close-notifi-config");
                    var $target = $this
                        .closest(".card")
                        .find(".card-container-wrapper .card-container");

                    $target.animate({
                            left: "-300px"
                        },
                        400
                    );

                    break;

                /**
                 * Action: Close
                 **/
                case "close-notifi-config":
                    $this
                        .children(".icon")
                        .toggleClass("dripicons-arrow-thin-left dripicons-gear");
                    $this.data("q-action", "open-notifi-config");
                    var $target = $this
                        .closest(".card")
                        .find(".card-container-wrapper .card-container");

                    $target.animate({
                            left: "0px"
                        },
                        400
                    );

                    break;

                /*-----------------------------------------------------------------------
                Template Page Sidebars
                -----------------------------------------------------------------------*/
                /**
                 * Action: Open/Close
                 **/
                case "page-aside-right-open":
                    if (
                        Modernizr.mq("(max-width: " + QuantumPro.APP_MEDIAQUERY.XLARGE + ")")
                    ) {
                        var $target = $(".aside-right"),
                            $backdropTarget = $("body");

                        $target.toggleClass("open");

                        var addClickEvent = function () {
                            $(".aside-overlay-fixed").on("touchend click", function (e) {
                                $(this)
                                    .fadeOut("fast")
                                    .remove();
                                $target.removeClass("open");
                                return false;
                            });
                        };
                        if ($target.hasClass("open")) {
                            var backDrop =
                                '<div class="aside-overlay-fixed" data-aos="fade-in" data-aos-duration="300"></div>';
                            $backdropTarget.append(backDrop);
                            addClickEvent();
                        } else {
                            $(".aside-overlay-fixed").trigger("click");
                        }
                    }

                    break;

                /**
                 * Action: Open/Close
                 **/
                case "page-aside-left-open":
                    if (Modernizr.mq("(max-width:992px)")) {
                        var $target = $(".aside-left"),
                            $backdropTarget = $("body");

                        $target.toggleClass("open");

                        var addClickEvent = function () {
                            $(".aside-overlay-fixed").on("touchend click", function (e) {
                                $(this)
                                    .fadeOut("fast")
                                    .remove();
                                $target.removeClass("open");
                                return false;
                            });
                        };

                        if ($target.hasClass("open")) {
                            var backDrop =
                                '<div class="aside-overlay-fixed" data-aos="fade-in" data-aos-duration="300"></div>';
                            $backdropTarget.append(backDrop);
                            addClickEvent();
                        } else {
                            $(".aside-overlay-fixed").trigger("click");
                        }
                    }

                    break;

                /*-----------------------------------------------------------------------
                Card Actions
                -----------------------------------------------------------------------*/
                /**
                 * Action: Expand Fullscreen
                 **/
                case "card-expand":
                    $card.toggleClass(action);
                    $this
                        .children("i")
                        .toggleClass("dripicons-expand-2 icon dripicons-contract-2");
                    break;
                /**
                 * Action: Remove
                 **/
                case "card-remove":
                    $card.fadeOut();
                    break;
                /**
                 * Action: Collapse
                 **/
                case "card-collapsed":
                    $card.toggleClass(action);
                    $card.find(".card-body").slideToggle();
                    $this.children("i").toggleClass("collapsedRotate");
                    break;
            }
        });
    });
})(window, document, window.jQuery);
//----some cleanup checks
var removeBackdrop = function () {
    if ($("#3-col-wrapper").length) {
        var $targetRight = $("#3-col-wrapper .aside-right");
        ($targetLeft = $("#3-col-wrapper .aside-left")),
            ($backdropTarget = $("#3-col-wrapper"));
        if (
            $targetRight.hasClass("open") &&
            Modernizr.mq("(min-width: " + QuantumPro.APP_MEDIAQUERY.XLARGE + ")")
        ) {
            $(".aside-overlay-absolute").remove();
            $targetRight.removeClass("open");
        }
        if ($targetLeft.hasClass("open") && Modernizr.mq("(max-width: 670px)")) {
            $(".aside-overlay-absolute").remove();
            $targetLeft.removeClass("open");
        }
    }
};
$(window).on("resize", function () {
    debounce(removeBackdrop, 300, false)();
});
//----end cleanup checks
// -----------------------------------------------------------------------------
// 4. UI Blocking
// -----------------------------------------------------------------------------

(function (window, document, $, undefined) {
    "use strict";
    $(function () {
        $("body").on("click", "[data-qt-block]", function (e) {
            e.stopPropagation();
            var $this = $(this),
                target = $(this).data("qt-block");

            switch (target) {
                /*-----------------------------------------------------------------------
                Element blocking
                -----------------------------------------------------------------------*/
                /**
                 * Action: Open
                 **/
                case ".block-el":
                    var $blockTarget = $(this)
                        .parents(".card")
                        .find(target);
                    $blockTarget.append('<div class="qt-block-ui"></div>');
                    var $block = $blockTarget.find(".qt-block-ui");
                    setTimeout(function () {
                        $block.fadeOut("3000", function () {
                            $block.remove();
                        });
                    }, 1700);

                    break;
                /*-----------------------------------------------------------------------
                Form blocking
                -----------------------------------------------------------------------*/
                case "#block-form":
                    $(target).submit(function (event) {
                        event.preventDefault();
                        $(target)
                            .find("input")
                            .attr("disabled", "disabled");
                        $this.addClass("btn-block-ui").attr("disabled", "disabled");
                        setTimeout(function () {
                            $(target)
                                .find("input")
                                .attr("disabled", false)
                                .val("");
                            $this.removeClass("btn-block-ui").attr("disabled", false);
                        }, 3000);
                        return true;
                    });

                    break;
                /*-----------------------------------------------------------------------
                Full Page Blocking
                -----------------------------------------------------------------------*/
                case "body":
                    $(target).append('<div class="qt-block-ui"></div>');
                    var $block = $(".qt-block-ui");
                    setTimeout(function () {
                        $block.fadeOut("10000", function () {
                            $block.remove();
                        });
                    }, 1700);

                    break;
            }
        });
    });
})(window, document, window.jQuery);

// -----------------------------------------------------------------------------
// 5. GLOBAL INIT SNIPPETS
// -----------------------------------------------------------------------------

(function (window, document, $) {
    "use strict";
    $(function () {
        // Smooth Scroll
        // ----------------------------------
        if ($('a.smooth-scroll[href*="#"]:not([href="#"])').length > 0) {
            $('a.smooth-scroll[href*="#"]:not([href="#"])').click(function () {
                if (
                    location.pathname.replace(/^\//, "") ==
                    this.pathname.replace(/^\//, "") &&
                    location.hostname == this.hostname
                ) {
                    var target = $(this.hash);
                    target = target.length ?
                        target :
                        $("[name=" + this.hash.slice(1) + "]");
                    if (target.length) {
                        $("html, body").animate({
                                scrollTop: target.offset().top
                            },
                            1000
                        );
                        return false;
                    }
                }
            });
        }

        // Filter Toolbar Dropdown Menu
        // ----------------------------------
        if ($(".filter-input").length > 0) {
            var $filterInput = $(".filter-input"),
                $filterList = $("ul.filter-list li a.dropdown-item"),
                $clearList = $(".clear-filter");
            $clearList.hide();
            $filterInput.on("keyup", function () {
                var value = $(this)
                    .val()
                    .toLowerCase();
                $filterList.filter(function () {
                    $(this).toggle(
                        $(this)
                            .text()
                            .toLowerCase()
                            .indexOf(value) > -1
                    );
                    $clearList.show();
                    if (!$filterInput.val().length) {
                        $clearList.hide();
                    }
                });
            });

            $clearList.on("click", function () {
                $(this).hide();
                $filterInput.val("");
                $filterList.fadeIn();
            });
        }

        // Custom Scrollbar
        // ----------------------------------
        if ($("[data-scroll='minimal-dark']").length > 0 && $("[data-scroll='minimal-dark']").hasClass('scroll-bottom')) {
            $("[data-scroll='minimal-dark']").mCustomScrollbar({
                theme: "minimal-dark",
                scrollInertia: 100,
                setTop: "-999999px",
                mouseWheel: {
                    preventDefault: true
                }
            });
        } else if ($("[data-scroll='minimal-dark']").length > 0) {
            $("[data-scroll='minimal-dark']").mCustomScrollbar({
                theme: "minimal-dark",
                scrollInertia: 100,
                mouseWheel: {
                    preventDefault: true
                }
            });
        }
        if ($("[data-scroll='minimal']").length > 0) {
            $("[data-scroll='minimal']").mCustomScrollbar({
                theme: "minimal",
                scrollInertia: 100,
                mouseWheel: {
                    preventDefault: true
                }
            });
        }
        if ($("[data-scroll='minimal-light']").length > 0) {
            $("[data-scroll='minimal-light']").mCustomScrollbar({
                theme: "minimal-light",
                scrollInertia: 100,
                mouseWheel: {
                    preventDefault: true
                }
            });
        }
        // Modal Custom Scrollbar
        // ----------------------------------
        if ($('[data-modal="scroll"]').length > 0) {
            $('[data-modal="scroll"]').on("shown.bs.modal", function () {
                $(".modal-body").mCustomScrollbar({
                    theme: "minimal-dark"
                });
            });
        }

        // Initialize the switchery plugin
        // ----------------------------------
        if ($(".js-switch").length > 0) {
            var elems = Array.prototype.slice.call($(".js-switch"));
            elems.forEach(function (html) {
                var switchery = new Switchery(html, {
                    size: "small",
                    color: QuantumPro.APP_COLORS.primary,
                    secondaryColor: QuantumPro.APP_COLORS.lightergrey
                });
            });
        }

        // Keep the dropdowns open when clicking switches
        // ------------------------------------------------
        if ($(".switchery").length > 0) {
            $(".switchery").on("click", function (e) {
                e.stopPropagation();
            });
        }

        // Dropdown menu animation
        // ------------------------------------------------
        if ($(".dropdown").length > 0) {
            $(".dropdown").on("hidden.bs.dropdown", function () {
                $(this)
                    .find(".dropdown-menu")
                    .removeAttr("style");
            });
        }

        // Tooltip init
        // ------------------------------------------------
        if ($('[data-toggle="tooltip"]').length > 0) {
            $('[data-toggle="tooltip"]').tooltip();
        }
        // Reset Form
        // ------------------------------------------------
        $(".clear-form").on("click", function () {
            $(this)
                .closest("form")
                .find(":input")
                .val("");
            $(this)
                .closest("form")
                .find(":checkbox")
                .prop("checked", false);
            $(this)
                .closest("form")
                .find(":radio")
                .prop("checked", false);
        });
        // Popover init
        // ------------------------------------------------
        if ($('[data-toggle="popover"]').length > 0) {
            $('[data-toggle="popover"]').popover({
                container: "body",
                html: true,
                content: function () {
                    return $(this)
                        .next(".popper-content")
                        .html();
                }
            });
        }

        // Auto Hide Menu Option for Horizontal Menu
        // ------------------------------------------------
        if ($("body.layout-horizontal.menu-auto-hide").length > 0) {
            // scroll is still position
            var scroll = $(document).scrollTop();
            var headerHeight = $('.header-bottom').outerHeight();
            //console.log(headerHeight);

            $(window).scroll(function () {
                // scrolled is new position just obtained
                var scrolled = $(document).scrollTop();

                // optionally emulate non-fixed positioning behaviour

                if (scrolled > headerHeight) {
                    $('.header-bottom').addClass('off-canvas');
                } else {
                    $('.header-bottom').removeClass('off-canvas');
                }

                if (scrolled > scroll) {
                    // scrolling down
                    $('.header-bottom').removeClass('fixed');
                } else {
                    //scrolling up
                    $('.header-bottom').addClass('fixed');
                }

                scroll = $(document).scrollTop();
            });
        }

        // Checar e desmarcar todos os check box da pagina
        // ------------------------------------------------
        $('.check-all').on('click', function () {
            $('.checkbox').attr('checked', true);
        });

        $('.uncheck-all').on('click', function () {
            $('.checkbox').attr('checked', false);
        });

        // Configuração do editor de texto
        // ------------------------------------------------
        $('.editor').trumbowyg({
            'svgPath': '/img/icons.svg'
        });

        // Configuração de datepicer
        // ------------------------------------------------
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            orientation: "top auto",
            autoclose: true,
            todayHighlight: true
        });


        $('.date').mask('00/00/0000');
        $('.time').mask('00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0,00", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.sp_celphones').mask(SPMaskBehavior, spOptions);

        // Modal Change Log
        // ------------------------------------------------
        $('#changelogModal').modal('show');

        /**
         * Converte numeros vindos de inputs para float para que possam ser somados d eformado correta
         * @param val
         * @returns {*}
         */
        function convertValueToFloat(val) {
            return (isNaN(parseFloat(val))) ? 0 : parseFloat(val);
        }

        // Setar links como ativo
        // -------------------------------------------------
        // var url = window.location.pathname; //sets the variable "url" to the pathname of the current window
        // var activePage = url.substring(url.lastIndexOf('/') + 1); //sets the variable "activePage" as the substring after the last "/" in the "url" variable
        // $('a').each(function () { //looks in each link item within the primary-nav list
        //     var linkPage = this.href.substring(this.href.lastIndexOf('/') + 1); //sets the variable "linkPage" as the substring of the url path in each &lt;a&gt;
        //     if (activePage === linkPage) { //compares the path of the current window to the path of the linked page in the nav item
        //         $(this).addClass('active'); //if the above is true, add the "active" class to the parent of the &lt;a&gt; which is the &lt;li&gt; in the nav list
        //     }
        // });
    });

    // Upload de arquivos
    // ------------------------------------------------
    $('#value-plan').on('change keyup', function () {
        var value = $(this).val();
        $('.procedure').each(function () {
            var useValue = $(this).attr('data-use-value');
            $(this).find('.procedure-price').val(value * useValue);
        });
    });


    function calculateInvestPerBtc() {
        var valInvest = $('.invest').val();
        var valValueBtc = $('.value-btc').val();
        var total = (valInvest / valValueBtc).toFixed(8);
        $('.result-invest-btc').val(total);
    }

    $('.invest').on('change keyup', calculateInvestPerBtc);
    $('.value-btc').on('change keyup', calculateInvestPerBtc)


    $('.value-dolar-btc').on('change keyup', function(){
        var total = $('.last-operation').val() / $(this).val();
        $('.result-invest-btc').val(total.toFixed(8));
    })
})(window, document, window.jQuery);