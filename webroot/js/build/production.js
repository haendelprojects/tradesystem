/*! findup-app - v - 2018-06-04 */(function ($) {
    "use strict";


    var inputs = {
        street: $('.street'),
        city: $('.city'),
        state: $('.state'),
        neighborhood: $('.neighborhood'),
        zipcode: $('.zipcode'),
        country: $('.country')
    }

    var boxSearchAddress = $('.box-search-address');

    var btnGetByZipcode = $('.search-zipcode');

    btnGetByZipcode.on('click', function () {
        boxSearchAddress.LoadingOverlay("show");
        $.get("https://api.postmon.com.br/v1/cep/" + inputs.zipcode.val() + "?format=json ",
            function (result) {
                inputs.neighborhood.val(result.bairro);
                inputs.street.val(result.logradouro);

                inputs.state
                    .children('option')
                    .each(function (o) {
                        if ($(this).html() == result.estado_info.nome) {
                            inputs.state.val($(this).val());
                        }
                    });
                $.get('/rest/addresses/selectCitiesByStateName/' + result.estado, function (res) {
                    inputs.city.empty();
                    inputs.city.append(res);
                    inputs.city
                        .children('option')
                        .each(function (o) {
                            if ($(this).html() == result.cidade) {
                                inputs.city.val($(this).val());
                            }
                        });
                    boxSearchAddress.LoadingOverlay("hide");
                });
            }).fail(function () {
                alert('Servidor dos correios fora do ar, abra sem buscar CEP.');
                boxSearchAddress.LoadingOverlay("hide");
            });
        })


    /**
     * Se tiver pais
     */
    inputs.country.on('change', function () {
        var url = '/rest/addresses/states/' + $(this).val();
        boxSearchAddress.LoadingOverlay("show");
        $.get(url, function (data) {
            inputs.state.empty();
            inputs.city.empty();
            inputs.state.append(data);
            boxSearchAddress.LoadingOverlay("hide");
        });
    });

    /**
     * Se tiver estado
     */
    inputs.state.on('change', function () {
        var url = '/rest/addresses/cities/' + $(this).val();
        boxSearchAddress.LoadingOverlay("show");
        $.get(url, function (data) {
            inputs.city.empty();
            inputs.city.append(data);
            boxSearchAddress.LoadingOverlay("hide");
        });
    });
})(jQuery);

/*! AdminLTE app.js
 * ================
 * Main JS application file for AdminLTE v2. This file
 * should be included in all pages. It controls some layout
 * options and implements exclusive AdminLTE plugins.
 *
 * @Author  Almsaeed Studio
 * @Support <http://www.almsaeedstudio.com>
 * @Email   <abdullah@almsaeedstudio.com>
 * @version 2.3.8
 * @license MIT <http://opensource.org/licenses/MIT>
 */

//Make sure jQuery has been loaded before app.js
if (typeof jQuery === "undefined") {
    throw new Error("AdminLTE requires jQuery");
}

/* AdminLTE
 *
 * @type Object
 * @description $.AdminLTE is the main object for the template's app.
 *              It's used for implementing functions and options related
 *              to the template. Keeping everything wrapped in an object
 *              prevents conflict with other plugins and is a better
 *              way to organize our code.
 */
$.AdminLTE = {};

/* --------------------
 * - AdminLTE Options -
 * --------------------
 * Modify these options to suit your implementation
 */
$.AdminLTE.options = {
    //Add slimscroll to navbar menus
    //This requires you to load the slimscroll plugin
    //in every page before app.js
    navbarMenuSlimscroll: true,
    navbarMenuSlimscrollWidth: "3px", //The width of the scroll bar
    navbarMenuHeight: "200px", //The height of the inner menu
    //General animation speed for JS animated elements such as box collapse/expand and
    //sidebar treeview slide up/down. This options accepts an integer as milliseconds,
    //'fast', 'normal', or 'slow'
    animationSpeed: 500,
    //Sidebar push menu toggle button selector
    sidebarToggleSelector: "[data-toggle='offcanvas']",
    //Activate sidebar push menu
    sidebarPushMenu: true,
    //Activate sidebar slimscroll if the fixed layout is set (requires SlimScroll Plugin)
    sidebarSlimScroll: true,
    //Enable sidebar expand on hover effect for sidebar mini
    //This option is forced to true if both the fixed layout and sidebar mini
    //are used together
    sidebarExpandOnHover: false,
    //BoxRefresh Plugin
    enableBoxRefresh: true,
    //Bootstrap.js tooltip
    enableBSToppltip: true,
    BSTooltipSelector: "[data-toggle='tooltip']",
    //Enable Fast Click. Fastclick.js creates a more
    //native touch experience with touch devices. If you
    //choose to enable the plugin, make sure you load the script
    //before AdminLTE's app.js
    enableFastclick: false,
    //Control Sidebar Tree views
    enableControlTreeView: true,
    //Control Sidebar Options
    enableControlSidebar: true,
    controlSidebarOptions: {
        //Which button should trigger the open/close event
        toggleBtnSelector: "[data-toggle='control-sidebar']",
        //The sidebar selector
        selector: ".control-sidebar",
        //Enable slide over content
        slide: true
    },
    //Box Widget Plugin. Enable this plugin
    //to allow boxes to be collapsed and/or removed
    enableBoxWidget: true,
    //Box Widget plugin options
    boxWidgetOptions: {
        boxWidgetIcons: {
            //Collapse icon
            collapse: 'fa-minus',
            //Open icon
            open: 'fa-plus',
            //Remove icon
            remove: 'fa-times'
        },
        boxWidgetSelectors: {
            //Remove button selector
            remove: '[data-widget="remove"]',
            //Collapse button selector
            collapse: '[data-widget="collapse"]'
        }
    },
    //Direct Chat plugin options
    directChat: {
        //Enable direct chat by default
        enable: true,
        //The button to open and close the chat contacts pane
        contactToggleSelector: '[data-widget="chat-pane-toggle"]'
    },
    //Define the set of colors to use globally around the website
    colors: {
        lightBlue: "#3c8dbc",
        red: "#f56954",
        green: "#00a65a",
        aqua: "#00c0ef",
        yellow: "#f39c12",
        blue: "#0073b7",
        navy: "#001F3F",
        teal: "#39CCCC",
        olive: "#3D9970",
        lime: "#01FF70",
        orange: "#FF851B",
        fuchsia: "#F012BE",
        purple: "#8E24AA",
        maroon: "#D81B60",
        black: "#222222",
        gray: "#d2d6de"
    },
    //The standard screen sizes that bootstrap uses.
    //If you change these in the variables.less file, change
    //them here too.
    screenSizes: {
        xs: 480,
        sm: 768,
        md: 992,
        lg: 1200
    }
};

/* ------------------
 * - Implementation -
 * ------------------
 * The next block of code implements AdminLTE's
 * functions and plugins as specified by the
 * options above.
 */
$(function () {
    "use strict";

    //Fix for IE page transitions
    $("body").removeClass("hold-transition");

    //Extend options if external options exist
    if (typeof AdminLTEOptions !== "undefined") {
        $.extend(true,
            $.AdminLTE.options,
            AdminLTEOptions);
    }

    //Easy access to options
    var o = $.AdminLTE.options;

    //Set up the object
    _init();

    //Activate the layout maker
    $.AdminLTE.layout.activate();

    //Enable sidebar tree view controls
    if (o.enableControlTreeView) {
        $.AdminLTE.tree('.sidebar');
    }

    //Enable control sidebar
    if (o.enableControlSidebar) {
        $.AdminLTE.controlSidebar.activate();
    }

    //Add slimscroll to navbar dropdown
    if (o.navbarMenuSlimscroll && typeof $.fn.slimscroll != 'undefined') {
        $(".navbar .menu").slimscroll({
            height: o.navbarMenuHeight,
            alwaysVisible: false,
            size: o.navbarMenuSlimscrollWidth
        }).css("width", "100%");
    }

    //Activate sidebar push menu
    if (o.sidebarPushMenu) {
        $.AdminLTE.pushMenu.activate(o.sidebarToggleSelector);
    }

    //Activate Bootstrap tooltip
    if (o.enableBSToppltip) {
        $('body').tooltip({
            selector: o.BSTooltipSelector,
            container: 'body'
        });
    }

    //Activate box widget
    if (o.enableBoxWidget) {
        $.AdminLTE.boxWidget.activate();
    }

    //Activate fast click
    if (o.enableFastclick && typeof FastClick != 'undefined') {
        FastClick.attach(document.body);
    }

    //Activate direct chat widget
    if (o.directChat.enable) {
        $(document).on('click', o.directChat.contactToggleSelector, function () {
            var box = $(this).parents('.direct-chat').first();
            box.toggleClass('direct-chat-contacts-open');
        });
    }

    /*
     * INITIALIZE BUTTON TOGGLE
     * ------------------------
     */
    $('.btn-group[data-toggle="btn-toggle"]').each(function () {
        var group = $(this);
        $(this).find(".btn").on('click', function (e) {
            group.find(".btn.active").removeClass("active");
            $(this).addClass("active");
            e.preventDefault();
        });

    });
});

/* ----------------------------------
 * - Initialize the AdminLTE Object -
 * ----------------------------------
 * All AdminLTE functions are implemented below.
 */
function _init() {
    'use strict';
    /* Layout
     * ======
     * Fixes the layout height in case min-height fails.
     *
     * @type Object
     * @usage $.AdminLTE.layout.activate()
     *        $.AdminLTE.layout.fix()
     *        $.AdminLTE.layout.fixSidebar()
     */
    $.AdminLTE.layout = {
        activate: function () {
            var _this = this;
            _this.fix();
            _this.fixSidebar();
            $('body, html, .wrapper').css('height', 'auto');
            $(window, ".wrapper").resize(function () {
                _this.fix();
                _this.fixSidebar();
            });
        },
        fix: function () {
            // Remove overflow from .wrapper if layout-boxed exists
            $(".layout-boxed > .wrapper").css('overflow', 'hidden');
            //Get window height and the wrapper height
            var footer_height = $('.main-footer').outerHeight() || 0;
            var neg = $('.main-header').outerHeight() + footer_height;
            var window_height = $(window).height();
            var sidebar_height = $(".sidebar").height() || 0;
            //Set the min-height of the content and sidebar based on the
            //the height of the document.
            if ($("body").hasClass("fixed")) {
                $(".content-wrapper, .right-side").css('min-height', window_height - footer_height);
            } else {
                var postSetWidth;
                if (window_height >= sidebar_height) {
                    $(".content-wrapper, .right-side").css('min-height', window_height - neg);
                    postSetWidth = window_height - neg;
                } else {
                    $(".content-wrapper, .right-side").css('min-height', sidebar_height);
                    postSetWidth = sidebar_height;
                }

                //Fix for the control sidebar height
                var controlSidebar = $($.AdminLTE.options.controlSidebarOptions.selector);
                if (typeof controlSidebar !== "undefined") {
                    if (controlSidebar.height() > postSetWidth)
                        $(".content-wrapper, .right-side").css('min-height', controlSidebar.height());
                }

            }
        },
        fixSidebar: function () {
            //Make sure the body tag has the .fixed class
            if (!$("body").hasClass("fixed")) {
                if (typeof $.fn.slimScroll != 'undefined') {
                    $(".sidebar").slimScroll({destroy: true}).height("auto");
                }
                return;
            } else if (typeof $.fn.slimScroll == 'undefined' && window.console) {
                window.console.error("Error: the fixed layout requires the slimscroll plugin!");
            }
            //Enable slimscroll for fixed layout
            if ($.AdminLTE.options.sidebarSlimScroll) {
                if (typeof $.fn.slimScroll != 'undefined') {
                    //Destroy if it exists
                    $(".sidebar").slimScroll({destroy: true}).height("auto");
                    //Add slimscroll
                    $(".sidebar").slimScroll({
                        height: ($(window).height() - $(".main-header").height()) + "px",
                        color: "rgba(0,0,0,0.2)",
                        size: "3px"
                    });
                }
            }
        }
    };

    /* PushMenu()
     * ==========
     * Adds the push menu functionality to the sidebar.
     *
     * @type Function
     * @usage: $.AdminLTE.pushMenu("[data-toggle='offcanvas']")
     */
    $.AdminLTE.pushMenu = {
        activate: function (toggleBtn) {
            //Get the screen sizes
            var screenSizes = $.AdminLTE.options.screenSizes;

            //Enable sidebar toggle
            $(document).on('click', toggleBtn, function (e) {
                e.preventDefault();

                //Enable sidebar push menu
                if ($(window).width() > (screenSizes.sm - 1)) {
                    if ($("body").hasClass('sidebar-collapse')) {
                        $("body").removeClass('sidebar-collapse').trigger('expanded.pushMenu');
                    } else {
                        $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    }
                }
                //Handle sidebar push menu for small screens
                else {
                    if ($("body").hasClass('sidebar-open')) {
                        $("body").removeClass('sidebar-open').removeClass('sidebar-collapse').trigger('collapsed.pushMenu');
                    } else {
                        $("body").addClass('sidebar-open').trigger('expanded.pushMenu');
                    }
                }
            });

            $(".content-wrapper").click(function () {
                //Enable hide menu when clicking on the content-wrapper on small screens
                if ($(window).width() <= (screenSizes.sm - 1) && $("body").hasClass("sidebar-open")) {
                    $("body").removeClass('sidebar-open');
                }
            });

            //Enable expand on hover for sidebar mini
            if ($.AdminLTE.options.sidebarExpandOnHover
                || ($('body').hasClass('fixed')
                    && $('body').hasClass('sidebar-mini'))) {
                this.expandOnHover();
            }
        },
        expandOnHover: function () {
            var _this = this;
            var screenWidth = $.AdminLTE.options.screenSizes.sm - 1;
            //Expand sidebar on hover
            $('.main-sidebar').hover(function () {
                if ($('body').hasClass('sidebar-mini')
                    && $("body").hasClass('sidebar-collapse')
                    && $(window).width() > screenWidth) {
                    _this.expand();
                }
            }, function () {
                if ($('body').hasClass('sidebar-mini')
                    && $('body').hasClass('sidebar-expanded-on-hover')
                    && $(window).width() > screenWidth) {
                    _this.collapse();
                }
            });
        },
        expand: function () {
            $("body").removeClass('sidebar-collapse').addClass('sidebar-expanded-on-hover');
        },
        collapse: function () {
            if ($('body').hasClass('sidebar-expanded-on-hover')) {
                $('body').removeClass('sidebar-expanded-on-hover').addClass('sidebar-collapse');
            }
        }
    };

    /* Tree()
     * ======
     * Converts the sidebar into a multilevel
     * tree view menu.
     *
     * @type Function
     * @Usage: $.AdminLTE.tree('.sidebar')
     */
    $.AdminLTE.tree = function (menu) {
        var _this = this;
        var animationSpeed = $.AdminLTE.options.animationSpeed;
        $(document).off('click', menu + ' li a')
            .on('click', menu + ' li a', function (e) {
                //Get the clicked link and the next element
                var $this = $(this);
                var checkElement = $this.next();

                //Check if the next element is a menu and is visible
                if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible')) && (!$('body').hasClass('sidebar-collapse'))) {
                    //Close the menu
                    checkElement.slideUp(animationSpeed, function () {
                        checkElement.removeClass('menu-open');
                        //Fix the layout in case the sidebar stretches over the height of the window
                        //_this.layout.fix();
                    });
                    checkElement.parent("li").removeClass("active");
                }
                //If the menu is not visible
                else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
                    //Get the parent menu
                    var parent = $this.parents('ul').first();
                    //Close all open menus within the parent
                    var ul = parent.find('ul:visible').slideUp(animationSpeed);
                    //Remove the menu-open class from the parent
                    ul.removeClass('menu-open');
                    //Get the parent li
                    var parent_li = $this.parent("li");

                    //Open the target menu and add the menu-open class
                    checkElement.slideDown(animationSpeed, function () {
                        //Add the class active to the parent li
                        checkElement.addClass('menu-open');
                        parent.find('li.active').removeClass('active');
                        parent_li.addClass('active');
                        //Fix the layout in case the sidebar stretches over the height of the window
                        _this.layout.fix();
                    });
                }
                //if this isn't a link, prevent the page from being redirected
                if (checkElement.is('.treeview-menu')) {
                    e.preventDefault();
                }
            });
    };

    /* ControlSidebar
     * ==============
     * Adds functionality to the right sidebar
     *
     * @type Object
     * @usage $.AdminLTE.controlSidebar.activate(options)
     */
    $.AdminLTE.controlSidebar = {
        //instantiate the object
        activate: function () {
            //Get the object
            var _this = this;
            //Update options
            var o = $.AdminLTE.options.controlSidebarOptions;
            //Get the sidebar
            var sidebar = $(o.selector);
            //The toggle button
            var btn = $(o.toggleBtnSelector);

            //Listen to the click event
            btn.on('click', function (e) {
                e.preventDefault();
                //If the sidebar is not open
                if (!sidebar.hasClass('control-sidebar-open')
                    && !$('body').hasClass('control-sidebar-open')) {
                    //Open the sidebar
                    _this.open(sidebar, o.slide);
                } else {
                    _this.close(sidebar, o.slide);
                }
            });

            //If the body has a boxed layout, fix the sidebar bg position
            var bg = $(".control-sidebar-bg");
            _this._fix(bg);

            //If the body has a fixed layout, make the control sidebar fixed
            if ($('body').hasClass('fixed')) {
                _this._fixForFixed(sidebar);
            } else {
                //If the content height is less than the sidebar's height, force max height
                if ($('.content-wrapper, .right-side').height() < sidebar.height()) {
                    _this._fixForContent(sidebar);
                }
            }
        },
        //Open the control sidebar
        open: function (sidebar, slide) {
            //Slide over content
            if (slide) {
                sidebar.addClass('control-sidebar-open');
            } else {
                //Push the content by adding the open class to the body instead
                //of the sidebar itself
                $('body').addClass('control-sidebar-open');
            }
        },
        //Close the control sidebar
        close: function (sidebar, slide) {
            if (slide) {
                sidebar.removeClass('control-sidebar-open');
            } else {
                $('body').removeClass('control-sidebar-open');
            }
        },
        _fix: function (sidebar) {
            var _this = this;
            if ($("body").hasClass('layout-boxed')) {
                sidebar.css('position', 'absolute');
                sidebar.height($(".wrapper").height());
                if (_this.hasBindedResize) {
                    return;
                }
                $(window).resize(function () {
                    _this._fix(sidebar);
                });
                _this.hasBindedResize = true;
            } else {
                sidebar.css({
                    'position': 'fixed',
                    'height': 'auto'
                });
            }
        },
        _fixForFixed: function (sidebar) {
            sidebar.css({
                'position': 'fixed',
                'max-height': '100%',
                'overflow': 'auto',
                'padding-bottom': '50px'
            });
        },
        _fixForContent: function (sidebar) {
            $(".content-wrapper, .right-side").css('min-height', sidebar.height());
        }
    };

    /* BoxWidget
     * =========
     * BoxWidget is a plugin to handle collapsing and
     * removing boxes from the screen.
     *
     * @type Object
     * @usage $.AdminLTE.boxWidget.activate()
     *        Set all your options in the main $.AdminLTE.options object
     */
    $.AdminLTE.boxWidget = {
        selectors: $.AdminLTE.options.boxWidgetOptions.boxWidgetSelectors,
        icons: $.AdminLTE.options.boxWidgetOptions.boxWidgetIcons,
        animationSpeed: $.AdminLTE.options.animationSpeed,
        activate: function (_box) {
            var _this = this;
            if (!_box) {
                _box = document; // activate all boxes per default
            }
            //Listen for collapse event triggers
            $(_box).on('click', _this.selectors.collapse, function (e) {
                e.preventDefault();
                _this.collapse($(this));
            });

            //Listen for remove event triggers
            $(_box).on('click', _this.selectors.remove, function (e) {
                e.preventDefault();
                _this.remove($(this));
            });
        },
        collapse: function (element) {
            var _this = this;
            //Find the box parent
            var box = element.parents(".box").first();
            //Find the body and the footer
            var box_content = box.find("> .box-body, > .box-footer, > form  >.box-body, > form > .box-footer");
            if (!box.hasClass("collapsed-box")) {
                //Convert minus into plus
                element.children(":first")
                    .removeClass(_this.icons.collapse)
                    .addClass(_this.icons.open);
                //Hide the content
                box_content.slideUp(_this.animationSpeed, function () {
                    box.addClass("collapsed-box");
                });
            } else {
                //Convert plus into minus
                element.children(":first")
                    .removeClass(_this.icons.open)
                    .addClass(_this.icons.collapse);
                //Show the content
                box_content.slideDown(_this.animationSpeed, function () {
                    box.removeClass("collapsed-box");
                });
            }
        },
        remove: function (element) {
            //Find the box parent
            var box = element.parents(".box").first();
            box.slideUp(this.animationSpeed);
        }
    };
}

(function ($) {
    'use strict';

    var timeOut = undefined;

    function initAtt() {
        var attCheck = window.localStorage.getItem('attCheck');
        clearTimeout(timeOut);
        if (attCheck) {
            var attTime = window.localStorage.getItem('attTime');
            if ($('.att-check').length) {
                $('.att-check').prop('checked', true);
                $('.att-time').val(attTime);
                timeOut = setTimeout(function () {
                    window.location.reload(1);
                }, attTime * 10000);
            }
        }
    }

    initAtt();

    $('.att-check').on('click', function () {
        if ($(this).prop('checked')) {
            window.localStorage.setItem('attCheck', true);
            window.localStorage.setItem('attTime', $('.att-time').val());
            initAtt();
        } else {
            window.localStorage.removeItem('attCheck');
            window.localStorage.removeItem('attTime');
        }
    });

    $('.att-time').on('change', function () {
        window.localStorage.removeItem('attTime');
        window.localStorage.setItem('attTime', $('.att-time').val());
        initAtt();
    })
})(jQuery);


(function ($) {
    'use strict';
    /**
     * Chart de filiais
     */
    var classChartAddressOccurrences = $('.chart-address-occurrences');
    if (classChartAddressOccurrences.length) {
        var ctxa = document.getElementById("idChartOccurrencesAddress").getContext('2d');
        var chartOccurrencesAddress = new Chart(ctxa, {
            type: 'bar',

            data: {
                labels: JSON.parse(classChartAddressOccurrences.attr('data-labels')),
                ids: JSON.parse(classChartAddressOccurrences.attr('data-ids')),
                datasets: [{
                    label: 'Total de ocorrências por filial',
                    data: JSON.parse(classChartAddressOccurrences.attr('data-values')),
                    backgroundColor: '#3c8dbc '
                }]
            },
            options: {
                legend: {display: false},
                scaleShowLabels: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return value.substr(0, 5);
                            }
                        },
                    }]
                }
            }
        });

        classChartAddressOccurrences.on('click', function (evt) {
            var activePoint = chartOccurrencesAddress.getElementAtEvent(evt);
            var val = chartOccurrencesAddress.data.ids[activePoint[0]._index];
            var win = window.open('/occurrences?filial=' + val, '_blank');
            win.focus();
        });
    }

    /**
     * Chart serviços
     * @type {void|jQuery|HTMLElement}
     */
    var classChartServices = $('.chart-services');
    if (classChartServices.length) {
        var ctxs = document.getElementById("idChartServices").getContext('2d');
        var chartServices = new Chart(ctxs, {
            type: 'bar',
            data: {
                labels: JSON.parse(classChartServices.attr('data-labels')),
                ids: JSON.parse(classChartServices.attr('data-ids')),
                datasets: [{
                    label: 'Total de ocorrências por serviço',
                    data: JSON.parse(classChartServices.attr('data-values')),
                    backgroundColor: '#2b89f8 '
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return value.substr(0, 5);
                            },
                            minor: {
                                callback: function (v) {
                                }
                            }
                        },
                        minor: {
                            callback: function (v) {
                            }
                        }
                    }]
                }
            }
        });

        classChartServices.on('click', function (evt) {
            var activePoint = chartServices.getElementAtEvent(evt);
            var val = chartServices.data.ids[activePoint[0]._index];
            var win = window.open('/occurrences?service_id=' + val, '_blank');
            win.focus();
        });
    }

    /**
     * Chart ups
     * @type {void|jQuery|HTMLElement}
     */
    var classChartUps = $('.chart-ups');
    if (classChartUps.length) {
        var ctxu = document.getElementById("idChartUps").getContext('2d');
        var chartUps = new Chart(ctxu, {
            type: 'bar',
            data: {
                labels: JSON.parse(classChartUps.attr('data-labels')),
                datasets: [{
                    label: 'Total de UPs no mês',
                    data: JSON.parse(classChartUps.attr('data-values')),
                    backgroundColor: '#42fbca '
                }]
            },
            options: {
                legend: {display: false},
                scaleShowLabels: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {display: false}
                    }],
                    xAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return value.substr(0, 5);
                            }
                        },
                    }]
                }
            }
        });

        classChartUps.on('click', function (evt) {
            var activePoint = chartUps.getElementAtEvent(evt);
            var val = chartUps.data.labels[activePoint[0]._index];
            var win = window.open('/occurrences?month=' + val, '_blank');
            win.focus();
        });
    }

    /**
     * Chart de validações dadas
     * @type {void|jQuery|HTMLElement}
     */

    var classChartValidation = $('.chart-validation');
    if (classChartValidation.length) {
        var ctxv = document.getElementById("idValidation").getContext('2d');
        var chartValidation = new Chart(ctxv, {
            type: 'doughnut',
            data: {
                labels: JSON.parse(classChartValidation.attr('data-labels')),
                datasets: [{
                    label: 'Total de UPs no mês',
                    data: JSON.parse(classChartValidation.attr('data-values')),
                    backgroundColor: ["#dedede", "#d5fb42", "#70a2b5", "#3c00ff", "#42b7fb", "#42fbca"]
                }]
            }
        });

        classChartValidation.on('click', function (evt) {
            var activePoint = chartValidation.getElementAtEvent(evt);
            var val = chartValidation.data.labels[activePoint[0]._index];
            var win = window.open('/occurrences?ratings=' + val, '_blank');
            win.focus();
        });
    }

})(jQuery);

(function ($) {
    'use strict';
    /**
     * Ações Gerais
     */
    $('.check-all').on('click', function () {
        $('.checkbox').attr('checked', true);
    });

    $('.uncheck-all').on('click', function () {
        $('.checkbox').attr('checked', false);
    });

    /**
     * Campo de data
     */
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR",
        orientation: "top auto",
        autoclose: true,
        todayHighlight: true
    });


    // Firebase

    // Initialize Firebase
    var firebaseConfig = {
        apiKey: "AIzaSyATF1QlHR-wIxnWJiqoD9GdLv4OP-mMeoE",
        authDomain: "client-findup.firebaseapp.com",
        databaseURL: "https://client-findup.firebaseio.com",
        storageBucket: "client-findup.appspot.com",
        messagingSenderId: "235256403990"
    };

    firebase.initializeApp(firebaseConfig);
})(jQuery);

$('.editor').trumbowyg({
    'svgPath': '/img/icons.svg'
});

/**
 * Ativar aba ao dar refresh na pagina
 */
$('a[data-toggle="tab"]').on('click', function (e) {
    window.localStorage.setItem('activeTab', $(e.target).attr('href'));
});
var activeTab = window.localStorage.getItem('activeTab');
if (activeTab) {
    $('a[href="' + activeTab + '"]').tab('show');
}
(function ($) {
    'use strict';

    /**
     * Elemento da div de mapa
     */
    var mapElement = $('.map');

    /**
     * Campos de latitude e long
     */
    var inputLat = $('.input-lat');
    var inputLng = $('.input-lng');

    /**
     * Opções do Mapa
     * @type {{zoom: number, center: Array}}
     */
    var optionsMap = {
        zoom: 14,
        center: [8, 32]
    }

    if (mapElement.length) {

        /**
         *Verificar as opções do mapa, e mesclar com o padrão
         */
        if (mapElement.attr('data-options')) {
            var dataOptions = JSON.parse(mapElement.attr('data-options'));
            optionsMap = Object.assign(optionsMap, dataOptions);
        }

        /**
         * Definir instancia do mapa, e ponto central
         */
        var map = L.map('mapid', {
            center: optionsMap.center,
            zoom: optionsMap.zoom
        });

        /**
         * Mapa a ser utilizado na plataforma
         */
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">FindUP</a>'
        }).addTo(map);

        /**
         * Icons
         */
        var icons = {
            primary: L.icon({
                iconUrl: '/img/icon.png',
                iconAnchor: [24, 48]
            }),
            tech: L.icon({
                iconUrl: '/img/icon_tec.png',
                iconAnchor: [24, 48]
            })
        };

        /**
         * Se o marcador padrão do mapa for arrastavel, executar ações
         */
        if (mapElement.attr('data-marker')) {
            var markersInfo = JSON.parse(mapElement.attr('data-marker'));
            //Definir icon padrão da aplicação
            markersInfo.options.icon = icons.primary;
            // Criação do marcador principal
            var marker = L.marker(markersInfo.location, markersInfo.options).addTo(map);
            marker.on('dragend', function (e) {
                var position = e.target.getLatLng();
                inputLat.val(position.lat);
                inputLng.val(position.lng);
            })
        }

        /**
         * Se o marcador padrão do mapa for arrastavel, executar ações
         */
        if (mapElement.attr('data-marker-tech')) {
            var markersInfo = JSON.parse(mapElement.attr('data-marker-tech'));
            //Definir icon padrão da aplicação
            if (markersInfo.options) {
                markersInfo.options.icon = icons.tech;
            } else {
                markersInfo.options = {icon: icons.tech};
            }
            // Criação do marcador principal
            var markerTech = L.marker(markersInfo.location, markersInfo.options).addTo(map);
        }

        /**
         * Multiplos pontos
         */
        if (mapElement.attr('data-markers')) {
            var markersInfo = JSON.parse(mapElement.attr('data-markers'));

            var icon = undefined;
            if (markersInfo.icon) {
                icon = L.icon({
                    iconUrl: markersInfo.icon,
                    iconAnchor: [24, 48]
                })
            } else {
                icon = icons.tech;
            }

            console.log(markersInfo);

            markersInfo.markers.forEach(function (m) {
                console.log(L.marker(m.position, {icon: icon}).bindPopup(m.popup).addTo(map));
            });
        }

        if (mapElement.attr('data-tech-geofire')) {
            var markerTech = L.marker([8, 32], {icon: icons.tech}).addTo(map);
            var uid = mapElement.attr('data-tech-geofire');
            var ref = firebase.database().ref('/geolocation/' + uid);

            ref.on('value', function (snap) {
                var lat = snap.val().l[0];
                var lng = snap.val().l[1];

                markerTech.setLatLng([parseFloat(lat), parseFloat(lng)]).update();

                $.get('https://router.project-osrm.org/route/v1/driving/' + markersInfo.location[1] + ',' + markersInfo.location[0] + ';' + lng + ',' + lat + '?overview=false&alternatives=false&steps=true&hints=;', function (res) {

                    $('#field-distance').html((res.routes[0].distance / 1000));
                    $('#field-distance-time').html(minHour(res.routes[0].duration));

                    var route = [];
                    res.routes[0].legs[0].steps.forEach(function (step) {
                        step.intersections.forEach(function (int) {
                            route.push({lng: int.location[0], lat: int.location[1]});
                        })
                    });
                    var polyline = L.polyline(route, {color: '#25ccd8'}).addTo(map);
                    map.fitBounds(polyline.getBounds());
                });
            });
        }
    }

    function minHour(s) {
        function twoHouse(num) {
            if (num <= 9) {
                num = "0" + num;
            }
            return num;
        }

        var hour = twoHouse(Math.round(s / 3600));
        var minute = twoHouse(Math.floor((s % 3600) / 60));

        var formatted = hour + ":" + minute;

        return formatted;
    }
})(jQuery);


(function ($) {
    'use strict';

    $(document).ready(function () {
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0,00", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.sp_celphones').mask(SPMaskBehavior, spOptions);
    });
})(jQuery);
(function ($) {
    'use strict';

    var searchInput = $('.search-client');
    var URL = '/rest/users/clients.json';

    searchInput.select2({
        ajax: {
            url: URL,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.data.users,
                    pagination: {
                        more: (params.page * 30) < data.data.pagination.count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 5,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    })

    function formatRepo(repo) {
        // debugger;
        if (repo.first_name) {
            return (repo.first_name + " " + repo.last_name);
        } else {
            return repo.text;
        }

    }

    function formatRepoSelection(repo) {
        // debugger;
        if (repo.first_name) {
            return (repo.first_name + " " + repo.last_name);
        } else {
            return repo.text;
        }
    }
})(jQuery);

(function ($) {
    'use strict';

    var searchInput = $('.search-coupon');
    var URL = '/rest/coupons/index.json';

    searchInput.select2({
        ajax: {
            url: URL,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.data.coupons,
                    pagination: {
                        more: (params.page * 30) < data.data.pagination.count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 3,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    })

    function formatRepo(repo) {
       return repo.code;
    }

    function formatRepoSelection(repo) {
        return repo.code;
    }
})(jQuery);

(function ($) {
    'use strict';

    var searchInput = $('.search-tech');
    var URL = '/rest/users/specialists.json';

    searchInput.select2({
        ajax: {
            url: URL,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                    corporate: $(this).attr('data-corporate-id')
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.data.users,
                    pagination: {
                        more: (params.page * 30) < data.data.pagination.count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 5,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    })

    function formatRepo(repo) {
        // debugger;
        if (repo.first_name) {
            return (repo.first_name + " " + repo.last_name);
        } else {
            return repo.text;
        }

    }

    function formatRepoSelection(repo) {
        // debugger;
        if (repo.first_name) {
            return (repo.first_name + " " + repo.last_name);
        } else {
            return repo.text;
        }
    }
})(jQuery);

(function () {
    'use strict';

    var searchSelect = $('.search-select');
    var searchSelectTag = $('.search-select-tag');
    /**
     * Instanciar o select2
     */
    searchSelect.select2();

    /**
     * Tags
     */
    searchSelectTag.select2({
        tags: true,
        createTag: function (params) {
            return {
                id: 'tag-new-' + params.term,
                text:  params.term
            }
        }
    });

    /**
     * Eventos
     */

    searchSelect.on('change', function () {
        var elementId = $(this).val();

        /**
         * Carregar informações de load
         */
        if ($(this).attr('data-load-selects')) {
            var loadSelects = JSON.parse($(this).attr('data-load-selects'));
        }



        var hideLoad = 0;

        if (loadSelects) {
            $.LoadingOverlay("show");
            loadSelects.forEach(function (select) {
                var $ele = $('#' + select.id);
                $ele.empty();
                // Criar url com base na enviada, subistuir ID pelo id selecionar no campo
                var url = select.url.replace('ID', elementId);
                $.get(url, function (data) {
                    $ele.append(data);
                    hideLoad++;
                    if (hideLoad == loadSelects.length) $.LoadingOverlay("hide");
                });
            });

        }
    });


})();
// Set the configuration for your app
// var config = {
//     apiKey: "AIzaSyATF1QlHR-wIxnWJiqoD9GdLv4OP-mMeoE",
//     authDomain: "client-findup.firebaseapp.com",
//     databaseURL: "https://client-findup.firebaseio.com",
//     storageBucket: "client-findup.appspot.com",
//     messagingSenderId: "235256403990"
// };
// firebase.initializeApp(config);
var auth = firebase.auth();

//Loga anonimamente no firebase ao abrir a pagina
window.onload = function () {
    auth.onAuthStateChanged(function (user) {
        if (user) {
            console.log('Anonymous user signed-in.', user);
        } else {
            console.log('There was no anonymous session. Creating a new anonymous user.');
            // Sign the user in anonymously since accessing Storage requires the user to be authorized.
            auth.signInAnonymously();
        }
    });
}
// Get a reference to the storage service, which is used to create references in your storage bucket
var storageRef = firebase.storage().ref();

var obj = $("#drop-zone");
obj.on('dragenter', function (e) {
    e.stopPropagation();
    e.preventDefault();
    $(this).css('border', '2px solid #0B85A1');
});
obj.on('dragover', function (e) {
    e.stopPropagation();
    e.preventDefault();
});
obj.on('drop', function (e) {

    $(this).css('border', '2px dotted #0B85A1');
    e.preventDefault();
    var files = e.originalEvent.dataTransfer.files;

    //We need to send dropped files to Server
    handleFileUpload(files, obj);
});

$(document).on('dragenter', function (e) {
    e.stopPropagation();
    e.preventDefault();
});
$(document).on('dragover', function (e) {
    e.stopPropagation();
    e.preventDefault();
    obj.css('border', '2px dotted #0B85A1');
});
$(document).on('drop', function (e) {
    e.stopPropagation();
    e.preventDefault();
});

// automatically submit the form on file select
$('#drop-zone-file').on('change', function (e) {
    var files = $('#drop-zone-file')[0].files;
    handleFileUpload(files, obj);
});


function handleFileUpload(files, obj) {

    $.LoadingOverlay("show");

    for (var i = 0; i < files.length; i++) {
        var fd = new FormData();
        fd.append('file', files[i]);
        console.log(files[i]);
        fireBaseImageUpload({
            'file': files[i],
            'path': 'MEDIAS/occurrences/client/'
        }, function (data) {
            //console.log(data);
            if (!data.error) {
                if (data.progress) {
                    // progress update to view here
                }
                if (data.downloadURL) {
                    var html = '' +
                        '<div id="' + data.id + '" class="anex  col-md-12">' +
                        '<span><a href="' + data.downloadURL + '" target="_blank">' + data.fileName + '</a></span>' +
                        '<div class="invisible " style="display: none ">' +
                        '<input type="text" name="medias[' + data.id + '][filepath]" value="' + data.downloadURL + '">' +
                        '<input type="text" name="medias[' + data.id + '][file_type]" value="' + data.fileType + '">' +
                        '<input type="text" name="medias[' + data.id + '][name]" value="' + data.fileName + '">' +
                        '<input type="text" name="medias[' + data.id + '][type]" value="CLIENT">' +
                        '<input type="text" name="medias[' + data.id + '][firebase_ref]" value="' + data.firebase_ref + '">' +
                        '</div>' +
                        '<a href="javascript:void(0)" class="text-right pull-right" data-id="' + data.id + '"  data-ref="' + data.firebase_ref + '" onclick="removeFile(this)">X</a>' +
                        '</div>';
                    $('.image-preview').append(html);

                    $.LoadingOverlay("hide");
                }
            } else {
                console.log(data.error + ' Firebase image upload error');
            }
        });
    }
};

function removeFile(e) {
    $.LoadingOverlay("show");
    //Delete from firebase:
    storageRef.child(e.getAttribute('data-ref')).delete().then(function () {
        $('#' + e.getAttribute('data-id')).remove();
        console.log(e.getAttribute('data-ref'));
        console.log('Delete success');
        $.LoadingOverlay("hide");
    }).catch(function (error) {
        console.log('Delete fail ' + exception);
        $.LoadingOverlay("hide");
    });
}

function getImage(data) {
    if (data.fileType == 'image/jpeg') {
        return '<img src="' + data.downloadURL + '" width="32"/>'
    } else if (data.fileType == 'image/jpg') {
        return '<img src="' + data.downloadURL + '" width="32"/>'
    } else if (data.fileType == 'image/png') {
        return '<img src="' + data.downloadURL + '" width="32"/>'
    } else {
        return '<img src="/img/file.png" width="32"/>'
    }
}

function fireBaseImageUpload(parameters, callBackData) {

    // expected parameters to start storage upload
    var file = parameters.file;
    var path = parameters.path;
    var name;

    //just some error check
    if (!file) {
        callBackData({error: 'file required to interact with Firebase storage'});
    }
    if (!path) {
        callBackData({error: 'Node name required to interact with Firebase storage'});
    }

    var metaData = {'contentType': file.type};
    var arr = file.name.split('.');
    var fileSize = formatBytes(file.size); // get clean file size (function below)
    var fileType = file.type;
    var n = file.name;

    // generate random string to identify each upload instance
    name = generateRandomString(12); //(location function below)

    //var fullPath = path + '/' + name + '.' + arr.slice(-1)[0];
    var fullPath = path + '/' + name + '/' + file.name;

    var uploadFile = storageRef.child(fullPath).put(file, metaData);

    // first instance identifier
    callBackData({id: name, fileSize: fileSize, fileType: fileType, fileName: n});

    uploadFile.on('state_changed', function (snapshot) {
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        progress = Math.floor(progress);
        callBackData({
            progress: progress,
            element: name,
            fileSize: fileSize,
            fileType: fileType,
            fileName: n
        });
    }, function (error) {
        callBackData({error: error});
    }, function () {
        var downloadURL = uploadFile.snapshot.downloadURL;
        callBackData({
            downloadURL: downloadURL,
            element: name,
            fileSize: fileSize,
            fileType: fileType,
            fileName: n,
            id: name,
            firebase_ref: fullPath
        });
    });
}

function generateRandomString(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

function formatBytes(bytes, decimals) {
    if (bytes == 0) return '0 Byte';
    var k = 1000;
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
(function ($) {
    'use strict';

    $.fn.uploadIndividualMedia = function () {

        var supportedFormats = ['image/jpg', 'image/gif', 'image/png',
            'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'];

        /**
         * Gerar UUID
         * @returns {string}
         */
        function guid() {
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        this.each(function () {
            var fileInput = $(this).find('.fileButton');
            var loadingBox = $(this).find('.loading-box-client');
            var uploader = $(this).find('.uploader');
            fileInput.on('change', function (e) {
                var file = e.target.files[0];
                if (supportedFormats.indexOf(file.type)) {
                    // Carregar overlay
                    $(loadingBox).LoadingOverlay('show');
                    fileInput.attr('disabled', true);
                    //obter o arquivo
                    //referenciar o storege
                    var firebase_ref = 'medias/occurrences/' + $(this).attr('data-type') + '/' + guid() + '/' + file.name;
                    var storageRef = firebase.storage().ref(firebase_ref);
                    var data = {
                        name: file.name,
                        size: file.size,
                        file_type: file.type,
                        firebase_ref: firebase_ref,
                        occurrence_id: $(this).attr('data-occ-id'),
                        type: $(this).attr('data-type')
                    }
                    //enviar o a   rquivo
                    var task = storageRef.put(file);
                    task.on('state_changed',
                        function progress(snapshot) {
                            var percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                            uploader.val(percentage);
                        },
                        function error(err) {
                            console.log(err);
                        },
                        function complete() {
                            data.filepath = task.snapshot.downloadURL;
                            $.post('/medias/add.json', data)
                                .done(function (data) {
                                    window.location.reload();
                                }).fail(function () {
                                $('body').append('<div class="alert alert-success  alert-popup alert-dismissible"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> <strong><i class="icon fa fa-check"></i> Falha!</strong>Não aceitamos este tipo de arquivo.</div>');
                            });
                        }
                    );
                } else {
                    $('body').append('<div class="alert alert-success  alert-popup alert-dismissible"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> <strong><i class="icon fa fa-check"></i> Falha!</strong>Não aceitamos este tipo de arquivo.</div>');
                }
            });
        });
    };


    $('.annex-box').uploadIndividualMedia();

})(jQuery);