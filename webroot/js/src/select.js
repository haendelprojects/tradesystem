(function () {
    'use strict';

    var searchSelect = $('.search-select');
    var searchSelectTag = $('.search-select-tag');
    /**
     * Instanciar o select2
     */
    searchSelect.select2();

    /**
     * Tags
     */
    searchSelectTag.select2({
        tags: true,
        createTag: function (params) {
            return {
                id: 'tag-new-' + params.term,
                text:  params.term
            }
        }
    });

    /**
     * Eventos
     */

    searchSelect.on('change', function () {
        var elementId = $(this).val();

        /**
         * Carregar informações de load
         */
        if ($(this).attr('data-load-selects')) {
            var loadSelects = JSON.parse($(this).attr('data-load-selects'));
        }



        var hideLoad = 0;

        if (loadSelects) {
            $.LoadingOverlay("show");
            loadSelects.forEach(function (select) {
                var $ele = $('#' + select.id);
                $ele.empty();
                // Criar url com base na enviada, subistuir ID pelo id selecionar no campo
                var url = select.url.replace('ID', elementId);
                $.get(url, function (data) {
                    $ele.append(data);
                    hideLoad++;
                    if (hideLoad == loadSelects.length) $.LoadingOverlay("hide");
                });
            });

        }
    });


})();