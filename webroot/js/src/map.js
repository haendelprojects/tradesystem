(function ($) {
    'use strict';

    /**
     * Elemento da div de mapa
     */
    var mapElement = $('.map');

    /**
     * Campos de latitude e long
     */
    var inputLat = $('.input-lat');
    var inputLng = $('.input-lng');

    /**
     * Opções do Mapa
     * @type {{zoom: number, center: Array}}
     */
    var optionsMap = {
        zoom: 14,
        center: [8, 32]
    }

    if (mapElement.length) {

        /**
         *Verificar as opções do mapa, e mesclar com o padrão
         */
        if (mapElement.attr('data-options')) {
            var dataOptions = JSON.parse(mapElement.attr('data-options'));
            optionsMap = Object.assign(optionsMap, dataOptions);
        }

        /**
         * Definir instancia do mapa, e ponto central
         */
        var map = L.map('mapid', {
            center: optionsMap.center,
            zoom: optionsMap.zoom
        });

        /**
         * Mapa a ser utilizado na plataforma
         */
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">FindUP</a>'
        }).addTo(map);

        /**
         * Icons
         */
        var icons = {
            primary: L.icon({
                iconUrl: '/img/icon.png',
                iconAnchor: [24, 48]
            }),
            tech: L.icon({
                iconUrl: '/img/icon_tec.png',
                iconAnchor: [24, 48]
            })
        };

        /**
         * Se o marcador padrão do mapa for arrastavel, executar ações
         */
        if (mapElement.attr('data-marker')) {
            var markersInfo = JSON.parse(mapElement.attr('data-marker'));
            //Definir icon padrão da aplicação
            markersInfo.options.icon = icons.primary;
            // Criação do marcador principal
            var marker = L.marker(markersInfo.location, markersInfo.options).addTo(map);
            marker.on('dragend', function (e) {
                var position = e.target.getLatLng();
                inputLat.val(position.lat);
                inputLng.val(position.lng);
            })
        }

        /**
         * Se o marcador padrão do mapa for arrastavel, executar ações
         */
        if (mapElement.attr('data-marker-tech')) {
            var markersInfo = JSON.parse(mapElement.attr('data-marker-tech'));
            //Definir icon padrão da aplicação
            if (markersInfo.options) {
                markersInfo.options.icon = icons.tech;
            } else {
                markersInfo.options = {icon: icons.tech};
            }
            // Criação do marcador principal
            var markerTech = L.marker(markersInfo.location, markersInfo.options).addTo(map);
        }

        /**
         * Multiplos pontos
         */
        if (mapElement.attr('data-markers')) {
            var markersInfo = JSON.parse(mapElement.attr('data-markers'));

            var icon = undefined;
            if (markersInfo.icon) {
                icon = L.icon({
                    iconUrl: markersInfo.icon,
                    iconAnchor: [24, 48]
                })
            } else {
                icon = icons.tech;
            }

            console.log(markersInfo);

            markersInfo.markers.forEach(function (m) {
                console.log(L.marker(m.position, {icon: icon}).bindPopup(m.popup).addTo(map));
            });
        }

        if (mapElement.attr('data-tech-geofire')) {
            var markerTech = L.marker([8, 32], {icon: icons.tech}).addTo(map);
            var uid = mapElement.attr('data-tech-geofire');
            var ref = firebase.database().ref('/geolocation/' + uid);

            ref.on('value', function (snap) {
                var lat = snap.val().l[0];
                var lng = snap.val().l[1];

                markerTech.setLatLng([parseFloat(lat), parseFloat(lng)]).update();

                $.get('https://router.project-osrm.org/route/v1/driving/' + markersInfo.location[1] + ',' + markersInfo.location[0] + ';' + lng + ',' + lat + '?overview=false&alternatives=false&steps=true&hints=;', function (res) {

                    $('#field-distance').html((res.routes[0].distance / 1000));
                    $('#field-distance-time').html(minHour(res.routes[0].duration));

                    var route = [];
                    res.routes[0].legs[0].steps.forEach(function (step) {
                        step.intersections.forEach(function (int) {
                            route.push({lng: int.location[0], lat: int.location[1]});
                        })
                    });
                    var polyline = L.polyline(route, {color: '#25ccd8'}).addTo(map);
                    map.fitBounds(polyline.getBounds());
                });
            });
        }
    }

    function minHour(s) {
        function twoHouse(num) {
            if (num <= 9) {
                num = "0" + num;
            }
            return num;
        }

        var hour = twoHouse(Math.round(s / 3600));
        var minute = twoHouse(Math.floor((s % 3600) / 60));

        var formatted = hour + ":" + minute;

        return formatted;
    }
})(jQuery);

