(function ($) {
    "use strict";


    var inputs = {
        street: $('.street'),
        city: $('.city'),
        state: $('.state'),
        neighborhood: $('.neighborhood'),
        zipcode: $('.zipcode'),
        country: $('.country')
    }

    var boxSearchAddress = $('.box-search-address');

    var btnGetByZipcode = $('.search-zipcode');

    btnGetByZipcode.on('click', function () {
        boxSearchAddress.LoadingOverlay("show");
        $.get("https://api.postmon.com.br/v1/cep/" + inputs.zipcode.val() + "?format=json ",
            function (result) {
                inputs.neighborhood.val(result.bairro);
                inputs.street.val(result.logradouro);

                inputs.state
                    .children('option')
                    .each(function (o) {
                        if ($(this).html() == result.estado_info.nome) {
                            inputs.state.val($(this).val());
                        }
                    });
                $.get('/rest/addresses/selectCitiesByStateName/' + result.estado, function (res) {
                    inputs.city.empty();
                    inputs.city.append(res);
                    inputs.city
                        .children('option')
                        .each(function (o) {
                            if ($(this).html() == result.cidade) {
                                inputs.city.val($(this).val());
                            }
                        });
                    boxSearchAddress.LoadingOverlay("hide");
                });
            }).fail(function () {
                alert('Servidor dos correios fora do ar, abra sem buscar CEP.');
                boxSearchAddress.LoadingOverlay("hide");
            });
        })


    /**
     * Se tiver pais
     */
    inputs.country.on('change', function () {
        var url = '/rest/addresses/states/' + $(this).val();
        boxSearchAddress.LoadingOverlay("show");
        $.get(url, function (data) {
            inputs.state.empty();
            inputs.city.empty();
            inputs.state.append(data);
            boxSearchAddress.LoadingOverlay("hide");
        });
    });

    /**
     * Se tiver estado
     */
    inputs.state.on('change', function () {
        var url = '/rest/addresses/cities/' + $(this).val();
        boxSearchAddress.LoadingOverlay("show");
        $.get(url, function (data) {
            inputs.city.empty();
            inputs.city.append(data);
            boxSearchAddress.LoadingOverlay("hide");
        });
    });
})(jQuery);
