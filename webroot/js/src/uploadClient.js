// (function ($) {
//     'use strict';

    $.fn.uploadIndividualMedia = function () {

        var supportedFormats = ['image/jpg', 'image/gif', 'image/png',
            'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf'];

        /**
         * Gerar UUID
         * @returns {string}
         */
        function guid() {
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        this.each(function () {
            var fileInput = $(this).find('.fileButton');
            var loadingBox = $(this).find('.loading-box-client');
            var uploader = $(this).find('.uploader');
            fileInput.on('change', function (e) {
                var file = e.target.files[0];
                if (supportedFormats.indexOf(file.type)) {
                    // Carregar overlay
                    $(loadingBox).LoadingOverlay('show');
                    fileInput.attr('disabled', true);
                    //obter o arquivo
                    //referenciar o storege
                    var firebase_ref = 'medias/occurrences/' + $(this).attr('data-type') + '/' + guid() + '/' + file.name;
                    var storageRef = firebase.storage().ref(firebase_ref);
                    var data = {
                        name: file.name,
                        size: file.size,
                        file_type: file.type,
                        firebase_ref: firebase_ref,
                        occurrence_id: $(this).attr('data-occ-id'),
                        type: $(this).attr('data-type')
                    }
                    //enviar o a   rquivo
                    var task = storageRef.put(file);
                    task.on('state_changed',
                        function progress(snapshot) {
                            var percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                            uploader.val(percentage);
                        },
                        function error(err) {
                            console.log(err);
                        },
                        function complete() {
                            data.filepath = task.snapshot.downloadURL;
                            $.post('/medias/add.json', data)
                                .done(function (data) {
                                    window.location.reload();
                                }).fail(function () {
                                $('body').append('<div class="alert alert-success  alert-popup alert-dismissible"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> <strong><i class="icon fa fa-check"></i> Falha!</strong>Não aceitamos este tipo de arquivo.</div>');
                            });
                        }
                    );
                } else {
                    $('body').append('<div class="alert alert-success  alert-popup alert-dismissible"> <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> <strong><i class="icon fa fa-check"></i> Falha!</strong>Não aceitamos este tipo de arquivo.</div>');
                }
            });
        });
    };
    $('.annex-box').uploadIndividualMedia();

// })(jQuery);