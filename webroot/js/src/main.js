(function ($) {
    'use strict';
    /**
     * Ações Gerais
     */
    $('.check-all').on('click', function () {
        $('.checkbox').attr('checked', true);
    });

    $('.uncheck-all').on('click', function () {
        $('.checkbox').attr('checked', false);
    });

    /**
     * Campo de data
     */
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR",
        orientation: "top auto",
        autoclose: true,
        todayHighlight: true
    });


    // Firebase

    // // Initialize Firebase
    // var firebaseConfig = {
    //     apiKey: "AIzaSyATF1QlHR-wIxnWJiqoD9GdLv4OP-mMeoE",
    //     authDomain: "client-findup.firebaseapp.com",
    //     databaseURL: "https://client-findup.firebaseio.com",
    //     storageBucket: "client-findup.appspot.com",
    //     messagingSenderId: "235256403990"
    // };
    //
    // firebase.initializeApp(firebaseConfig);  // // Initialize Firebase
    // var firebaseConfig = {
    //     apiKey: "AIzaSyATF1QlHR-wIxnWJiqoD9GdLv4OP-mMeoE",
    //     authDomain: "client-findup.firebaseapp.com",
    //     databaseURL: "https://client-findup.firebaseio.com",
    //     storageBucket: "client-findup.appspot.com",
    //     messagingSenderId: "235256403990"
    // };
    //
    // firebase.initializeApp(firebaseConfig);
})(jQuery);

$('.editor').trumbowyg({
    'svgPath': '/img/icons.svg'
});

/**
 * Ativar aba ao dar refresh na pagina
 */
$('a[data-toggle="tab"]').on('click', function (e) {
    window.localStorage.setItem('activeTab', $(e.target).attr('href'));
});
var activeTab = window.localStorage.getItem('activeTab');
if (activeTab) {
    $('a[href="' + activeTab + '"]').tab('show');
}