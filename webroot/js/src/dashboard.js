(function ($) {
    'use strict';
    /**
     * Chart de filiais
     */
    var classChartAddressOccurrences = $('.chart-address-occurrences');
    if (classChartAddressOccurrences.length) {
        var ctxa = document.getElementById("idChartOccurrencesAddress").getContext('2d');
        var chartOccurrencesAddress = new Chart(ctxa, {
            type: 'bar',

            data: {
                labels: JSON.parse(classChartAddressOccurrences.attr('data-labels')),
                ids: JSON.parse(classChartAddressOccurrences.attr('data-ids')),
                datasets: [{
                    label: 'Total de ocorrências por filial',
                    data: JSON.parse(classChartAddressOccurrences.attr('data-values')),
                    backgroundColor: '#3c8dbc '
                }]
            },
            options: {
                legend: {display: false},
                scaleShowLabels: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return value.substr(0, 5);
                            }
                        },
                    }]
                }
            }
        });

        classChartAddressOccurrences.on('click', function (evt) {
            var activePoint = chartOccurrencesAddress.getElementAtEvent(evt);
            var val = chartOccurrencesAddress.data.ids[activePoint[0]._index];
            var win = window.open('/occurrences?filial=' + val, '_blank');
            win.focus();
        });
    }

    /**
     * Chart serviços
     * @type {void|jQuery|HTMLElement}
     */
    var classChartServices = $('.chart-services');
    if (classChartServices.length) {
        var ctxs = document.getElementById("idChartServices").getContext('2d');
        var chartServices = new Chart(ctxs, {
            type: 'bar',
            data: {
                labels: JSON.parse(classChartServices.attr('data-labels')),
                ids: JSON.parse(classChartServices.attr('data-ids')),
                datasets: [{
                    label: 'Total de ocorrências por serviço',
                    data: JSON.parse(classChartServices.attr('data-values')),
                    backgroundColor: '#2b89f8 '
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return value.substr(0, 5);
                            },
                            minor: {
                                callback: function (v) {
                                }
                            }
                        },
                        minor: {
                            callback: function (v) {
                            }
                        }
                    }]
                }
            }
        });

        classChartServices.on('click', function (evt) {
            var activePoint = chartServices.getElementAtEvent(evt);
            var val = chartServices.data.ids[activePoint[0]._index];
            var win = window.open('/occurrences?service_id=' + val, '_blank');
            win.focus();
        });
    }

    /**
     * Chart ups
     * @type {void|jQuery|HTMLElement}
     */
    var classChartUps = $('.chart-ups');
    if (classChartUps.length) {
        var ctxu = document.getElementById("idChartUps").getContext('2d');
        var chartUps = new Chart(ctxu, {
            type: 'bar',
            data: {
                labels: JSON.parse(classChartUps.attr('data-labels')),
                datasets: [{
                    label: 'Total de UPs no mês',
                    data: JSON.parse(classChartUps.attr('data-values')),
                    backgroundColor: '#42fbca '
                }]
            },
            options: {
                legend: {display: false},
                scaleShowLabels: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {display: false}
                    }],
                    xAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return value.substr(0, 5);
                            }
                        },
                    }]
                }
            }
        });

        classChartUps.on('click', function (evt) {
            var activePoint = chartUps.getElementAtEvent(evt);
            var val = chartUps.data.labels[activePoint[0]._index];
            var win = window.open('/occurrences?month=' + val, '_blank');
            win.focus();
        });
    }

    /**
     * Chart de validações dadas
     * @type {void|jQuery|HTMLElement}
     */

    var classChartValidation = $('.chart-validation');
    if (classChartValidation.length) {
        var ctxv = document.getElementById("idValidation").getContext('2d');
        var chartValidation = new Chart(ctxv, {
            type: 'doughnut',
            data: {
                labels: JSON.parse(classChartValidation.attr('data-labels')),
                datasets: [{
                    label: 'Total de UPs no mês',
                    data: JSON.parse(classChartValidation.attr('data-values')),
                    backgroundColor: ["#dedede", "#d5fb42", "#70a2b5", "#3c00ff", "#42b7fb", "#42fbca"]
                }]
            }
        });

        classChartValidation.on('click', function (evt) {
            var activePoint = chartValidation.getElementAtEvent(evt);
            var val = chartValidation.data.labels[activePoint[0]._index];
            var win = window.open('/occurrences?ratings=' + val, '_blank');
            win.focus();
        });
    }

})(jQuery);
