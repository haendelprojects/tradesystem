// Set the configuration for your app
// var config = {
//     apiKey: "AIzaSyATF1QlHR-wIxnWJiqoD9GdLv4OP-mMeoE",
//     authDomain: "client-findup.firebaseapp.com",
//     databaseURL: "https://client-findup.firebaseio.com",
//     storageBucket: "client-findup.appspot.com",
//     messagingSenderId: "235256403990"
// };
// firebase.initializeApp(config);



var auth = firebase.auth();

//Loga anonimamente no firebase ao abrir a pagina
window.onload = function () {
    auth.onAuthStateChanged(function (user) {
        if (user) {
            console.log('Anonymous user signed-in.', user);
        } else {
            console.log('There was no anonymous session. Creating a new anonymous user.');
            // Sign the user in anonymously since accessing Storage requires the user to be authorized.
            auth.signInAnonymously();
        }
    });
}
// Get a reference to the storage service, which is used to create references in your storage bucket
var storageRef = firebase.storage().ref();

var obj = $("#drop-zone");
obj.on('dragenter', function (e) {
    e.stopPropagation();
    e.preventDefault();
    $(this).css('border', '2px solid #0B85A1');
});
obj.on('dragover', function (e) {
    e.stopPropagation();
    e.preventDefault();
});
obj.on('drop', function (e) {

    $(this).css('border', '2px dotted #0B85A1');
    e.preventDefault();
    var files = e.originalEvent.dataTransfer.files;

    //We need to send dropped files to Server
    handleFileUpload(files, obj);
});

$(document).on('dragenter', function (e) {
    e.stopPropagation();
    e.preventDefault();
});
$(document).on('dragover', function (e) {
    e.stopPropagation();
    e.preventDefault();
    obj.css('border', '2px dotted #0B85A1');
});
$(document).on('drop', function (e) {
    e.stopPropagation();
    e.preventDefault();
});

// automatically submit the form on file select
$('#drop-zone-file').on('change', function (e) {
    var files = $('#drop-zone-file')[0].files;
    handleFileUpload(files, obj);
});


function handleFileUpload(files, obj) {

    $.LoadingOverlay("show");

    for (var i = 0; i < files.length; i++) {
        var fd = new FormData();
        fd.append('file', files[i]);
        console.log(files[i]);
        fireBaseImageUpload({
            'file': files[i],
            'path': 'MEDIAS/occurrences/client/'
        }, function (data) {
            //console.log(data);
            if (!data.error) {
                if (data.progress) {
                    // progress update to view here
                }
                if (data.downloadURL) {
                    var html = '' +
                        '<div id="' + data.id + '" class="anex  col-md-12">' +
                        '<span><a href="' + data.downloadURL + '" target="_blank">' + data.fileName + '</a></span>' +
                        '<div class="invisible " style="display: none ">' +
                        '<input type="text" name="medias[' + data.id + '][filepath]" value="' + data.downloadURL + '">' +
                        '<input type="text" name="medias[' + data.id + '][file_type]" value="' + data.fileType + '">' +
                        '<input type="text" name="medias[' + data.id + '][name]" value="' + data.fileName + '">' +
                        '<input type="text" name="medias[' + data.id + '][type]" value="CLIENT">' +
                        '<input type="text" name="medias[' + data.id + '][firebase_ref]" value="' + data.firebase_ref + '">' +
                        '</div>' +
                        '<a href="javascript:void(0)" class="text-right pull-right" data-id="' + data.id + '"  data-ref="' + data.firebase_ref + '" onclick="removeFile(this)">X</a>' +
                        '</div>';
                    $('.image-preview').append(html);

                    $.LoadingOverlay("hide");
                }
            } else {
                console.log(data.error + ' Firebase image upload error');
            }
        });
    }
};

function removeFile(e) {
    $.LoadingOverlay("show");
    //Delete from firebase:
    storageRef.child(e.getAttribute('data-ref')).delete().then(function () {
        $('#' + e.getAttribute('data-id')).remove();
        console.log(e.getAttribute('data-ref'));
        console.log('Delete success');
        $.LoadingOverlay("hide");
    }).catch(function (error) {
        console.log('Delete fail ' + exception);
        $.LoadingOverlay("hide");
    });
}

function getImage(data) {
    if (data.fileType == 'image/jpeg') {
        return '<img src="' + data.downloadURL + '" width="32"/>'
    } else if (data.fileType == 'image/jpg') {
        return '<img src="' + data.downloadURL + '" width="32"/>'
    } else if (data.fileType == 'image/png') {
        return '<img src="' + data.downloadURL + '" width="32"/>'
    } else {
        return '<img src="/img/file.png" width="32"/>'
    }
}

function fireBaseImageUpload(parameters, callBackData) {

    // expected parameters to start storage upload
    var file = parameters.file;
    var path = parameters.path;
    var name;

    //just some error check
    if (!file) {
        callBackData({error: 'file required to interact with Firebase storage'});
    }
    if (!path) {
        callBackData({error: 'Node name required to interact with Firebase storage'});
    }

    var metaData = {'contentType': file.type};
    var arr = file.name.split('.');
    var fileSize = formatBytes(file.size); // get clean file size (function below)
    var fileType = file.type;
    var n = file.name;

    // generate random string to identify each upload instance
    name = generateRandomString(12); //(location function below)

    //var fullPath = path + '/' + name + '.' + arr.slice(-1)[0];
    var fullPath = path + '/' + name + '/' + file.name;

    var uploadFile = storageRef.child(fullPath).put(file, metaData);

    // first instance identifier
    callBackData({id: name, fileSize: fileSize, fileType: fileType, fileName: n});

    uploadFile.on('state_changed', function (snapshot) {
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        progress = Math.floor(progress);
        callBackData({
            progress: progress,
            element: name,
            fileSize: fileSize,
            fileType: fileType,
            fileName: n
        });
    }, function (error) {
        callBackData({error: error});
    }, function () {
        var downloadURL = uploadFile.snapshot.downloadURL;
        callBackData({
            downloadURL: downloadURL,
            element: name,
            fileSize: fileSize,
            fileType: fileType,
            fileName: n,
            id: name,
            firebase_ref: fullPath
        });
    });
}

function generateRandomString(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

function formatBytes(bytes, decimals) {
    if (bytes == 0) return '0 Byte';
    var k = 1000;
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}