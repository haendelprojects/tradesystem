(function ($) {
    'use strict';

    var timeOut = undefined;

    function initAtt() {
        var attCheck = window.localStorage.getItem('attCheck');
        clearTimeout(timeOut);
        if (attCheck) {
            var attTime = window.localStorage.getItem('attTime');
            if ($('.att-check').length) {
                $('.att-check').prop('checked', true);
                $('.att-time').val(attTime);
                timeOut = setTimeout(function () {
                    window.location.reload(1);
                }, attTime * 10000);
            }
        }
    }

    initAtt();

    $('.att-check').on('click', function () {
        if ($(this).prop('checked')) {
            window.localStorage.setItem('attCheck', true);
            window.localStorage.setItem('attTime', $('.att-time').val());
            initAtt();
        } else {
            window.localStorage.removeItem('attCheck');
            window.localStorage.removeItem('attTime');
        }
    });

    $('.att-time').on('change', function () {
        window.localStorage.removeItem('attTime');
        window.localStorage.setItem('attTime', $('.att-time').val());
        initAtt();
    })
})(jQuery);

