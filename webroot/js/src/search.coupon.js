/**
 * Created by Haendel on 02/09/2016.
 */
(function ($) {
    'use strict';

    var searchInput = $('.search-coupon');
    var URL = '/rest/coupons/index.json';

    searchInput.select2({
        ajax: {
            url: URL,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.data.coupons,
                    pagination: {
                        more: (params.page * 30) < data.data.pagination.count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 3,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    })

    function formatRepo(repo) {
       return repo.code;
    }

    function formatRepoSelection(repo) {
        return repo.code;
    }
})(jQuery);
