<?php
/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 02/06/2016
 * Time: 14:53
 */

namespace EmailNotifications\Controller\Component;


use Cake\Controller\Component;
use Cake\Network\Http\Client;

/**
 * Class MaildockerComponent
 * @package EmailNotifications\Controller\Component
 * @property \Cake\Http\Client $Http
 *
 */
class MaildockerComponent extends Component
{
    private $to, $from, $subject, $substitutions, $content, $template, $Http, $apiKey, $templates;

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setKey($key, $value)
    {
        $this->apiKey = base64_encode("$key:$value");


        $this->Http = new Client([
            'headers' => [
                'Authorization' => 'Basic ' . $this->apiKey
            ]
        ]);
        return $this;
    }

    /**
     * Adicionar templates
     * @param $name
     * @param $value
     * @return $this
     */
    public function addTemplate($name, $value)
    {
        $this->templates[$name] = $value;
        return $this;
    }

    /**
     * @param string $email
     * @param string $name
     * * @return $this
     */
    public function setTo($email = '', $name = '')
    {
        $this->to = [
            "email" => $email,
            "name" => $name
        ];
        return $this;
    }

    /**
     * @param string $email
     * @param string $name
     * * @return $this
     */
    public function setFrom($email = '', $name = '')
    {
        $this->from = [
            "email" => $email,
            "name" => $name
        ];

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setSubject($value = '')
    {
        $this->subject = $value;

        return $this;
    }

    /**
     * @param array $array
     * @return $this
     */
    public function setSubstitutions($array = [])
    {
        $this->to['merge_vars'] = $array;

        return $this;
    }

    /**
     * @param string $type
     * @param string $value
     * @return $this
     */
    public function setContent($type = 'text/html', $value = 'Não tem Suporte a Html.')
    {
        $this->content = [
            'type' => $type,
            'value' => $value
        ];

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setTemplateId($value)
    {
        $this->template = $value;

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setTemplate($value)
    {
        $this->template = $this->templates[$value];

        return $this;
    }

    public function getSendInfos()
    {
        $data = [

            "to" =>
                $this->to,
            "from" => $this->from,
            "template" => $this->template
        ];

        return json_encode($data);
    }

    public function send()
    {
        $r = $this->Http->post(
            'http://findup.maildocker.io/api/maildocker/v1/mail/',
            $this->getSendInfos(),
            ['type' => 'json']
        );

        debug($r->getStatusCode());
//        return

        return $r;
    }


}