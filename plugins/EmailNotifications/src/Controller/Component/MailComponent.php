<?php

namespace EmailNotifications\Controller\Component;

use App\Model\Entity\User;
use App\Model\Entity\Occurrence;
use App\Model\Entity\Service;
use App\Model\Entity\Contract;
use App\Model\Entity\Address;
use App\Model\Entity\Fund;
use Cake\Controller\Component;
use Cake\Network\Http\Client;
use Cake\Routing\Router;
use Cake\Utility\Security;

/**
 * Description of MailComponent
 * @author Erick
 * @property \SendGrid $Sender
 * @property Client $Http
 * @property SendgridComponent $Sendgrid
 * @property MaildockerComponent $MailDocker
 * @property MaildockerComponent $Transition
 */
class MailComponent extends Component
{
    private $Transition;
    private $Http;
    private $email_enterprise, $name_enterprise;

    public $components = [
        'EmailNotifications.Sendgrid',
        'EmailNotifications.Maildocker'
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);
        //$this->Http = new Client();

        $this->email_enterprise = 'noreply@findup.com.br';
        $this->name_enterprise = 'FindUP';

        $this->Transition = $this->Maildocker;
        /**
         * Criando aplicação MailDocker
         */
        $this->Transition
            ->setKey('048815b867fafffe363b6', 'be9ff8eea08204ad2d')
            ->addTemplate('cadaster', 'CADASTRO')
            ->addTemplate('invite', 'CONVITE')
            ->addTemplate('new-occurrence', 'NOVA_OCORRENCIA')
            ->addTemplate('specialist-accepted', 'ESPECIALISTA_ACEITOU')
            ->addTemplate('activated-specialist', 'CADASTRO VALIDADO')
            ->addTemplate('specialist-going', 'ESPECIALISTA_GOING')
            ->addTemplate('specialist-service', 'ESPECIALISTA_SERVICE')
            ->addTemplate('specialist-finish-b2b', 'ESPECIALISTA_ACABOU_B2B')
            ->addTemplate('specialist-finish-b2c', 'ESPECIALISTA_ACABOU_B2C')
            ->addTemplate('specialist-validated-b2b', 'VALIDADO_B2B')
            ->addTemplate('message', 'MESSAGE')
            ->addTemplate('SLA', 'SLA')
            ->addTemplate('DOCUMENTS', 'DOCUMENTS')
            ->addTemplate('INVOICE_TECH','INVOICE_TECH');

    }


    /**
     * @param $title
     * @param $text
     * @param $user
     * @return \Cake\Http\Client
     */
    public function message($title, $text, $user)
    {
        if (is_string($user)) {
            return $this->Transition
                ->setTo($user, $user)
                ->setFrom($this->email_enterprise, $this->name_enterprise)
                ->setSubject($title)
                ->setContent()
                ->setSubstitutions(['text' => $text])
                ->setTemplate('message')
                ->send();
        } else {
            return $this->Transition
                ->setTo($user->username, $user->first_name)
                ->setFrom($this->email_enterprise, $this->name_enterprise)
                ->setSubject($title)
                ->setContent()
                ->setSubstitutions(['text' => $text])
                ->setTemplate('message')
                ->send();
        }
    }

    /**
     * Pedir nota fiscal de atendimentos
     * @param $user
     * @return \Cake\Http\Client\Response
     */
    public function invoice($email, $name, $price)
    {
        return $this->Transition
            ->setTo($email, $name)
            ->setFrom($this->email_enterprise, $this->name_enterprise)
            ->setSubject('Nota Fiscal')
            ->setContent()
            ->setSubstitutions(['name' => $name,'price' => $price])
            ->setTemplate('INVOICE_TECH')
            ->send();
    }

    public function documents($user)
    {
        return $this->Transition
            ->setTo($user->username, $user->first_name)
            ->setFrom($this->email_enterprise, $this->name_enterprise)
            ->setSubject('Documentos')
            ->setContent()
            ->setTemplate('DOCUMENTS')
            ->send();
    }

    public function newCadasterInvite(User $user)
    {
        $hash = Security::encrypt($user->username, Security::salt());

        $this->Transition
            ->setTo($user->username, $user->full_name)
            ->setFrom($this->email_enterprise, $this->name_enterprise)
            ->setSubject(__('Bem Vindo'))
            ->setContent()
            ->setTemplate('invite')
            ->setSubstitutions(['-name-' => $user->full_name, '-link-' => Router::url(['controller' => 'users', 'action' => 'invite', 123])])
            ->send();
    }

    public function activatedSpecialist(User $user)
    {
        $this->Transition
            ->setTo($user->username, $user->first_name)
            ->setFrom($this->email_enterprise, $this->name_enterprise)
            ->setSubject(__('Cadastro Validado'))
            ->setContent()
            ->setTemplate('activated-specialist')
            ->setSubstitutions(['-name-' => $user->first_name])
            ->send();
    }

    public function newCadaster(User $user)
    {
        $this->Sendgrid
            ->setTo($user->username, $user->first_name)
            ->setFrom($this->email_enterprise, $this->name_enterprise)
            ->setSubject('Bem Vindo')
            ->setContent()
            ->setTemplate('cadaster')
            ->setSubstitutions(['-name-' => $user->first_name])
            ->send();

//        $email = new Email();
////
//        $email
//            ->addTo('erickhaendel@gmail.com', ['name' => "Erick"])
//            ->setFrom('noreply@findup.com.br', ['name' => 'FindUP'])
//            ->setSubject('Bem-vindo')
//            ->addFilter('content' , 'type', "text/html")
//            ->setText(' ')
//            ->setTemplateId('38888f2e-6eec-4098-976d-619f103ecf0e')
//            ->addSubstitution('-name-', ["Erick Haendel"]);
//
//
//        $this->Sender->send($email);
    }

    public function newOccurrence(User $user, Occurrence $occurrence, Service $service, Address $address, Contract $contract, Fund $fund)
    {
        $this->Sendgrid
            ->setTo($user->username, $user->first_name)
            ->setFrom($this->email_enterprise, $this->name_enterprise)
            ->setSubject('Nova Ocorrência!')
            ->setContent()
            ->setTemplate('cadaster')
            ->setSubstitutions([
                '-name-' => $user->first_name, 'schedule_time' => $occurrence->schedule_time, '-description-' => $occurrence->description, 'address' => $address,
                'service' => $service->name, 'filial' => $address->title, 'contract' => $contract->name, 'created' => $occurrence->created, 'credit_balance' => $fund->credit_balance
            ])
            ->send();
    }

    public function sla($first_name, $username, $occurrence_id, $sla)
    {
        $this->Transition
            ->setTo($username, $first_name)
            ->setFrom($this->email_enterprise, $this->name_enterprise)
            ->setSubject(__('SLA Violado.'))
            ->setContent()
            ->setTemplate('SLA')
            ->setSubstitutions(['os' => $occurrence_id, 'sla' => $sla])
            ->send();
    }

}

