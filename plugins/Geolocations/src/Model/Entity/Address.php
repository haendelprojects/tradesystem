<?php
namespace GeoLocations\Model\Entity;

use Cake\ORM\Entity;

/**
 * Address Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $street
 * @property string $number
 * @property string $complement
 * @property string $neighborhood
 * @property string $city
 * @property string $country
 * @property string $state
 * @property string $title
 * @property string $zipcode
 * @property float $latitude
 * @property float $longitude
 * @property int $individual_id
 * @property int $enterprise_id
 * @property bool $active
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Address extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _setZipcode($value)
    {
        if ($value) {
            return preg_replace("/\D+/", "", $value);
        } else {
            return null;
        }

    }

    protected function _getFormatted()
    {
        return $this->_properties['street'] . ', ' .  $this->_properties['neighborhood'] . ' - ' . $this->_properties['city'];
    }

}
