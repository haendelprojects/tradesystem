<?php
/**
 * Created by PhpStorm.
 * User: Ananda Almeida
 * Date: 20/02/2018
 * Time: 10:58
 */

namespace App\Test;


class AuthUser
{
    static function getUser()
    {
        return [
            'id' => (int)255,
            'username' => 'desenvolvimento@findup.com.br',
            'first_name' => 'Desenvolvimento',
            'last_name' => 'FindUP',
            'new_photo' => true,
            'reset_password_token' => '817d1b87b32cd9823d151f7de6a87b99afd395ee9c147af0a111504da328cfd8',
            'hash_password' => null,
            'user_reference_id' => null,
            'accept_demands' => false,
            'moderator' => false,
            'role_id' => (int)1,
            'notification_id' => (int)14,
            'enterprise_id' => (int)1903,
            'access_client' => true,
            'access_admin' => true,
            'access_specialist' => true,
            'must_change_password' => false,
            'hash_invite' => null,
            'lat' => '-8.0618757',
            'lng' => '-34.8726667',
            'facebook_pass' => '',
            'facebook_id' => '',
            'uid_user' => '-Kmb-zXyslGZnr7zb958',
            'uid_geolocation' => '-Kmb-zXyslGZnr7zb958',
            'black_list' => false,
            'timezone' => 'America/Sao_Paulo',
            'available' => true,
            'active' => true,
            'full_record' => false,
            'created' => null,
            'rating' => (float)5,
            'role' => [
                'id' => (int)1,
                'name' => 'Admin',
                'modules' => [
                    (int)0 => [
                        'id' => (int)3,
                        'parent_id' => (int)2,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)1,
                            'role_id' => (int)1,
                            'module_id' => (int)3,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)2,
                            'parent_id' => (int)1,
                            'alias' => 'Users',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)1 => [
                        'id' => (int)4,
                        'parent_id' => (int)2,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)2,
                            'role_id' => (int)1,
                            'module_id' => (int)4,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)2,
                            'parent_id' => (int)1,
                            'alias' => 'Users',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)2 => [
                        'id' => (int)5,
                        'parent_id' => (int)2,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)3,
                            'role_id' => (int)1,
                            'module_id' => (int)5,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)2,
                            'parent_id' => (int)1,
                            'alias' => 'Users',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)3 => [
                        'id' => (int)6,
                        'parent_id' => (int)2,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)4,
                            'role_id' => (int)1,
                            'module_id' => (int)6,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)2,
                            'parent_id' => (int)1,
                            'alias' => 'Users',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)4 => [
                        'id' => (int)7,
                        'parent_id' => (int)2,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)5,
                            'role_id' => (int)1,
                            'module_id' => (int)7,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)2,
                            'parent_id' => (int)1,
                            'alias' => 'Users',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)5 => [
                        'id' => (int)8,
                        'parent_id' => (int)2,
                        'alias' => 'login',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)6,
                            'role_id' => (int)1,
                            'module_id' => (int)8,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)2,
                            'parent_id' => (int)1,
                            'alias' => 'Users',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)6 => [
                        'id' => (int)9,
                        'parent_id' => (int)2,
                        'alias' => 'logout',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)7,
                            'role_id' => (int)1,
                            'module_id' => (int)9,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)2,
                            'parent_id' => (int)1,
                            'alias' => 'Users',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)7 => [
                        'id' => (int)11,
                        'parent_id' => (int)10,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)8,
                            'role_id' => (int)1,
                            'module_id' => (int)11,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)10,
                            'parent_id' => (int)1,
                            'alias' => 'Regions',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)8 => [
                        'id' => (int)12,
                        'parent_id' => (int)10,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)9,
                            'role_id' => (int)1,
                            'module_id' => (int)12,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)10,
                            'parent_id' => (int)1,
                            'alias' => 'Regions',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)9 => [
                        'id' => (int)13,
                        'parent_id' => (int)10,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)10,
                            'role_id' => (int)1,
                            'module_id' => (int)13,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)10,
                            'parent_id' => (int)1,
                            'alias' => 'Regions',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)10 => [
                        'id' => (int)14,
                        'parent_id' => (int)10,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)11,
                            'role_id' => (int)1,
                            'module_id' => (int)14,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)10,
                            'parent_id' => (int)1,
                            'alias' => 'Regions',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)11 => [
                        'id' => (int)17,
                        'parent_id' => (int)16,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)12,
                            'role_id' => (int)1,
                            'module_id' => (int)17,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)16,
                            'parent_id' => (int)1,
                            'alias' => 'Corporates',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)12 => [
                        'id' => (int)18,
                        'parent_id' => (int)16,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)13,
                            'role_id' => (int)1,
                            'module_id' => (int)18,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)16,
                            'parent_id' => (int)1,
                            'alias' => 'Corporates',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)13 => [
                        'id' => (int)19,
                        'parent_id' => (int)16,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)14,
                            'role_id' => (int)1,
                            'module_id' => (int)19,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)16,
                            'parent_id' => (int)1,
                            'alias' => 'Corporates',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)14 => [
                        'id' => (int)20,
                        'parent_id' => (int)16,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)15,
                            'role_id' => (int)1,
                            'module_id' => (int)20,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)16,
                            'parent_id' => (int)1,
                            'alias' => 'Corporates',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)15 => [
                        'id' => (int)21,
                        'parent_id' => (int)16,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)16,
                            'role_id' => (int)1,
                            'module_id' => (int)21,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)16,
                            'parent_id' => (int)1,
                            'alias' => 'Corporates',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)16 => [
                        'id' => (int)35,
                        'parent_id' => (int)34,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)17,
                            'role_id' => (int)1,
                            'module_id' => (int)35,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)34,
                            'parent_id' => (int)1,
                            'alias' => 'Clients',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)17 => [
                        'id' => (int)36,
                        'parent_id' => (int)34,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)18,
                            'role_id' => (int)1,
                            'module_id' => (int)36,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)34,
                            'parent_id' => (int)1,
                            'alias' => 'Clients',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)18 => [
                        'id' => (int)37,
                        'parent_id' => (int)34,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)19,
                            'role_id' => (int)1,
                            'module_id' => (int)37,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)34,
                            'parent_id' => (int)1,
                            'alias' => 'Clients',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)19 => [
                        'id' => (int)38,
                        'parent_id' => (int)34,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)20,
                            'role_id' => (int)1,
                            'module_id' => (int)38,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)34,
                            'parent_id' => (int)1,
                            'alias' => 'Clients',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)20 => [
                        'id' => (int)39,
                        'parent_id' => (int)34,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)21,
                            'role_id' => (int)1,
                            'module_id' => (int)39,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)34,
                            'parent_id' => (int)1,
                            'alias' => 'Clients',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)21 => [
                        'id' => (int)49,
                        'parent_id' => (int)48,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)22,
                            'role_id' => (int)1,
                            'module_id' => (int)49,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)48,
                            'parent_id' => (int)1,
                            'alias' => 'Contracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)22 => [
                        'id' => (int)50,
                        'parent_id' => (int)48,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)23,
                            'role_id' => (int)1,
                            'module_id' => (int)50,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)48,
                            'parent_id' => (int)1,
                            'alias' => 'Contracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)23 => [
                        'id' => (int)51,
                        'parent_id' => (int)48,
                        'alias' => 'addPoints',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)24,
                            'role_id' => (int)1,
                            'module_id' => (int)51,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)48,
                            'parent_id' => (int)1,
                            'alias' => 'Contracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)24 => [
                        'id' => (int)52,
                        'parent_id' => (int)48,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)25,
                            'role_id' => (int)1,
                            'module_id' => (int)52,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)48,
                            'parent_id' => (int)1,
                            'alias' => 'Contracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)25 => [
                        'id' => (int)53,
                        'parent_id' => (int)48,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)26,
                            'role_id' => (int)1,
                            'module_id' => (int)53,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)48,
                            'parent_id' => (int)1,
                            'alias' => 'Contracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)26 => [
                        'id' => (int)54,
                        'parent_id' => (int)48,
                        'alias' => 'active',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)27,
                            'role_id' => (int)1,
                            'module_id' => (int)54,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)48,
                            'parent_id' => (int)1,
                            'alias' => 'Contracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)27 => [
                        'id' => (int)58,
                        'parent_id' => (int)57,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)28,
                            'role_id' => (int)1,
                            'module_id' => (int)58,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)57,
                            'parent_id' => (int)1,
                            'alias' => 'Skills',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)28 => [
                        'id' => (int)59,
                        'parent_id' => (int)57,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)29,
                            'role_id' => (int)1,
                            'module_id' => (int)59,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)57,
                            'parent_id' => (int)1,
                            'alias' => 'Skills',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)29 => [
                        'id' => (int)60,
                        'parent_id' => (int)57,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)30,
                            'role_id' => (int)1,
                            'module_id' => (int)60,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)57,
                            'parent_id' => (int)1,
                            'alias' => 'Skills',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)30 => [
                        'id' => (int)61,
                        'parent_id' => (int)57,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)31,
                            'role_id' => (int)1,
                            'module_id' => (int)61,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)57,
                            'parent_id' => (int)1,
                            'alias' => 'Skills',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)31 => [
                        'id' => (int)62,
                        'parent_id' => (int)57,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)32,
                            'role_id' => (int)1,
                            'module_id' => (int)62,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)57,
                            'parent_id' => (int)1,
                            'alias' => 'Skills',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)32 => [
                        'id' => (int)64,
                        'parent_id' => (int)63,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)33,
                            'role_id' => (int)1,
                            'module_id' => (int)64,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)33 => [
                        'id' => (int)65,
                        'parent_id' => (int)63,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)34,
                            'role_id' => (int)1,
                            'module_id' => (int)65,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)34 => [
                        'id' => (int)66,
                        'parent_id' => (int)63,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)35,
                            'role_id' => (int)1,
                            'module_id' => (int)66,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)35 => [
                        'id' => (int)67,
                        'parent_id' => (int)63,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)36,
                            'role_id' => (int)1,
                            'module_id' => (int)67,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)36 => [
                        'id' => (int)68,
                        'parent_id' => (int)63,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)37,
                            'role_id' => (int)1,
                            'module_id' => (int)68,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)37 => [
                        'id' => (int)120,
                        'parent_id' => (int)119,
                        'alias' => 'Permissions',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)38,
                            'role_id' => (int)1,
                            'module_id' => (int)120,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)119,
                            'parent_id' => (int)1,
                            'alias' => 'PermissionsManager',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)38 => [
                        'id' => (int)123,
                        'parent_id' => (int)119,
                        'alias' => 'Roles',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)39,
                            'role_id' => (int)1,
                            'module_id' => (int)123,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)119,
                            'parent_id' => (int)1,
                            'alias' => 'PermissionsManager',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)39 => [
                        'id' => (int)133,
                        'parent_id' => (int)132,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)40,
                            'role_id' => (int)1,
                            'module_id' => (int)133,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)40 => [
                        'id' => (int)134,
                        'parent_id' => (int)132,
                        'alias' => 'provisionTec',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)41,
                            'role_id' => (int)1,
                            'module_id' => (int)134,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)41 => [
                        'id' => (int)89,
                        'parent_id' => (int)88,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)42,
                            'role_id' => (int)1,
                            'module_id' => (int)89,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)88,
                            'parent_id' => (int)1,
                            'alias' => 'Roles',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)42 => [
                        'id' => (int)90,
                        'parent_id' => (int)88,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)43,
                            'role_id' => (int)1,
                            'module_id' => (int)90,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)88,
                            'parent_id' => (int)1,
                            'alias' => 'Roles',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)43 => [
                        'id' => (int)91,
                        'parent_id' => (int)88,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)44,
                            'role_id' => (int)1,
                            'module_id' => (int)91,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)88,
                            'parent_id' => (int)1,
                            'alias' => 'Roles',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)44 => [
                        'id' => (int)92,
                        'parent_id' => (int)88,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)45,
                            'role_id' => (int)1,
                            'module_id' => (int)92,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)88,
                            'parent_id' => (int)1,
                            'alias' => 'Roles',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)45 => [
                        'id' => (int)93,
                        'parent_id' => (int)88,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)46,
                            'role_id' => (int)1,
                            'module_id' => (int)93,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)88,
                            'parent_id' => (int)1,
                            'alias' => 'Roles',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)46 => [
                        'id' => (int)136,
                        'parent_id' => (int)132,
                        'alias' => 'ups',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)47,
                            'role_id' => (int)1,
                            'module_id' => (int)136,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)47 => [
                        'id' => (int)137,
                        'parent_id' => (int)132,
                        'alias' => 'filial',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)48,
                            'role_id' => (int)1,
                            'module_id' => (int)137,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)48 => [
                        'id' => (int)138,
                        'parent_id' => (int)132,
                        'alias' => 'specialists',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)49,
                            'role_id' => (int)1,
                            'module_id' => (int)138,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)49 => [
                        'id' => (int)139,
                        'parent_id' => (int)132,
                        'alias' => 'specialistsCities',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)50,
                            'role_id' => (int)1,
                            'module_id' => (int)139,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)50 => [
                        'id' => (int)140,
                        'parent_id' => (int)132,
                        'alias' => 'sla',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)51,
                            'role_id' => (int)1,
                            'module_id' => (int)140,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)51 => [
                        'id' => (int)142,
                        'parent_id' => (int)132,
                        'alias' => 'mapSpecialists',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)52,
                            'role_id' => (int)1,
                            'module_id' => (int)142,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)52 => [
                        'id' => (int)144,
                        'parent_id' => (int)143,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)53,
                            'role_id' => (int)1,
                            'module_id' => (int)144,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)143,
                            'parent_id' => (int)1,
                            'alias' => 'Documents',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)53 => [
                        'id' => (int)145,
                        'parent_id' => (int)143,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)54,
                            'role_id' => (int)1,
                            'module_id' => (int)145,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)143,
                            'parent_id' => (int)1,
                            'alias' => 'Documents',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)54 => [
                        'id' => (int)147,
                        'parent_id' => (int)143,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)55,
                            'role_id' => (int)1,
                            'module_id' => (int)147,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)143,
                            'parent_id' => (int)1,
                            'alias' => 'Documents',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)55 => [
                        'id' => (int)148,
                        'parent_id' => (int)143,
                        'alias' => 'editField',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)56,
                            'role_id' => (int)1,
                            'module_id' => (int)148,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)143,
                            'parent_id' => (int)1,
                            'alias' => 'Documents',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)56 => [
                        'id' => (int)149,
                        'parent_id' => (int)143,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)57,
                            'role_id' => (int)1,
                            'module_id' => (int)149,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)143,
                            'parent_id' => (int)1,
                            'alias' => 'Documents',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)57 => [
                        'id' => (int)151,
                        'parent_id' => (int)150,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)58,
                            'role_id' => (int)1,
                            'module_id' => (int)151,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)150,
                            'parent_id' => (int)1,
                            'alias' => 'Specialists',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)58 => [
                        'id' => (int)152,
                        'parent_id' => (int)150,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)59,
                            'role_id' => (int)1,
                            'module_id' => (int)152,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)150,
                            'parent_id' => (int)1,
                            'alias' => 'Specialists',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)59 => [
                        'id' => (int)153,
                        'parent_id' => (int)150,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)60,
                            'role_id' => (int)1,
                            'module_id' => (int)153,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)150,
                            'parent_id' => (int)1,
                            'alias' => 'Specialists',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)60 => [
                        'id' => (int)154,
                        'parent_id' => (int)150,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)61,
                            'role_id' => (int)1,
                            'module_id' => (int)154,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)150,
                            'parent_id' => (int)1,
                            'alias' => 'Specialists',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)61 => [
                        'id' => (int)157,
                        'parent_id' => (int)132,
                        'alias' => 'listSpecialists',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)62,
                            'role_id' => (int)1,
                            'module_id' => (int)157,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)62 => [
                        'id' => (int)158,
                        'parent_id' => (int)132,
                        'alias' => 'avg',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)63,
                            'role_id' => (int)1,
                            'module_id' => (int)158,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)63 => [
                        'id' => (int)165,
                        'parent_id' => (int)63,
                        'alias' => 'newHistoric',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)64,
                            'role_id' => (int)1,
                            'module_id' => (int)165,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)64 => [
                        'id' => (int)166,
                        'parent_id' => (int)63,
                        'alias' => 'setResponsible',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)65,
                            'role_id' => (int)1,
                            'module_id' => (int)166,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)65 => [
                        'id' => (int)167,
                        'parent_id' => (int)63,
                        'alias' => 'editSpecialist',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)66,
                            'role_id' => (int)1,
                            'module_id' => (int)167,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)66 => [
                        'id' => (int)168,
                        'parent_id' => (int)63,
                        'alias' => 'reschedule',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)67,
                            'role_id' => (int)1,
                            'module_id' => (int)168,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)67 => [
                        'id' => (int)169,
                        'parent_id' => (int)63,
                        'alias' => 'canceled',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)68,
                            'role_id' => (int)1,
                            'module_id' => (int)169,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)68 => [
                        'id' => (int)170,
                        'parent_id' => (int)63,
                        'alias' => 'ongoing',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)69,
                            'role_id' => (int)1,
                            'module_id' => (int)170,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)69 => [
                        'id' => (int)171,
                        'parent_id' => (int)63,
                        'alias' => 'checkin',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)70,
                            'role_id' => (int)1,
                            'module_id' => (int)171,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)70 => [
                        'id' => (int)172,
                        'parent_id' => (int)63,
                        'alias' => 'checkout',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)71,
                            'role_id' => (int)1,
                            'module_id' => (int)172,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)71 => [
                        'id' => (int)173,
                        'parent_id' => (int)63,
                        'alias' => 'unproductive',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)72,
                            'role_id' => (int)1,
                            'module_id' => (int)173,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)72 => [
                        'id' => (int)174,
                        'parent_id' => (int)63,
                        'alias' => 'invalidated',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)73,
                            'role_id' => (int)1,
                            'module_id' => (int)174,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)73 => [
                        'id' => (int)175,
                        'parent_id' => (int)63,
                        'alias' => 'recalculate',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)74,
                            'role_id' => (int)1,
                            'module_id' => (int)175,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)74 => [
                        'id' => (int)209,
                        'parent_id' => (int)63,
                        'alias' => 'addCorporate',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)75,
                            'role_id' => (int)1,
                            'module_id' => (int)209,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)75 => [
                        'id' => (int)210,
                        'parent_id' => (int)63,
                        'alias' => 'addResident',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)76,
                            'role_id' => (int)1,
                            'module_id' => (int)210,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)76 => [
                        'id' => (int)15,
                        'parent_id' => (int)10,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)77,
                            'role_id' => (int)1,
                            'module_id' => (int)15,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)10,
                            'parent_id' => (int)1,
                            'alias' => 'Regions',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)77 => [
                        'id' => (int)23,
                        'parent_id' => (int)22,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)78,
                            'role_id' => (int)1,
                            'module_id' => (int)23,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)22,
                            'parent_id' => (int)1,
                            'alias' => 'Extracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)78 => [
                        'id' => (int)24,
                        'parent_id' => (int)22,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)79,
                            'role_id' => (int)1,
                            'module_id' => (int)24,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)22,
                            'parent_id' => (int)1,
                            'alias' => 'Extracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)79 => [
                        'id' => (int)25,
                        'parent_id' => (int)22,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)80,
                            'role_id' => (int)1,
                            'module_id' => (int)25,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)22,
                            'parent_id' => (int)1,
                            'alias' => 'Extracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)80 => [
                        'id' => (int)26,
                        'parent_id' => (int)22,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)81,
                            'role_id' => (int)1,
                            'module_id' => (int)26,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)22,
                            'parent_id' => (int)1,
                            'alias' => 'Extracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)81 => [
                        'id' => (int)27,
                        'parent_id' => (int)22,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)82,
                            'role_id' => (int)1,
                            'module_id' => (int)27,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)22,
                            'parent_id' => (int)1,
                            'alias' => 'Extracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)82 => [
                        'id' => (int)29,
                        'parent_id' => (int)28,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)83,
                            'role_id' => (int)1,
                            'module_id' => (int)29,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)28,
                            'parent_id' => (int)1,
                            'alias' => 'States',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)83 => [
                        'id' => (int)30,
                        'parent_id' => (int)28,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)84,
                            'role_id' => (int)1,
                            'module_id' => (int)30,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)28,
                            'parent_id' => (int)1,
                            'alias' => 'States',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)84 => [
                        'id' => (int)31,
                        'parent_id' => (int)28,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)85,
                            'role_id' => (int)1,
                            'module_id' => (int)31,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)28,
                            'parent_id' => (int)1,
                            'alias' => 'States',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)85 => [
                        'id' => (int)32,
                        'parent_id' => (int)28,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)86,
                            'role_id' => (int)1,
                            'module_id' => (int)32,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)28,
                            'parent_id' => (int)1,
                            'alias' => 'States',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)86 => [
                        'id' => (int)33,
                        'parent_id' => (int)28,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)87,
                            'role_id' => (int)1,
                            'module_id' => (int)33,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)28,
                            'parent_id' => (int)1,
                            'alias' => 'States',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)87 => [
                        'id' => (int)40,
                        'parent_id' => (int)34,
                        'alias' => 'login',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)88,
                            'role_id' => (int)1,
                            'module_id' => (int)40,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)34,
                            'parent_id' => (int)1,
                            'alias' => 'Clients',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)88 => [
                        'id' => (int)41,
                        'parent_id' => (int)34,
                        'alias' => 'logout',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)89,
                            'role_id' => (int)1,
                            'module_id' => (int)41,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)34,
                            'parent_id' => (int)1,
                            'alias' => 'Clients',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)89 => [
                        'id' => (int)43,
                        'parent_id' => (int)42,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)90,
                            'role_id' => (int)1,
                            'module_id' => (int)43,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)42,
                            'parent_id' => (int)1,
                            'alias' => 'Services',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)90 => [
                        'id' => (int)44,
                        'parent_id' => (int)42,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)91,
                            'role_id' => (int)1,
                            'module_id' => (int)44,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)42,
                            'parent_id' => (int)1,
                            'alias' => 'Services',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)91 => [
                        'id' => (int)45,
                        'parent_id' => (int)42,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)92,
                            'role_id' => (int)1,
                            'module_id' => (int)45,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)42,
                            'parent_id' => (int)1,
                            'alias' => 'Services',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)92 => [
                        'id' => (int)46,
                        'parent_id' => (int)42,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)93,
                            'role_id' => (int)1,
                            'module_id' => (int)46,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)42,
                            'parent_id' => (int)1,
                            'alias' => 'Services',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)93 => [
                        'id' => (int)47,
                        'parent_id' => (int)42,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)94,
                            'role_id' => (int)1,
                            'module_id' => (int)47,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)42,
                            'parent_id' => (int)1,
                            'alias' => 'Services',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)94 => [
                        'id' => (int)56,
                        'parent_id' => (int)55,
                        'alias' => 'display',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)95,
                            'role_id' => (int)1,
                            'module_id' => (int)56,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)55,
                            'parent_id' => (int)1,
                            'alias' => 'Pages',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)95 => [
                        'id' => (int)70,
                        'parent_id' => (int)69,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)96,
                            'role_id' => (int)1,
                            'module_id' => (int)70,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)69,
                            'parent_id' => (int)1,
                            'alias' => 'Addresses',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)96 => [
                        'id' => (int)71,
                        'parent_id' => (int)69,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)97,
                            'role_id' => (int)1,
                            'module_id' => (int)71,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)69,
                            'parent_id' => (int)1,
                            'alias' => 'Addresses',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)97 => [
                        'id' => (int)72,
                        'parent_id' => (int)69,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)98,
                            'role_id' => (int)1,
                            'module_id' => (int)72,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)69,
                            'parent_id' => (int)1,
                            'alias' => 'Addresses',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)98 => [
                        'id' => (int)73,
                        'parent_id' => (int)69,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)99,
                            'role_id' => (int)1,
                            'module_id' => (int)73,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)69,
                            'parent_id' => (int)1,
                            'alias' => 'Addresses',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)99 => [
                        'id' => (int)74,
                        'parent_id' => (int)69,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)100,
                            'role_id' => (int)1,
                            'module_id' => (int)74,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)69,
                            'parent_id' => (int)1,
                            'alias' => 'Addresses',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)100 => [
                        'id' => (int)76,
                        'parent_id' => (int)75,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)101,
                            'role_id' => (int)1,
                            'module_id' => (int)76,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)75,
                            'parent_id' => (int)1,
                            'alias' => 'Coupons',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)101 => [
                        'id' => (int)77,
                        'parent_id' => (int)75,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)102,
                            'role_id' => (int)1,
                            'module_id' => (int)77,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)75,
                            'parent_id' => (int)1,
                            'alias' => 'Coupons',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)102 => [
                        'id' => (int)78,
                        'parent_id' => (int)75,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)103,
                            'role_id' => (int)1,
                            'module_id' => (int)78,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)75,
                            'parent_id' => (int)1,
                            'alias' => 'Coupons',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)103 => [
                        'id' => (int)79,
                        'parent_id' => (int)75,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)104,
                            'role_id' => (int)1,
                            'module_id' => (int)79,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)75,
                            'parent_id' => (int)1,
                            'alias' => 'Coupons',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)104 => [
                        'id' => (int)80,
                        'parent_id' => (int)75,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)105,
                            'role_id' => (int)1,
                            'module_id' => (int)80,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)75,
                            'parent_id' => (int)1,
                            'alias' => 'Coupons',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)105 => [
                        'id' => (int)82,
                        'parent_id' => (int)81,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)106,
                            'role_id' => (int)1,
                            'module_id' => (int)82,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)81,
                            'parent_id' => (int)1,
                            'alias' => 'Cities',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)106 => [
                        'id' => (int)83,
                        'parent_id' => (int)81,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)107,
                            'role_id' => (int)1,
                            'module_id' => (int)83,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)81,
                            'parent_id' => (int)1,
                            'alias' => 'Cities',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)107 => [
                        'id' => (int)84,
                        'parent_id' => (int)81,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)108,
                            'role_id' => (int)1,
                            'module_id' => (int)84,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)81,
                            'parent_id' => (int)1,
                            'alias' => 'Cities',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)108 => [
                        'id' => (int)85,
                        'parent_id' => (int)81,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)109,
                            'role_id' => (int)1,
                            'module_id' => (int)85,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)81,
                            'parent_id' => (int)1,
                            'alias' => 'Cities',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)109 => [
                        'id' => (int)86,
                        'parent_id' => (int)81,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)110,
                            'role_id' => (int)1,
                            'module_id' => (int)86,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)81,
                            'parent_id' => (int)1,
                            'alias' => 'Cities',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)110 => [
                        'id' => (int)95,
                        'parent_id' => (int)94,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)111,
                            'role_id' => (int)1,
                            'module_id' => (int)95,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)94,
                            'parent_id' => (int)1,
                            'alias' => 'Reviews',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)111 => [
                        'id' => (int)96,
                        'parent_id' => (int)94,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)112,
                            'role_id' => (int)1,
                            'module_id' => (int)96,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)94,
                            'parent_id' => (int)1,
                            'alias' => 'Reviews',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)112 => [
                        'id' => (int)97,
                        'parent_id' => (int)94,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)113,
                            'role_id' => (int)1,
                            'module_id' => (int)97,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)94,
                            'parent_id' => (int)1,
                            'alias' => 'Reviews',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)113 => [
                        'id' => (int)98,
                        'parent_id' => (int)94,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)114,
                            'role_id' => (int)1,
                            'module_id' => (int)98,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)94,
                            'parent_id' => (int)1,
                            'alias' => 'Reviews',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)114 => [
                        'id' => (int)99,
                        'parent_id' => (int)94,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)115,
                            'role_id' => (int)1,
                            'module_id' => (int)99,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)94,
                            'parent_id' => (int)1,
                            'alias' => 'Reviews',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)115 => [
                        'id' => (int)103,
                        'parent_id' => (int)102,
                        'alias' => 'Admin',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)116,
                            'role_id' => (int)1,
                            'module_id' => (int)103,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)102,
                            'parent_id' => (int)1,
                            'alias' => 'AuditLog',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)116 => [
                        'id' => (int)105,
                        'parent_id' => (int)104,
                        'alias' => 'Toolbar',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)117,
                            'role_id' => (int)1,
                            'module_id' => (int)105,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)104,
                            'parent_id' => (int)1,
                            'alias' => 'DebugKit',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)117 => [
                        'id' => (int)107,
                        'parent_id' => (int)104,
                        'alias' => 'Requests',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)118,
                            'role_id' => (int)1,
                            'module_id' => (int)107,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)104,
                            'parent_id' => (int)1,
                            'alias' => 'DebugKit',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)118 => [
                        'id' => (int)109,
                        'parent_id' => (int)104,
                        'alias' => 'Composer',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)119,
                            'role_id' => (int)1,
                            'module_id' => (int)109,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)104,
                            'parent_id' => (int)1,
                            'alias' => 'DebugKit',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)119 => [
                        'id' => (int)111,
                        'parent_id' => (int)104,
                        'alias' => 'MailPreview',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)120,
                            'role_id' => (int)1,
                            'module_id' => (int)111,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)104,
                            'parent_id' => (int)1,
                            'alias' => 'DebugKit',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)120 => [
                        'id' => (int)115,
                        'parent_id' => (int)104,
                        'alias' => 'Panels',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)121,
                            'role_id' => (int)1,
                            'module_id' => (int)115,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)104,
                            'parent_id' => (int)1,
                            'alias' => 'DebugKit',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)121 => [
                        'id' => (int)131,
                        'parent_id' => (int)130,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)122,
                            'role_id' => (int)1,
                            'module_id' => (int)131,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)130,
                            'parent_id' => (int)1,
                            'alias' => 'Configurations',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)122 => [
                        'id' => (int)146,
                        'parent_id' => (int)143,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)123,
                            'role_id' => (int)1,
                            'module_id' => (int)146,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)143,
                            'parent_id' => (int)1,
                            'alias' => 'Documents',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)123 => [
                        'id' => (int)155,
                        'parent_id' => (int)69,
                        'alias' => 'listStates',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)124,
                            'role_id' => (int)1,
                            'module_id' => (int)155,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)69,
                            'parent_id' => (int)1,
                            'alias' => 'Addresses',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)124 => [
                        'id' => (int)156,
                        'parent_id' => (int)69,
                        'alias' => 'listCities',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)125,
                            'role_id' => (int)1,
                            'module_id' => (int)156,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)69,
                            'parent_id' => (int)1,
                            'alias' => 'Addresses',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)125 => [
                        'id' => (int)160,
                        'parent_id' => (int)130,
                        'alias' => 'saveList',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)126,
                            'role_id' => (int)1,
                            'module_id' => (int)160,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)130,
                            'parent_id' => (int)1,
                            'alias' => 'Configurations',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)126 => [
                        'id' => (int)161,
                        'parent_id' => (int)130,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)127,
                            'role_id' => (int)1,
                            'module_id' => (int)161,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)130,
                            'parent_id' => (int)1,
                            'alias' => 'Configurations',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)127 => [
                        'id' => (int)163,
                        'parent_id' => (int)162,
                        'alias' => 'editField',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)128,
                            'role_id' => (int)1,
                            'module_id' => (int)163,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)162,
                            'parent_id' => (int)1,
                            'alias' => 'Medias',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)128 => [
                        'id' => (int)164,
                        'parent_id' => (int)162,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)129,
                            'role_id' => (int)1,
                            'module_id' => (int)164,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)162,
                            'parent_id' => (int)1,
                            'alias' => 'Medias',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)129 => [
                        'id' => (int)177,
                        'parent_id' => (int)176,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)130,
                            'role_id' => (int)1,
                            'module_id' => (int)177,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)176,
                            'parent_id' => (int)1,
                            'alias' => 'Comments',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)130 => [
                        'id' => (int)178,
                        'parent_id' => (int)176,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)131,
                            'role_id' => (int)1,
                            'module_id' => (int)178,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)176,
                            'parent_id' => (int)1,
                            'alias' => 'Comments',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)131 => [
                        'id' => (int)179,
                        'parent_id' => (int)176,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)132,
                            'role_id' => (int)1,
                            'module_id' => (int)179,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)176,
                            'parent_id' => (int)1,
                            'alias' => 'Comments',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)132 => [
                        'id' => (int)180,
                        'parent_id' => (int)176,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)133,
                            'role_id' => (int)1,
                            'module_id' => (int)180,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)176,
                            'parent_id' => (int)1,
                            'alias' => 'Comments',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)133 => [
                        'id' => (int)181,
                        'parent_id' => (int)176,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)134,
                            'role_id' => (int)1,
                            'module_id' => (int)181,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)176,
                            'parent_id' => (int)1,
                            'alias' => 'Comments',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)134 => [
                        'id' => (int)183,
                        'parent_id' => (int)182,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)135,
                            'role_id' => (int)1,
                            'module_id' => (int)183,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)182,
                            'parent_id' => (int)1,
                            'alias' => 'Dashboard',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)135 => [
                        'id' => (int)186,
                        'parent_id' => (int)185,
                        'alias' => 'Users',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)136,
                            'role_id' => (int)1,
                            'module_id' => (int)186,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)185,
                            'parent_id' => (int)1,
                            'alias' => 'Rest',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)136 => [
                        'id' => (int)189,
                        'parent_id' => (int)185,
                        'alias' => 'Contracts',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)137,
                            'role_id' => (int)1,
                            'module_id' => (int)189,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)185,
                            'parent_id' => (int)1,
                            'alias' => 'Rest',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)137 => [
                        'id' => (int)191,
                        'parent_id' => (int)185,
                        'alias' => 'Addresses',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)138,
                            'role_id' => (int)1,
                            'module_id' => (int)191,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)185,
                            'parent_id' => (int)1,
                            'alias' => 'Rest',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)138 => [
                        'id' => (int)200,
                        'parent_id' => (int)199,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)139,
                            'role_id' => (int)1,
                            'module_id' => (int)200,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)199,
                            'parent_id' => (int)1,
                            'alias' => 'Invoices',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)139 => [
                        'id' => (int)201,
                        'parent_id' => (int)199,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)140,
                            'role_id' => (int)1,
                            'module_id' => (int)201,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)199,
                            'parent_id' => (int)1,
                            'alias' => 'Invoices',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)140 => [
                        'id' => (int)202,
                        'parent_id' => (int)130,
                        'alias' => 'financial',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)141,
                            'role_id' => (int)1,
                            'module_id' => (int)202,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)130,
                            'parent_id' => (int)1,
                            'alias' => 'Configurations',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)141 => [
                        'id' => (int)204,
                        'parent_id' => (int)203,
                        'alias' => 'occurrences',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)142,
                            'role_id' => (int)1,
                            'module_id' => (int)204,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)203,
                            'parent_id' => (int)1,
                            'alias' => 'Financial',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)142 => [
                        'id' => (int)205,
                        'parent_id' => (int)203,
                        'alias' => 'invoices',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)143,
                            'role_id' => (int)1,
                            'module_id' => (int)205,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)203,
                            'parent_id' => (int)1,
                            'alias' => 'Financial',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)143 => [
                        'id' => (int)206,
                        'parent_id' => (int)203,
                        'alias' => 'viewInvoice',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)144,
                            'role_id' => (int)1,
                            'module_id' => (int)206,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)203,
                            'parent_id' => (int)1,
                            'alias' => 'Financial',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)144 => [
                        'id' => (int)207,
                        'parent_id' => (int)203,
                        'alias' => 'editInvoice',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)145,
                            'role_id' => (int)1,
                            'module_id' => (int)207,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)203,
                            'parent_id' => (int)1,
                            'alias' => 'Financial',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)145 => [
                        'id' => (int)208,
                        'parent_id' => (int)162,
                        'alias' => 'validation',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)146,
                            'role_id' => (int)1,
                            'module_id' => (int)208,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)162,
                            'parent_id' => (int)1,
                            'alias' => 'Medias',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)146 => [
                        'id' => (int)211,
                        'parent_id' => (int)69,
                        'alias' => 'addMultiple',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)147,
                            'role_id' => (int)1,
                            'module_id' => (int)211,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)69,
                            'parent_id' => (int)1,
                            'alias' => 'Addresses',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)147 => [
                        'id' => (int)218,
                        'parent_id' => (int)217,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)148,
                            'role_id' => (int)1,
                            'module_id' => (int)218,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)217,
                            'parent_id' => (int)1,
                            'alias' => 'Messages',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)148 => [
                        'id' => (int)219,
                        'parent_id' => (int)217,
                        'alias' => 'view',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)149,
                            'role_id' => (int)1,
                            'module_id' => (int)219,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)217,
                            'parent_id' => (int)1,
                            'alias' => 'Messages',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)149 => [
                        'id' => (int)220,
                        'parent_id' => (int)217,
                        'alias' => 'add',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)150,
                            'role_id' => (int)1,
                            'module_id' => (int)220,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)217,
                            'parent_id' => (int)1,
                            'alias' => 'Messages',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)150 => [
                        'id' => (int)221,
                        'parent_id' => (int)217,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)151,
                            'role_id' => (int)1,
                            'module_id' => (int)221,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)217,
                            'parent_id' => (int)1,
                            'alias' => 'Messages',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)151 => [
                        'id' => (int)222,
                        'parent_id' => (int)217,
                        'alias' => 'delete',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)152,
                            'role_id' => (int)1,
                            'module_id' => (int)222,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)217,
                            'parent_id' => (int)1,
                            'alias' => 'Messages',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)152 => [
                        'id' => (int)224,
                        'parent_id' => (int)132,
                        'alias' => 'mapHot',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)153,
                            'role_id' => (int)1,
                            'module_id' => (int)224,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)153 => [
                        'id' => (int)227,
                        'parent_id' => (int)132,
                        'alias' => 'occurrences',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)154,
                            'role_id' => (int)1,
                            'module_id' => (int)227,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)132,
                            'parent_id' => (int)1,
                            'alias' => 'Bi',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)154 => [
                        'id' => (int)229,
                        'parent_id' => (int)228,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)155,
                            'role_id' => (int)1,
                            'module_id' => (int)229,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)228,
                            'parent_id' => (int)1,
                            'alias' => 'Comercial',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)155 => [
                        'id' => (int)223,
                        'parent_id' => (int)63,
                        'alias' => 'adjustGeo',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)156,
                            'role_id' => (int)1,
                            'module_id' => (int)223,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)156 => [
                        'id' => (int)226,
                        'parent_id' => (int)63,
                        'alias' => 'searchTech',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)157,
                            'role_id' => (int)1,
                            'module_id' => (int)226,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)157 => [
                        'id' => (int)232,
                        'parent_id' => (int)231,
                        'alias' => 'index',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)158,
                            'role_id' => (int)1,
                            'module_id' => (int)232,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)231,
                            'parent_id' => (int)1,
                            'alias' => 'Ratings',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)158 => [
                        'id' => (int)233,
                        'parent_id' => (int)231,
                        'alias' => 'edit',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)159,
                            'role_id' => (int)1,
                            'module_id' => (int)233,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)231,
                            'parent_id' => (int)1,
                            'alias' => 'Ratings',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)159 => [
                        'id' => (int)234,
                        'parent_id' => (int)63,
                        'alias' => 'finished',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)160,
                            'role_id' => (int)1,
                            'module_id' => (int)234,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)160 => [
                        'id' => (int)235,
                        'parent_id' => (int)63,
                        'alias' => 'operationValidation',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)161,
                            'role_id' => (int)1,
                            'module_id' => (int)235,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)161 => [
                        'id' => (int)236,
                        'parent_id' => (int)63,
                        'alias' => 'paymentCheck',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)162,
                            'role_id' => (int)1,
                            'module_id' => (int)236,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)162 => [
                        'id' => (int)237,
                        'parent_id' => (int)63,
                        'alias' => 'operationCheck',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)163,
                            'role_id' => (int)1,
                            'module_id' => (int)237,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)163 => [
                        'id' => (int)238,
                        'parent_id' => (int)63,
                        'alias' => 'bulkAlterStatus',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)164,
                            'role_id' => (int)1,
                            'module_id' => (int)238,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)164 => [
                        'id' => (int)239,
                        'parent_id' => (int)48,
                        'alias' => 'recalculate',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)165,
                            'role_id' => (int)1,
                            'module_id' => (int)239,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)48,
                            'parent_id' => (int)1,
                            'alias' => 'Contracts',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)165 => [
                        'id' => (int)241,
                        'parent_id' => (int)63,
                        'alias' => 'validated',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)166,
                            'role_id' => (int)1,
                            'module_id' => (int)241,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ],
                    (int)166 => [
                        'id' => (int)253,
                        'parent_id' => (int)63,
                        'alias' => 'editProcedure',
                        'created' => null,
                        'modified' => null,
                        '_joinData' => [
                            'id' => (int)167,
                            'role_id' => (int)1,
                            'module_id' => (int)253,
                            '_create' => '0',
                            '_read' => '0',
                            '_update' => '0',
                            '_delete' => '0',
                            'created' => null,
                            'modified' => null
                        ],
                        'parent_module' => [
                            'id' => (int)63,
                            'parent_id' => (int)1,
                            'alias' => 'Occurrences',
                            'created' => null,
                            'modified' => null
                        ]
                    ]
                ]
            ],
            'individual' => [
                'id' => (int)255,
                'user_id' => (int)255,
                'photo' => 'https://firebasestorage.googleapis.com/v0/b/client-findup.appspot.com/o/TECHWEB%2Fdesenvolvimento@findup.com.br%2Fprofile.jpg?alt=media&token=dccd7c84-a57a-4f59-9eaf-e8a63d7bab12',
                'cpf' => '11122211111',
                'rg' => '',
                'birthdate' => null,
                'gender' => 'MALE',
                'show_phone' => true,
                'individuals_test_id' => null,
                'description' => null,
                'active' => '1',
                'created' => null
            ]
        ];
    }
}