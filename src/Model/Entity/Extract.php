<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Extract Entity
 *
 * @property int $id
 * @property string $type
 * @property \Cake\I18n\FrozenTime $date
 * @property float $cripto_coin
 * @property float $real_coin
 * @property \Cake\I18n\FrozenTime $created
 * @property int $user_id
 * @property float $cripto_now_price_usd
 * @property float $cripto_now_price_real
 *
 * @property \App\Model\Entity\User $user
 */
class Extract extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
