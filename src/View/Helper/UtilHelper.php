<?php

/**
 * Created by PhpStorm.
 * User: Haendel
 * Date: 05/10/2016
 * Time: 11:01
 */

namespace App\View\Helper;


use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class UtilHelper extends Helper
{
    /**
     * Retornar imagens de acordo com o tipo de arquivo
     */
    public function iconPreview($file)
    {
        if (in_array($file->file_type, ['image/png', 'image/jpg', 'image/jpeg', 'PNG', 'png', 'jpg'])) {
            return "<img class='card-img-top' src='$file->filepath'>";
        } elseif (in_array($file->file_type, ['application/pdf'])) {
            return '<div class="card-img-top"><i class="fa fa-file-pdf-o"></i></div>';
        } else {
            return '<div class="card-img-top"><i class="fa fa-image"></i></span>';
        }
    }

    /**
     * Cores de acordo com a metrica a ser avaliada
     */
    public function classColorMetric($value, $danger, $alert, $success)
    {
        if ($danger[0] <= $value && $danger[1] >= $value) {
            return 'bg-red';
        } elseif ($alert[0] <= $value && $alert[1] >= $value) {
            return 'bg-yellow';
        } elseif ($success[0] <= $value && (($success[1] >= $value) || $success[1] == 0)) {
            return 'bg-green';
        } else {
            return 'bg-aqua';
        }
    }


    /**
     * Converter para o formato br e setar a timezone
     * @param $date
     * @param string $timezone
     * @return mixed
     */
    public function convertDate($date, $timezone = 'America/Sao_Paulo', $type = 'full')
    {
        return $this->convertDateTimezone($date, $timezone, $type);
    }

    /**
     * @param $date
     * @param string $timezone
     * @param string $type
     * @return string
     * @deprecated Usar convertDate
     */
    public function convertDateTimezone($date, $timezone = 'America/Sao_Paulo', $type = 'full')
    {
        if ($type == 'full') {
            $format = 'd/m/Y H:i';
        } elseif ($type == 'date') {
            $format = 'd/m/Y';
        } elseif ($type == 'time') {
            $format = 'H:i';
        } else {
            $format = 'd/m/Y H:i';
        }

        if ($date) {
            if ($timezone) {
                return $date->setTimezone(new \DateTimeZone($timezone))->format($format);
            } else {
                return $date->setTimezone(new \DateTimeZone('America/Sao_Paulo'))->format($format);
            }
        } else {
            return '';
        }
    }

    /**
     * Converter minutos para horas
     */
    public function minutesToHours($minutes)
    {
        $h = floor($minutes / 60) ? floor($minutes / 60) . ' h' : '';
        $m = $minutes % 60 ? $minutes % 60 . ' m' : '';
        return $h && $m ? $h . ' ' . $m : $h . $m;
    }

    /**
     * Verificar se a validação expirou
     */
    public function getValidateExpired($historic, $status)
    {
        if ($status == 'AWAITING_VALIDATION') {
            foreach ($historic as $his) {
                if ($his->description == 'AWAITING_VALIDATION') {
                    $now = new \DateTime();

                    if ($now->diff($his->created)->d >= 4) {
                        return 'style="background: rgba(232, 78, 64, 0.09) !important; border-left: 5px solid #e84e40;"';
                    } else {
                        return '';
                    };
                }
            }
        }
    }

    /**
     * TExtos a serem mostrados em tooltip nas listagens
     */
    public function textOccurrenceTooltip($occurrence)
    {
        $text = '';
        if (in_array($occurrence->status, ['ON_SERVICE', 'AWAITING_VALIDATION', 'PAYED_SPECIALIST', 'FINISHED']) && ($occurrence->schedule && $occurrence->checkin)) {
            if ($occurrence->schedule->add(new \DateInterval('PT10M')) < $occurrence->checkin) {
                $text = 'SLA violado.';
            }
        }

        if ($occurrence->status == 'AWAITING_VALIDATION') {
            foreach ($occurrence->historics as $his) {
                if ($his->description == 'AWAITING_VALIDATION') {
                    $now = new \DateTime();
                    if ($now->diff($his->created)->d >= 4) {
                        $text .= ' Cliente pendendo validação a mais de 4 dias.';
                        break;
                    }
                }
            }
        }

        return $text;
    }

    /**
     * Mascasras
     */
    function mask($val, $mask)
    {
        $val = preg_replace("/\D+/", "", $val);
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    /**
     * VAlores de occorrencias em um determinado espaço de tempo
     * @param $contract_id
     * @param $init
     * @param $end
     * @return $this|array
     *
     */
    public function occurrencesExpenseRangeDate($contract_id, $init, $end)
    {
        $Occurrences = TableRegistry::get('Occurrences');

        $end = $end->add(new \DateInterval('P1D'));
        $init = $init->sub(new \DateInterval('P1D'));

        $total = $Occurrences->find('all')->contain('Prices')->where(['contract_id' => $contract_id, 'schedule_time <=' => $end, 'schedule_time >=' => $init]);
        $total = $total->select([
            'count' => $total->func()->count('Occurrences.id'),
            'sum' => $total->func()->sum('Prices.specialist_total_price'),
            'sum_tools' => $total->func()->sum('Prices.occurrence_tools_costs'),
            'sum_displacement' => $total->func()->sum('Prices.occurrence_displacement_costs'),
            'sum_add' => $total->func()->sum('Prices.occurrence_additional_costs'),
        ])->toArray();

        return $total;
    }

    /**
     * Expecialistas na região
     */
    public function specialistsInRegion($state_id, $city_id, $active = 'all')
    {
        if ($active === 'all') {
            $conditions = ['Users.access_specialist' => true, 'city_id' => $city_id, 'state_id' => $state_id];
        } else {
            $conditions = ['Users.access_specialist' => true, 'Users.active' => $active, 'city_id' => $city_id, 'state_id' => $state_id];
        }
        $Users = TableRegistry::get('Individuals');
        $total = $Users->find('all')->contain(['Users', 'Addresses'])->where($conditions);
        $total = $total->select([
            'count' => $total->func()->count('DISTINCT Individuals.id')
        ])->first()->toArray();

        return $total['count'];
    }

    /**
     * Retorna o número de ocorrencias do técnico.
     * @param null $id
     * @return int|null
     */

    public function occourencesBySpecialist($id = null)
    {
        $Occurrences = TableRegistry::get('Occurrences');
        return $total_occurrences = $Occurrences->find('all')->where(['specialist_user_id' => $id])->count();
    }

    /**
     * Diferença em minutos entre duas data
     * @param $checkin
     * @param $checkout
     * @return float|int
     */
    public function diffDates($checkin, $checkout)
    {
        if ($checkin && $checkout) {
            $diff = strtotime($checkout->format('Y-m-d H:i')) - strtotime($checkin->format('Y-m-d H:i'));
            // Minutos
            return $this->convertToHoursMins(floor($diff / (60)), '%02d h %02d m');
        } else {
            return 0;
        }
    }

    /**
     * Diferença em minutos entre duas data
     * @param $checkin
     * @param $checkout
     * @return float|int
     */
    public function getExtraHour($checkin, $checkout, $base_hour)
    {
        if ($checkin && $checkout) {
            $diff = strtotime($checkout->format('Y-m-d H:i')) - strtotime($checkin->format('Y-m-d H:i'));
            // Minutos
            return $this->convertToHoursMins(floor($diff / (60) - $base_hour * 60), '%02d h %02d m');
        } else {
            return 0;
        }
    }

    /**
     * Diferença em minutos entre duas data
     * @param $checkin
     * @param $checkout
     * @return float|int
     */
    public function getIntegerDiffDates($checkin, $checkout)
    {
        if ($checkin && $checkout) {
            $diff = strtotime($checkout->format('Y-m-d H:i')) - strtotime($checkin->format('Y-m-d H:i'));
            // Minutos
            return floor($diff / (60));
        } else {
            return 0;
        }
    }


    /**
     * Diferença em minutos entre duas data
     * @param $checkin
     * @param $checkout
     * @return float|int
     */
    public function getPercentDate($checkin, $checkout, $base_hour)
    {
        if ($checkin && $checkout) {
            $diff = strtotime($checkout->format('Y-m-d H:i')) - strtotime($checkin->format('Y-m-d H:i'));
            // Minutos
            return floor(($diff / (60)) / ($base_hour * 60) * 100);
        } else {
            return 0;
        }
    }


    /**
     * Converter minutos para horas
     * @param $time
     * @param string $format
     * @return string|void
     */
    public function convertToHoursMins($time, $format = '%02d:%02d')
    {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    /**
     * VErificar se o sla está violado, até 15 minutos de atraso não é violado
     * @param $schedule
     * @param $checkin
     * @return bool
     */
    public function slaViolated($schedule, $checkin)
    {
        if ($schedule && $checkin) {
            return ($schedule->add(new \DateInterval('PT15M')) < $checkin);
        } else {
            return false;
        }
    }

    public function phoneFormatted($phone)
    {
        return "<a href='https://web.whatsapp.com/send?phone=55{$phone->number}' target='_black'>
            {$phone->DDI} {$this->mask($phone->number, '(##) #####-####')}</a>";
    }

    public function stringType($type)
    {
        $ar = ['DEPOSIT' => 'Investimento', 'WITHDRAWAL' => 'Retirada', 'OPERATION' => 'Operação', 'OPERATION_EXIT' => 'Operação', 'OPERATION_ENTRY' => 'Operação'];
        return $ar[$type];
    }

    public function badgeType($type)
    {
        $ar = ['DEPOSIT' => 'Deposito', 'WITHDRAWAL' => 'Retirada', 'OPERATION' => 'Operação', 'OPERATION_EXIT' => 'Operação', 'OPERATION_ENTRY' => 'Operação'];

        if ($type == 'DEPOSIT') {
            return "<span class='badge badge-success'>Inventimento</span>";
        } elseif ($type == 'WITHDRAWAL') {
            return "<span class='badge badge-warning'>Retirada</span>";
        } elseif ($type == 'OPERATION_EXIT' || $type == 'OPERATION_ENTRY') {
            return "<span class='badge badge-info'>Operação</span>";
        }
    }
}