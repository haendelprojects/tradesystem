<?php

/**
 * CakePHP 3.x - Acl Manager
 *
 * PHP version 5
 *
 * Class AclHelper
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category CakePHP3
 *
 * @package  AclManager\View\Helper
 *
 * @author Ivan Amat <dev@ivanamat.es>
 * @copyright Copyright 2016, Iván Amat
 * @license MIT http://opensource.org/licenses/MIT
 * @link https://github.com/ivanamat/cakephp3-aclmanager
 */

namespace App\View\Helper;

use App\Model\Table\ChangelogsTable;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\View\Helper;

/**
 * Class UserHelper
 * @package App\View\Helper
 * @property ChangelogsTable $Changelogs
 */
class UserHelper extends Helper
{
    public $helpers = ['Html'];
    private $User = [];
    private $Changelogs;


    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->User = $this->request->session()->read('Auth.User');
        $this->Changelogs = TableRegistry::get('Changelogs');

    }

    /**
     * Buscar ID do usuário
     * @return int
     */
    public function getId()
    {
        return $this->User['id'];
    }

    /**
     * Id da empresa
     * @return mixed
     */
    public function getCorporateId()
    {
        return $this->User['enterprise_id'];
    }

    public function getCorporateName()
    {
        return $this->User['enterprise']['name'];
    }

    /**
     * Buscar nome completo do usuário
     * @return string
     */
    function getName()
    {
        return $this->User['first_name'] . ' ' . $this->User['last_name'];
    }

    function getEmail()
    {
        return $this->User['username'];
    }


    /**
     * Buscar Foto do Usuario
     * @return string
     */
    function getPhoto()
    {
        return ($this->User['individual']['photo']) ? $this->User['individual']['photo'] : 'avatar-2-64.png';
    }

    /**
     * Return RoleId
     * */

    function getRoleId()
    {
        return ($this->User['role_id']);
    }

    /**
     * Verificar se é cliente
     * @return mixed
     */
    public function isClient()
    {
        return $this->User['access_client'];
    }

    /**
     * Verificar se é tecnico
     * @return mixed
     */
    public function isTech()
    {
        return $this->User['access_specialist'];
    }

    /**
     * Informações da Empresa
     * @param null $field
     * @return mixed|string
     */
    public function getEnterprise($field = null)
    {
        if ($field) {
            if (isset($this->User['enterprise'][$field])) {
                return $this->User['enterprise'][$field];
            } else {
                return 'ERROR_FIELD';
            }
        } else {
            return $this->User['enterprise'];
        }
    }

    /**
     * Pegar Informações da tabela individuo
     * @param null $field
     * @return mixed|string
     */
    public function getIndividual($field = null)
    {
        if ($field) {
            if (isset($this->User['individual'][$field])) {
                return $this->User['individual'][$field];
            } else {
                return 'ERROR_FIELD';
            }
        } else {
            return $this->User['individual'];
        }
    }

    /**
     * Verificar se é moderador
     * @return mixed
     */
    public function isModerator()
    {
        return $this->User['moderator'];
    }

    /**
     * Resposta para:
     * deve mostrar ou não o changelog ?
     * @return bool
     */
    public function showChangeLog()
    {
        $changelog = $this->Changelogs->find('all')->last();

        if ($changelog) {

            if ($this->User['changelog_read'] < $changelog->created) {
                return true;
            }
        }

        return false;
    }

    /**
     * Pegar informações do ultimo change log
     * @param $field
     * @return mixed
     */
    public function lastChangelog($field){
        $changelog = $this->Changelogs->find('all')->last();

        return $changelog->{$field};
    }
}