

<?= $this->element('header', [
    'title' =>  'Contracts',
    'menu' => [
        [
            'title' => 'Novo Contracts',
                'itens' => [
                [
                    'url' => ['action' => 'add'],
                    'icon' => 'plus',
                    'text' => 'Adicionar'
                ]
            ]
        ]
    ]
])
?>

<div class="page-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <!-- /.card-header -->
        <div class="card-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('accepted_terms') ?></th>
                <th><?= __('Ações') ?></th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($contracts as $contract): ?>
              <tr>
                <td><?= $this->Number->format($contract->id) ?></td>
                <td><?= h($contract->accepted_terms) ?></td>
                <td>
                  <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $contract->id], ['class'=>'btn btn-info btn-sm']) ?>
                  <?= $this->Html->link(__('Editar'), ['action' => 'edit', $contract->id], ['class'=>'btn btn-warning btn-sm']) ?>
                  <?= $this->Form->postLink(__('Ativar'), ['action' => 'delete', $contract->id], ['confirm' => __('Confirm to delete this entry?'), 'class'=>'btn btn-danger btn-sm']) ?>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <?php echo $this->Paginator->numbers(); ?>
          </ul>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->
