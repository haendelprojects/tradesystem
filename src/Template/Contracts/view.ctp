<?= $this->element('header', [
    'title' => 'Contracts',
])
?>

<section class="page-content container-fluid">
    <div class="row">
        <?= $this->element('menuClient') ?>


        <div class="col-md-9">
            <div class="card card-solid">
                <div class="card-header with-border">
                    <?php echo __('Informações'); ?>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Accepted Terms') ?></dt>
                        <dd>
                            <?= h($contract->accepted_terms) ?>
                        </dd>


                        <dt><?= __('Annex') ?></dt>
                        <dd>
                            <?= $this->Text->autoParagraph(h($contract->annex)); ?>
                        </dd>
                    </dl>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <?= __('Related {0}', ['Users']) ?>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive no-padding">

                    <?php if (!empty($contract->users)): ?>

                        <table class="table table-hover">
                            <tbody>
                            <tr>

                                <th>
                                    Id
                                </th>


                                <th>
                                    First Name
                                </th>


                                <th>
                                    Last Name
                                </th>


                                <th>
                                    Username
                                </th>


                                <th>
                                    Password
                                </th>


                                <th>
                                    Access Admin
                                </th>


                                <th>
                                    Access Client
                                </th>


                                <th>
                                    Active
                                </th>


                                <th>
                                    Contract Id
                                </th>


                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($contract->users as $users): ?>
                                <tr>

                                    <td>
                                        <?= h($users->id) ?>
                                    </td>

                                    <td>
                                        <?= h($users->first_name) ?>
                                    </td>

                                    <td>
                                        <?= h($users->last_name) ?>
                                    </td>

                                    <td>
                                        <?= h($users->username) ?>
                                    </td>

                                    <td>
                                        <?= h($users->password) ?>
                                    </td>

                                    <td>
                                        <?= h($users->access_admin) ?>
                                    </td>

                                    <td>
                                        <?= h($users->access_client) ?>
                                    </td>

                                    <td>
                                        <?= h($users->active) ?>
                                    </td>

                                    <td>
                                        <?= h($users->contract_id) ?>
                                    </td>

                                    <td class="actions">
                                        <?= $this->Html->link(__('Detalhes'), ['controller' => 'Users', 'action' => 'view', $users->id], ['class' => 'btn btn-info btn-sm']) ?>

                                        <?= $this->Html->link(__('Editar'), ['controller' => 'Users', 'action' => 'edit', $users->id], ['class' => 'btn btn-warning btn-sm']) ?>

                                        <?= $this->Form->postLink(__('Remover'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id), 'class' => 'btn btn-danger btn-sm']) ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>

                    <?php endif; ?>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
