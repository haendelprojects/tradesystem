
<?= $this->element('header', [
    'title' =>  'Contracts',
    'subtitle'=>'Edit'
])
?>

<div class="page-content container-fluid">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card">
        <!-- /.card-header -->
        <!-- form start -->
        <?= $this->Form->create($contract, array('role' => 'form')) ?>
          <div class="card-body">
          <?php
            echo $this->Form->input('accepted_terms');
            echo $this->Form->input('annex');
          ?>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <?= $this->Form->button(__('Save')) ?>
          </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>

