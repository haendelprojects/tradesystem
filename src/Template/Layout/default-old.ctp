<?php use Cake\Core\Configure; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TGV Invest - <?= $titlePage ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <?php echo $this->fetch('css'); ?>

    <?= $this->Html->css('build/libs.css'); ?>
    <?= $this->Html->css('build/main.css'); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="webroot/js/validator.min.js"></script>
    <![endif]-->
    <script src="https://www.gstatic.com/firebasejs/3.5.2/firebase.js"></script>


</head>
<body class="hold-transition skin-<?php echo Configure::read('Theme.skin'); ?> sidebar-collapse sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo $this->Url->build('/'); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><?= $this->Html->image('logo.png', ['style' => 'width:80%']) ?></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><?php echo Configure::read('Theme.logo.large'); ?></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <?php echo $this->element('nav-top') ?>
    </header>

    <!-- Left side column. contains the sidebar -->
    <?php echo $this->element('aside-main-sidebar'); ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>

    </div>
    <!-- /.content-wrapper -->
    <?php echo $this->element('footer'); ?>
    <?php echo $this->element('changelogModal'); ?>
    <!-- Control Sidebar -->
    <?php echo $this->element('aside-control-sidebar'); ?>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>


<!-- ./wrapper -->
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
        crossorigin="anonymous"></script>
<script src="https://cdn.firebase.com/libs/geofire/4.1.2/geofire.min.js"></script>
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<!-- SlimScroll -->
<!-- AdminLTE App -->
<?php echo $this->Html->script('build/libs'); ?>
<?php echo $this->Html->script('build/production'); ?>
<!-- AdminLTE for demo purposes -->
<?php echo $this->fetch('scriptBottom'); ?>
<script>
    $('#changelogModal').modal('show');
    var BASE_URL = '<?php echo $this->request->webroot ?>';
    var OneSignal = window.OneSignal || [];
    OneSignal.push(function () {
        OneSignal.init({
            appId: "d754e5b3-bddd-4466-ac66-e200cc64d720",
            autoRegister: true,
            subdomainName: 'admin-findup',

            notifyButton: {
                position: 'bottom-left',
                enable: true,
                prenotify: true, // Show an icon with 1 unread message for first-time site visitors
                showCredit: false, // Hide the OneSignal logo
                text: {
                    'tip.state.unsubscribed': 'Receber notificações',
                    'tip.state.subscribed': "Você já está habilitado para receber notificações",
                    'tip.state.blocked': "Você bloqueou as notificações",
                    'message.prenotify': 'Clique para se receber notificações',
                    'message.action.subscribed': "Obrigado por se inscrever!",
                    'message.action.resubscribed': "Você está recebendo notificações",
                    'message.action.unsubscribed': "Você não receberá notificações",
                    'dialog.main.title': 'Notificações FINDUP',
                    'dialog.main.button.subscribe': 'RECEBER NOTIFICAÇÕES',
                    'dialog.main.button.unsubscribe': 'NÂO RECEBER NOTIFICAÇÕES',
                    'dialog.blocked.title': 'Desbloquear notificações',
                    'dialog.blocked.message': "Siga as intruções para receber notificações:"
                },
            },
            welcomeNotification: {
                "title": "Notificações FindUP",
                "message": "Obrigado por se inscrever!",
            }
        });
        OneSignal.once('subscriptionChange', function (isSubscribed) {
            if (isSubscribed) {
                // Do something if the user is subscribed
            }
        });
    });

    $(document).ready(function () {
        var a = $('a[href="<?php echo $this->request->webroot . $this->request->url ?>"]');
        if (!a.parent().hasClass('treeview')) {
            a.parent().addClass('active').parents('.treeview').addClass('active');
        }
    });
</script>


</body>
</html>
