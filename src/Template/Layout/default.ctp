<html lang="en"
      class=" svg localstorage sessionstorage websqldatabase svgfilters bgpositionshorthand multiplebgs preserve3d inlinesvg csscalc supports svgclippaths svgforeignobject smil no-touchevents fontface svgasimg no-forcetouch matchmedia cssanimations bgpositionxy bgrepeatround bgrepeatspace bgsizecover borderradius no-flexcardtweener csstransforms csstransforms3d csstransitions">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TGV Invest - <?= $titlePage ?></title>
    <!-- ================== GOOGLE FONTS ==================-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
    <!-- ======================= GLOBAL VENDOR STYLES ========================-->
    <link rel="stylesheet" href="/css/vendor/bootstrap.css">

    <link rel="stylesheet" href="/vendor/metismenu/dist/metisMenu.css">
    <link rel="stylesheet" href="/vendor/switchery-npm/index.css">
    <link rel="stylesheet" href="/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="/vendor/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="/vendor/trumbowyg/dist/ui/trumbowyg.css">
    <link rel="stylesheet" href="/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="/vendor/leaflet/dist/leaflet.css">
    <!-- ======================= LINE AWESOME ICONS ===========================-->
    <link rel="stylesheet" href="/css/icons/line-awesome.min.css">
    <!-- ======================= DRIP ICONS ===================================-->
    <link rel="stylesheet" href="/css/icons/dripicons.min.css">
    <!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
    <link rel="stylesheet" href="/css/icons/material-design-iconic-font.min.css">
    <!-- ======================= GLOBAL COMMON STYLES ============================-->
    <link rel="stylesheet" href="/css/common/main.bundle.css">
    <!-- ======================= LAYOUT TYPE ===========================-->
    <link rel="stylesheet" href="/css/layouts/vertical/core/main.css">
    <!-- ======================= MENU TYPE ===========================================-->
    <link rel="stylesheet" href="/css/layouts/vertical/menu-type/default.css">
    <!-- ======================= THEME COLOR STYLES ===========================-->
    <link rel="stylesheet" href="/css/layouts/vertical/themes/theme-a.css">

    <script src="https://www.gstatic.com/firebasejs/3.5.2/firebase.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body class="pace-done mini-sidebar">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
</div>
<!-- CONTENT WRAPPER -->
<div id="app">
    <!-- MENU SIDEBAR WRAPPER -->
    <aside class="sidebar sidebar-left">
        <div class="sidebar-content">
            <div class="aside-toolbar">
                <ul class="site-logo">
                    <li>
                        <!-- START LOGO -->
                        <a href="/">
                            <div class="logo">
                                <img src="/img/logo.jpg" width="32">
                            </div>
                            <span class="brand-text">&nbsp; TGV Invest</span>
                        </a>
                        <!-- END LOGO -->
                    </li>
                </ul>

                <ul class="header-controls" style="display: none;">
                    <li class="nav-item menu-trigger">
                        <button type="button" class="btn btn-link btn-menu" data-toggle-state="mini-sidebar"
                                data-key="leftSideBar">
                            <i class="la la-dot-circle-o"></i>
                        </button>
                    </li>
                </ul>
            </div>
            <?= $this->element('menu'); ?>
        </div>
    </aside>
    <!-- END MENU SIDEBAR WRAPPER -->
    <div class="content-wrapper">
        <?php echo $this->element('nav-top') ?>
        <div class="content">

            <?php echo $this->Flash->render(); ?>
            <?php echo $this->fetch('content'); ?>

        </div>

        <?php echo $this->element('footer'); ?>
    </div>
    <!-- END CONTENT WRAPPER -->
</div>

<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
<script src="/vendor/modernizr/modernizr.custom.js"></script>
<script src="/vendor/jquery/dist/jquery.min.js"></script>
<script src="/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="/vendor/js-storage/js.storage.js"></script>
<script src="/vendor/js-cookie/src/js.cookie.js"></script>
<script src="/vendor/pace/pace.js"></script>
<script src="/vendor/select2/dist/js/select2.full.js"></script>
<script src="/vendor/metismenu/dist/metisMenu.js"></script>
<script src="/vendor/switchery-npm/index.js"></script>
<script src="/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/vendor/trumbowyg/dist/trumbowyg.js"></script>
<script src="/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/vendor/gasparesganga-jquery-loading-overlay/src/loadingoverlay.min.js"></script>
<script src="/vendor/leaflet/dist/leaflet-src.js"></script>
<script src="/vendor/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<script src="/vendor/chart.js/dist/Chart.js"></script>
<!-- ================== GLOBAL APP SCRIPTS ==================-->


<script src="/js/src/select.js"></script>
<script src="/js/global/app.js"></script>

</body>
</html>