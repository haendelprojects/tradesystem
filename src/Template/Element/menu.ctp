<nav class="main-menu">
    <ul class="nav metismenu">
        <?php if($Auth['access_admin']): ?>
        <li>
            <?= $this->Html->link('<i class="fas fa-briefcase"></i> <span>Clientes</span>', ['controller' => 'clients', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
        </li>
        <?php endif; ?>

        <?php if($Auth['access_admin']): ?>
            <li>
                <?= $this->Html->link('<i class="fas fa-user"></i> <span>Usuários</span>', ['controller' => 'users', 'action' => 'index', 'plugin' => false], ['escape' => false]); ?>
            </li>
        <?php endif; ?>

        <?php if($Auth['access_client']): ?>
            <li>
                <?= $this->Html->link('<i class="fas fa-list"></i> <span>Extrato</span>', ['controller' => 'account', 'action' => 'extract', 'plugin' => false], ['escape' => false]); ?>
            </li>
        <?php endif; ?>

        <?php if($Auth['access_client']): ?>
            <li>
                <?= $this->Html->link('<i class="fas fa-cog"></i> <span>Meus Dados</span>', ['controller' => 'account', 'action' => 'information', 'plugin' => false], ['escape' => false]); ?>
            </li>
        <?php endif; ?>
    </ul>
</nav>