<div class="col-md-3">

    <!-- Profile Image -->
    <div class="card">
        <div class="card-body card-profile">

            <div class="text-center">
                <img src="/img/avatar-2-64.png" class="rounded-circle" style="width:100px; height:100px"
                     alt="User profile picture">
                <h3 class="profile-username"><?= $user->first_name ?> <?= $user->lasts_name ?></h3>
                <p class="text-muted"><?= $user->username ?></p>
            </div>
            <p class="text-muted text-center">
            </p>
            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                    <b>Ativo</b><a class="pull-right"><i class="label label-success"> Sim</i></a>
                </li>
            </ul>

            <a href="/clients/edit/<?= $user->id?>" class="btn btn-warning btn-block">Editar Dados</a>

            <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#modal-new-password">
                <i class="fas fa-lock"></i> Alterar Senha
            </button>

            <!-- Modal -->
            <div id="modal-new-password" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <form method="post" accept-charset="utf-8" role="form" action="/clients/edit/12083">
                            <div style="display:none;"><input type="hidden" name="_method" value="PUT"></div>
                            <div class="modal-header">
                                <h5 class="modal-title">Alterar Senha</h5>
                            </div>
                            <div class="modal-body">
                                <div class="card-body">
                                    <div class="form-group password required"><label class="control-label"
                                                                                     for="password">Nova
                                            Senha</label><input type="password" name="password" class="form-control"
                                                                required="required" id="password" value=""
                                                                style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">
                                    Cancelar
                                </button>
                                <button class="btn btn-success" type="submit">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

    <!-- About Me card -->
    <div class="card">
        <div class="card-header with-border">
            <h3 class="card-title">Menu</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body no-padding">
            <div class="nav flex-column nav-pills">
                <a class="nav-link" href="/clients/view/<?= $user->id ?>">
                    <i class="fas fa-user"></i> Informações
                </a>
                <a class="nav-link" href="/extracts/index/<?= $user->id ?>">
                    <i class="fas fa-list"></i> Extrato</a>
            </div>

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>