<div class="alert alert-danger alert-outline alert-dismissible fade show" role="alert"
     style="z-index: 9999; position: absolute; top: 20px; right: 30px;">
    <?= ($message) ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" class="la la-close"></span>
    </button>
</div>