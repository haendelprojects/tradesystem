<?php foreach ($notificationList as $not): ?>
    <div class="timeline-list timeline-border timeline-primary">
        <div class="timeline-info">
            <h5><?= (json_decode($not->vars))->title ?></h5>
            <div><?= (json_decode($not->vars))->body ?></div>
            <small class="text-muted"><?= $this->Util->convertDate($not->created) ?></small>
        </div>
    </div>
<?php endforeach; ?>