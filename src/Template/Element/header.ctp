<?php
$subtitle = (isset($subtitle) ? $subtitle : ''); ?>

<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h1 class="separator"><?= $title ?></h1>
            <nav class="breadcrumb-wrapper" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page"><?= $subtitle ?></li>
                </ol>
            </nav>
        </div>

        <?php if (isset($menu)) : ?>
            <ul class="actions top-right">
                <li class="dropdown">
                    <a href="javascript:void(0)" class="btn btn-fab" data-toggle="dropdown"
                       aria-expanded="false">
                        <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-icon-menu dropdown-menu-right" x-placement="bottom-end">
                        <?php foreach ($menu as $group): ?>
                            <div class="dropdown-header">
                                <?= $group['title'] ?>
                            </div>
                            <?php foreach ($group['itens'] as $item): ?>
                                <?php
                                $options = array_merge((isset($item['options'])) ? $item['options'] : [], [
                                    'escape' => false,
                                    'class' => 'dropdown-item'
                                ]);
                                ?>
                                <?=
                                $this->Html->link("<i class='fas fa-{$item['icon']}'></i>{$item['text']}", $item['url'], $options)
                                ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>
                </li>
            </ul>
        <?php endif; ?>
    </div>
</header>