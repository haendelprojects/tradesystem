<ul class="actions top-right">
    <li>
        <a href="javascript:void(0)" data-q-action="card-collapsed">
            <i class="icon dripicons-chevron-down"></i>
        </a>
    </li>
    <li>
        <a href="javascript:void(0)" data-q-action="card-expand">
            <i class="icon dripicons-expand-2"></i>
        </a>
    </li>

    <li class="dropdown">
        <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
           aria-expanded="true">
            <i class="la la-ellipsis-h"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right animation" x-placement="bottom-end"
             style="position: absolute; transform: translate3d(-232px, 20px, 0px); top: 0px; left: 0px; will-change: transform;">

            <?php foreach ($itens as $item): ?>

                <?php if ($item['type'] == 'modal'): ?>
                    <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal"
                       data-target="<?= $item['target'] ?>">
                        <i class="icon dripicons-<?= $item['icon'] ?>"></i>
                        <?= $item['text'] ?>
                    </a>
                <?php else: ?>
                    <a class="dropdown-item" href="<?= $this->Url->build($item['url']) ?>">
                        <i class="icon dripicons-<?= $item['icon'] ?>"></i>
                        <?= $item['text'] ?>
                    </a>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </li>
</ul>