
<?= $this->element('header', [
    'title' =>  'Configurations',
    'subtitle'=>'Edit'
])
?>

<div class="page-content container-fluid">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card">
        <!-- /.card-header -->
        <!-- form start -->
        <?= $this->Form->create($configuration, array('role' => 'form')) ?>
          <div class="card-body">
          <?php
            echo $this->Form->input('key');
            echo $this->Form->input('value');
            echo $this->Form->input('type');
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
          ?>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <?= $this->Form->button(__('Save')) ?>
          </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>

