

<?= $this->element('header', [
    'title' =>  'Configurations',
    'menu' => [
        [
            'title' => 'Novo Configurations',
                'itens' => [
                [
                    'url' => ['action' => 'add'],
                    'icon' => 'plus',
                    'text' => 'Adicionar'
                ]
            ]
        ]
    ]
])
?>

<div class="page-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <!-- /.card-header -->
        <div class="card-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('key') ?></th>
                <th><?= $this->Paginator->sort('type') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= __('Ações') ?></th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($configurations as $configuration): ?>
              <tr>
                <td><?= $this->Number->format($configuration->id) ?></td>
                <td><?= h($configuration->key) ?></td>
                <td><?= h($configuration->type) ?></td>
                <td><?= $configuration->has('user') ? $this->Html->link($configuration->user->id, ['controller' => 'Users', 'action' => 'view', $configuration->user->id]) : '' ?></td>
                <td>
                  <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $configuration->id], ['class'=>'btn btn-info btn-sm']) ?>
                  <?= $this->Html->link(__('Editar'), ['action' => 'edit', $configuration->id], ['class'=>'btn btn-warning btn-sm']) ?>
                  <?= $this->Form->postLink(__('Ativar'), ['action' => 'delete', $configuration->id], ['confirm' => __('Confirm to delete this entry?'), 'class'=>'btn btn-danger btn-sm']) ?>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <?php echo $this->Paginator->numbers(); ?>
          </ul>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->
