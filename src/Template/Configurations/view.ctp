<?= $this->element('header', [
    'title' =>  'Configurations',
])
?>

<div class="page-content container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card card-solid">
            <div class="card-header with-border">
           <?php echo __('Informações'); ?>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Key') ?></dt>
                                        <dd>
                                            <?= h($configuration->key) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Type') ?></dt>
                                        <dd>
                                            <?= h($configuration->type) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('User') ?></dt>
                                <dd>
                                    <?= $configuration->has('user') ? $configuration->user->id : '' ?>
                                </dd>
                                                                                                
                                            
                                                                                                                                            
                                                                                                                                            
                                            
                                                                        <dt><?= __('Value') ?></dt>
                            <dd>
                            <?= $this->Text->autoParagraph(h($configuration->value)); ?>
                            </dd>
                                                            </dl>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

</section>
