<?php

use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'default';
?>

<section class="content-header">
    <h1>
        <?= h($message) ?> - NOT FOUND
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger">
                <!-- /.card-header -->
                <div class="card-body">
                    <?= __d('cake', 'O endereço requisitado {0} não foi possivel encontrar no servidor.', "<strong>'{$url}'</strong>") ?>
                </div>
            </div>

            <div class="card card-solid">
                <div class="card-body">
                    <?php
                    debug($error);
                    debug('oi');
                    if (Configure::read('debug')):
                        $this->assign('title', $message);
                        $this->assign('templateName', 'error400.ctp');
                        $this->start('file'); ?>
                        <?php if (!empty($error->queryString)) : ?>
                        <p class="notice">
                            <strong>SQL Query: </strong>
                            <?= h($error->queryString) ?>
                        </p>
                    <?php endif; ?>
                        <?php if (!empty($error->params)) : ?>
                        <strong>SQL Query Params: </strong>
                        <?php Debugger::dump($error->params) ?>
                    <?php endif; ?>
                        <?= $this->element('auto_table_warning') ?>
                        <?php
                        if (extension_loaded('xdebug')):
                            xdebug_print_function_stack();
                        endif;

                        $this->end();
                    endif;
                    ?>

                </div>
            </div>
        </div>
</section>

