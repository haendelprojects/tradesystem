<?php

use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'default';
?>

<section class="content-header">
    <h1>
        <?= h($message) ?>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <!-- /.card-header -->
                <div class="card-body">
                    <?= __d('cake', 'O endereço requisitado {0} não foi possivel encontrar no servidor.', "<strong>'{$url}'</strong>") ?>
                </div>
            </div>

            <div class="card card-info">
                <div class="card-body">
                    <div class="text-center">
                        <?php if ($code == 404): ?>
                            <?= $this->Html->image('404.png'); ?>
                        <?php else: ?>
                            <?= $this->Html->image('500.png'); ?>
                        <?php endif; ?>
                    </div>


                    <?php
                    if (Configure::read('debug')):
                        $this->assign('title', $message);
                        $this->assign('templateName', 'error400.ctp');
                        $this->start('file'); ?>
                        <?php if (!empty($error->queryString)) : ?>
                        <p class="notice">
                            <strong>SQL Query: </strong>
                            <?= h($error->queryString) ?>
                        </p>
                    <?php endif; ?>
                        <?php if (!empty($error->params)) : ?>
                        <strong>SQL Query Params: </strong>
                        <?php Debugger::dump($error->params) ?>
                    <?php endif; ?>
                        <?= $this->element('auto_table_warning') ?>
                        <?php
                        if (extension_loaded('xdebug')):
                            xdebug_print_function_stack();
                        endif;

                        $this->end();
                    endif;
                    ?>

                </div>
            </div>
        </div>
</section>

