<?= $this->element('header', [
    'title' => 'Usuários do Sistema',
    'subtitle' => 'Adicionar'
])
?>

<div class="page-content container-fluid">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
                <!-- /.card-header -->
                <!-- form start -->
                <?= $this->Form->create($user, array('role' => 'form')) ?>
                <div class="card-body">
                    <?php
                    echo $this->Form->input('first_name', ['label' => 'Nome']);
                    echo $this->Form->input('last_name', ['label' => 'Sobrenome']);
                    echo $this->Form->input('username', ['label' => 'Emial', 'type' => "email"]);
                    echo $this->Form->input('password', ['label' => 'Senha']);
                    echo $this->Form->input('access_admin', ['value' => true, 'type' => 'hidden']);
                    echo $this->Form->input('access_client', ['value' => false, 'type' => 'hidden']);
                    ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <?= $this->Form->button(__('Salvar')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

