<?= $this->element('header', [
    'title' => 'Usuários Do Sistema',
    'menu' => [
        [
            'title' => 'Novo Usuários',
            'itens' => [
                [
                    'url' => ['action' => 'add'],
                    'icon' => 'plus',
                    'text' => 'Adicionar'
                ]
            ]
        ]
    ]
])
?>

<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>

                            <th><?= $this->Paginator->sort('first_name', 'Nome') ?></th>
                            <th><?= $this->Paginator->sort('username', 'Email') ?></th>
                            <th><?= $this->Paginator->sort('active', 'Ativo') ?></th>
                            <th><?= __('Ações') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td><?= h($user->first_name) ?></td>
                                <td><?= h($user->username) ?></td>
                                <td><?= ($user->active) ? 'Sim' : 'Não' ?></td>
                                <td>
                                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $user->id], ['class' => 'btn btn-info btn-sm']) ?>
                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn btn-warning btn-sm']) ?>
                                    <?php if ($user->active): ?>
                                        <?= $this->Form->postLink(__('Desativar'), ['action' => 'delete', $user->id], ['confirm' => __('Confirma a desativação?'), 'class' => 'btn btn-danger btn-sm']) ?>
                                    <?php else: ?>
                                        <?= $this->Form->postLink(__('Ativar'), ['action' => 'delete', $user->id], ['confirm' => __('Confirma a ativação?'), 'class' => 'btn btn-danger btn-sm']) ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
