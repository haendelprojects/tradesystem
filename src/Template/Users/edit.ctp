
<?= $this->element('header', [
    'title' =>  'Users',
    'subtitle'=>'Edit'
])
?>

<div class="page-content container-fluid">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card">
        <!-- /.card-header -->
        <!-- form start -->
        <?= $this->Form->create($user, array('role' => 'form')) ?>
          <div class="card-body">
          <?php
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name');
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('access_admin');
            echo $this->Form->input('access_client');
            echo $this->Form->input('active');
            echo $this->Form->input('contract_id', ['options' => $contracts]);
          ?>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <?= $this->Form->button(__('Save')) ?>
          </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>

