<?= $this->element('header', [
    'title' => 'Users',
])
?>

<div class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-solid">
                <div class="card-header with-border">
                    <?php echo __('Informações'); ?>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Nome') ?></dt>
                        <dd>
                            <?= h($user->first_name) ?>
                        </dd>
                        <dt><?= __('Sobrenome') ?></dt>
                        <dd>
                            <?= h($user->last_name) ?>
                        </dd>
                        <dt><?= __('Email') ?></dt>
                        <dd>
                            <?= h($user->username) ?>
                        </dd>
                        <dt><?= __('Ativo') ?></dt>
                        <dd>
                            <?= ($user->active) ? 'Sim' : 'Não' ?>
                        </dd>
                    </dl>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->
</div>
