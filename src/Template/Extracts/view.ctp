<?= $this->element('header', [
    'title' =>  'Extracts',
])
?>

<div class="page-content container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card card-solid">
            <div class="card-header with-border">
           <?php echo __('Informações'); ?>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Type') ?></dt>
                                        <dd>
                                            <?= h($extract->type) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('User') ?></dt>
                                <dd>
                                    <?= $extract->has('user') ? $extract->user->id : '' ?>
                                </dd>
                                                                                                
                                            
                                                                                                                                                            <dt><?= __('Cripto Coin') ?></dt>
                                <dd>
                                    <?= $this->Number->format($extract->cripto_coin) ?>
                                </dd>
                                                                                                                <dt><?= __('Real Coin') ?></dt>
                                <dd>
                                    <?= $this->Number->format($extract->real_coin) ?>
                                </dd>
                                                                                                                <dt><?= __('Cripto Now Price Usd') ?></dt>
                                <dd>
                                    <?= $this->Number->format($extract->cripto_now_price_usd) ?>
                                </dd>
                                                                                                                <dt><?= __('Cripto Now Price Real') ?></dt>
                                <dd>
                                    <?= $this->Number->format($extract->cripto_now_price_real) ?>
                                </dd>
                                                                                                
                                                                                                        <dt><?= __('Date') ?></dt>
                                <dd>
                                    <?= h($extract->date) ?>
                                </dd>
                                                                                                                                                        
                                            
                                    </dl>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

</section>
