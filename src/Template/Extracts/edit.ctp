
<?= $this->element('header', [
    'title' =>  'Extracts',
    'subtitle'=>'Edit'
])
?>

<div class="page-content container-fluid">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card">
        <!-- /.card-header -->
        <!-- form start -->
        <?= $this->Form->create($extract, array('role' => 'form')) ?>
          <div class="card-body">
          <?php
            echo $this->Form->input('type');
            echo $this->Form->input('date');
            echo $this->Form->input('cripto_coin');
            echo $this->Form->input('real_coin');
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('cripto_now_price_usd');
            echo $this->Form->input('cripto_now_price_real');
          ?>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <?= $this->Form->button(__('Save')) ?>
          </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>

