<?= $this->element('header', [
    'title' => 'Novo Extrato Cliente',
    'subtitle' => $user->first_name . ' ' . $user->last_name,

])
?>


<section class="page-content container-fluid">
    <div class="row">
        <!-- left column -->
        <?= $this->element('menuClient') ?>
        <div class="col-md-9">
            <!-- general form elements -->
            <div class="card">
                <!-- /.card-header -->
                <!-- form start -->
                <?= $this->Form->create($extract, array('role' => 'form')) ?>
                <div class="card-body">
                    <?php
                    echo $this->Form->input('type', ['label' => 'Tipo de transação', 'value' => $type, 'type' => 'hidden']);

                    echo $this->Form->input('date', [
                        'default' => (new DateTime('now'))->format('d/m/Y'),
                        'class' => 'date datepicker', 'type' => 'text', 'label' => 'Dia da transação'
                    ]);
                    ?>
                    <?php

                    if ($type == 'DEPOSIT') {
                        echo $this->Form->input('real_coin', ['class' => 'invest', 'label' => 'Investimento', 'min' => 0, 'type' => 'number']);
                        echo $this->Form->input('cripto_now_price_real', ['class' => 'value-btc', 'label' => 'Cotação (Real) BTC Momento Compra/Venda', 'min' => 0, 'type' => 'number']);
                        echo $this->Form->input('cripto_coin', ['label' => 'Saldo BTC', 'class' => 'result-invest-btc', 'min' => 0, 'type' => 'number']);
                    }

                    if ($type == 'OPERATION_EXIT') {
                        echo $this->Form->input('cripto_now_price_usd', ['class' => '', 'label' => 'Cotação (Dolar) BTC Momento Compra/Venda', 'min' => 0, 'type' => 'number']);
                        echo $this->Form->input('cripto_coin', ['label' => 'Saldo BTC', 'class' => 'result-invest-btc', 'min' => 0, 'type' => 'number', 'value' => number_format(-1 * $sumExtractBtc, 8), 'disabled']);
                    }

                    if ($type == 'OPERATION_ENTRY') {
                        echo $this->Form->input('cripto_coin', ['class' => 'last-operation', 'min' => 0, 'type' => 'hidden', 'value' => number_format($sumExtractBtc, 8), 'disabled']);
                        echo $this->Form->input('cripto_now_price_usd', ['class' => 'value-dolar-btc', 'label' => 'Cotação (Dolar) BTC Momento Compra/Venda', 'min' => 0, 'type' => 'number']);
                        echo $this->Form->input('cripto_coin', ['label' => 'Saldo BTC', 'class' => 'result-invest-btc', 'min' => 0, 'type' => 'number', 'value' => '']);
                    }
                    if ($type == 'WITHDRAWAL') {
                        echo $this->Form->input('cripto_now_price_usd', ['class' => '', 'label' => 'Cotação (Dolar) BTC Momento Compra/Venda', 'min' => 0, 'type' => 'number']);
                        echo $this->Form->input('cripto_coin', ['label' => 'Saldo BTC', 'class' => 'result-invest-btc', 'min' => 0, 'type' => 'number', 'value' => number_format($sumExtractBtc, 8)]);
                    }
                    ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <?= $this->Form->button(__('Salvar')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

