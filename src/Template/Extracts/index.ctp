<?= $this->element('header', [
    'title' => 'Cliente',
    'subtitle' => $user->first_name . ' ' . $user->last_name,
    'menu' => [
        [
            'title' => 'Nova Ação',
            'itens' => [
                [
                    'url' => ['action' => 'add', $user->id, 'DEPOSIT'],
                    'icon' => 'plus',
                    'text' => 'Adicionar Investimento'
                ],

                [
                    'url' => ['action' => 'add', $user->id, 'WITHDRAWAL'],
                    'icon' => 'less',
                    'text' => 'Retirada'
                ],

                [
                    'url' => ['action' => 'add', $user->id, 'OPERATION_EXIT'],
                    'icon' => 'plus',
                    'text' => 'Nova Operação - Saida'
                ],

                [
                    'url' => ['action' => 'add', $user->id, 'OPERATION_ENTRY'],
                    'icon' => 'plus',
                    'text' => 'Nova Operação - Entrada'
                ]
            ]
        ]
    ]
])
?>

<section class="page-content container-fluid">
    <div class="row">
        <?= $this->element('menuClient') ?>
        <div class="col-md-7">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('real_coin', 'Investimento') ?></th>
                            <th><?= $this->Paginator->sort('date', 'Data') ?></th>
                            <th><?= $this->Paginator->sort('cripto_coin', 'Saldo BTC') ?></th>
                            <th><?= $this->Paginator->sort('dolar_count', 'Cotação BTC Momento da compra/Venda') ?></th>
                            <th><?= $this->Paginator->sort('type', 'Ação') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($extracts as $extract): ?>
                            <tr>
                                <td>R$ <?= $this->Number->format($extract->real_coin) ?></td>
                                <td><?= ($extract->date->format('d/m/Y')) ?></td>
                                <td><?= number_format($extract->cripto_coin, 8) ?></td>
                                <td><?= ($extract->type == 'DEPOSIT') ? 'R$ ' . number_format($extract->cripto_now_price_real, 2, ',', '.') : '$ ' . number_format($extract->cripto_now_price_usd, 2, ',', '.') ?></td>
                                <td><?= $this->Util->badgeType($extract->type) ?></td>

                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-2">
            <div class="card">
                <div class="card-body text-center">
                    <h3>
                        <?= number_format($sumExtractBtc->sum, 8, ',', '.') ?>
                    </h3>
                    <strong>BTC</strong>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <h3>
                        R$ <?= number_format($sumExtractReal->sum, 2, ',', '.') ?>
                    </h3>
                    <strong>Investimento</strong>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <h3>
                        <?= number_format($sumExitBtc->sum, 8, ',', '.') ?>
                    </h3>
                    <strong>Retiradas</strong>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <h3>
                        <?= number_format($sumOperationBtc->sum, 8, ',', '.') ?>
                    </h3>
                    <strong>Saldo Operações</strong>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
