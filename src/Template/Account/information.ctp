<?= $this->element('header', [
    'title' => 'Cliente',
    'subtitle' => $user->first_name . ' ' . h($user->first_name)
])
?>

<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-9">
            <div class="card card-solid">
                <div class="card-header with-border">
                    <?php echo __('Informações'); ?>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <dl class="dl-horizontal">
                        <dt><?= __('Nome') ?></dt>
                        <dd>
                            <?= h($user->first_name) ?> <?= h($user->last_name) ?>
                        </dd>
                        <dt><?= __('Email') ?></dt>
                        <dd>
                            <?= h($user->username) ?>
                        </dd>

                        <dt><?= __('Termos aceitos?') ?></dt>
                        <dd>
                            <?php if ($user->contract) {
                                echo ($user->contract->accepted_terms) ? 'Sim' : 'Não';
                            } ?>
                        </dd>


                        <dt><?= __('Contrato Anexo') ?></dt>
                        <dd>
                            <?php if ($user->contract) : ?>
                                <a href="<?= $user->contract->annex ?>" target="_blank">Visualizar </a>
                            <?php endif; ?>
                        </dd>

                    </dl>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- ./col -->
    </div>
    <!-- div -->
</section>
