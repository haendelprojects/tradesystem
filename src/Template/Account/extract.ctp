<?= $this->element('header', [
    'title' => 'Extrato',
    'subtitle' => $user->first_name . ' ' . $user->last_name
])
?>

<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-9">

            <form class="row" method="get">
                <div class="col-md-3">
                    <select name="year" class="form-control co4">
                        <option value="all" <?= ('all' == $e_year ? 'selected' : '') ?>>Todos
                        </option>
                        <?php for ($i = 2015; $i <= date("Y"); $i++) : ?>
                            <option value="<?= $i ?>" <?= ($i == $e_year ? 'selected' : '') ?>><?= $i ?></option>
                        <?php endfor ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <select class="form-control col-md-" name="month">
                        <option>Escolha o mês</option>
                        <option <?= ('all' == $e_month ? 'selected' : '') ?> value="all">Todos
                        </option>
                        <option <?= (1 == $e_month ? 'selected' : '') ?> value="01">Janeiro
                        </option>
                        <option <?= (2 == $e_month ? 'selected' : '') ?> value="02">Fevereiro
                        </option>
                        <option <?= (3 == $e_month ? 'selected' : '') ?> value="03">Março
                        </option>
                        <option <?= (4 == $e_month ? 'selected' : '') ?> value="04">Abril
                        </option>
                        <option <?= (5 == $e_month ? 'selected' : '') ?> value="05">Maio
                        </option>
                        <option <?= (6 == $e_month ? 'selected' : '') ?> value="06">Junho
                        </option>
                        <option <?= (7 == $e_month ? 'selected' : '') ?> value="07">Julho
                        </option>
                        <option <?= (8 == $e_month ? 'selected' : '') ?> value="08">Agosto
                        </option>
                        <option <?= (9 == $e_month ? 'selected' : '') ?> value="09">Setembro
                        </option>
                        <option <?= (10 == $e_month ? 'selected' : '') ?> value="10">Outubro
                        </option>
                        <option <?= (11 == $e_month ? 'selected' : '') ?> value="11">Novembro
                        </option>
                        <option <?= (12 == $e_month ? 'selected' : '') ?> value="12">Dezembro
                        </option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary col-">Alterar</button>
                </div>

            </form>

            <div class="card">
                <!-- /.card-header -->
                <div class="card-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('real_coin', 'Investimento') ?></th>
                            <th><?= $this->Paginator->sort('date', 'Data') ?></th>
                            <th><?= $this->Paginator->sort('cripto_coin', 'Saldo BTC') ?></th>
                            <th><?= $this->Paginator->sort('dolar_count', 'Cotação BTC Momento da compra/Venda') ?></th>
                            <th><?= $this->Paginator->sort('type', 'Ação') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($extracts as $extract): ?>
                            <tr>
                                <td>R$ <?= $this->Number->format($extract->real_coin) ?></td>
                                <td><?= ($extract->date->format('d/m/Y')) ?></td>
                                <td><?= number_format($extract->cripto_coin, 8) ?></td>
                                <td><?= ($extract->type == 'DEPOSIT') ? 'R$ ' . number_format($extract->cripto_now_price_real, 2, ',', '.') : '$ ' . number_format($extract->cripto_now_price_usd, 2, ',', '.') ?></td>
                                <td><?= $this->Util->badgeType($extract->type) ?></td>

                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>


        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <h3>
                        <?= number_format($sumExtractBtc->sum, 8, ',', '.') ?>
                    </h3>
                    <strong>BTC</strong>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <h3>
                        R$ <?= number_format($sumExtractReal->sum, 2, ',', '.') ?>
                    </h3>
                    <strong>Investimento</strong>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <h3>
                        <?= number_format($sumExitBtc->sum, 8, ',', '.') ?>
                    </h3>
                    <strong>Retiradas</strong>
                </div>
            </div>

            <div class="card">
                <div class="card-body text-center">
                    <h3>
                        <?= number_format($sumOperationBtc->sum, 8, ',', '.') ?>
                    </h3>
                    <strong>Saldo Operações</strong>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- /.content -->
