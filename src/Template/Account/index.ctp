<?= $this->element('header', [
    'title' => 'Clientes',
    'menu' => [
        [
            'title' => 'Clientes',
            'itens' => [
                [
                    'url' => ['action' => 'add'],
                    'icon' => 'plus',
                    'text' => 'Adicionar'
                ]
            ]
        ]
    ]
])
?>

<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('first_name', 'Nome') ?></th>
                            <th><?= $this->Paginator->sort('username', 'Email') ?></th>
                            <th><?= __('Ações') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td><?= h($user->first_name) ?> <?= h($user->last_name) ?></td>
                                <td><?= h($user->username) ?></td>
                                <td>
                                    <?= $this->Html->link(__('Detalhes'), ['action' => 'view', $user->id], ['class' => 'btn btn-info btn-sm']) ?>
                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn btn-warning btn-sm']) ?>
                                    <?= $this->Form->postLink(__('Ativar'), ['action' => 'delete', $user->id], ['confirm' => __('Confirm to delete this entry?'), 'class' => 'btn btn-danger btn-sm']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
