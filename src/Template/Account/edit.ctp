<?= $this->element('header', [
    'title' => 'Clientes',
    'subtitle' => 'Adicionar'
])
?>

<section class="page-content container-fluid">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
                <!-- /.card-header -->
                <!-- form start -->
                <?= $this->Form->create($user, array('role' => 'form')) ?>
                <div class="card-body">
                    <?php
                    echo $this->Form->input('first_name', ['label' => 'Primeiro Nome']);
                    echo $this->Form->input('last_name', ['label' => 'Sobrenome']);

                    echo $this->Form->input('access_admin', ['value' => false, 'type' => 'hidden']);
                    echo $this->Form->input('access_client', ['value' => true, 'type' => 'hidden']);
                    echo $this->Form->input('active', ['value' => true, 'type' => 'hidden']);
                    echo $this->Form->input('contract.accepted_terms', ['label' => 'Cliente aceitou os termos?', 'options' => ['Não', 'Sim']]);
                    echo $this->Form->input('contract.annex', ['label' => 'Link do contrato', 'placeaholder' => 'https://googledrive.com/anaexo']);
                    echo $this->Form->input('username', ['label' => 'Email']);
                    ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <?= $this->Form->button(__('Salvar')) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>

