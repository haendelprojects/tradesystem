<%
use Cake\Utility\Inflector;

$associations += ['BelongsTo' => [], 'HasOne' => [], 'HasMany' => [], 'BelongsToMany' => []];
$immediateAssociations = $associations['BelongsTo'];
$associationFields = collection($fields)
    ->map(function($field) use ($immediateAssociations) {
        foreach ($immediateAssociations as $alias => $details) {
            if ($field === $details['foreignKey']) {
                return [$field => $details];
            }
        }
    })
    ->filter()
    ->reduce(function($fields, $value) {
        return $fields + $value;
    }, []);

$groupedFields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->columnType($field) !== 'binary';
    })
    ->groupBy(function($field) use ($schema, $associationFields) {
        $type = $schema->columnType($field);
        if (isset($associationFields[$field])) {
            return 'string';
        }
        if (in_array($type, ['integer', 'float', 'decimal', 'biginteger'])) {
            return 'number';
        }
        if (in_array($type, ['date', 'time', 'datetime', 'timestamp'])) {
            return 'date';
        }
        return in_array($type, ['text', 'boolean']) ? $type : 'string';
    })
    ->toArray();

$groupedFields += ['number' => [], 'string' => [], 'boolean' => [], 'date' => [], 'text' => []];
$pk = "\$$singularVar->{$primaryKey[0]}";
%>
<?= $this->element('header', [
    'title' =>  '<%= $pluralHumanName %>',
])
?>

<div class="page-content container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card card-solid">
            <div class="card-header with-border">
           <?php echo __('Informações'); ?>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <dl class="dl-horizontal">
                    <% if ($groupedFields['string']) : %>
                        <% foreach ($groupedFields['string'] as $field) : %>
                            <% if (isset($associationFields[$field])) :
                                $details = $associationFields[$field];
                                %>
                                <dt><?= __('<%= Inflector::humanize($details['property']) %>') ?></dt>
                                <dd>
                                    <?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %> : '' ?>
                                </dd>
                            <% else :
                                    if ($field != 'password') :%>
                                        <dt><?= __('<%= Inflector::humanize($field) %>') ?></dt>
                                        <dd>
                                            <?= h($<%= $singularVar %>-><%= $field %>) ?>
                                        </dd>
                                    <% endif; %>
                            <% endif; %>
                        <% endforeach; %>
                    <% endif; %>
                        
                    <% if ($associations['HasOne']) : %>
                        <% foreach ($associations['HasOne'] as $alias => $details) : %>
                            <dt><?= __('<%= Inflector::humanize(Inflector::singularize(Inflector::underscore($alias))) %>') ?></dt>
                            <dd>
                                <?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?>
                            </dd>
                        <% endforeach; %>
                    <% endif; %>
                        
                    <% if ($groupedFields['number']) : %>
                        <% foreach ($groupedFields['number'] as $field) : %>
                            <% if ($field != $primaryKey[0]) :%>
                                <dt><?= __('<%= Inflector::humanize($field) %>') ?></dt>
                                <dd>
                                    <?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?>
                                </dd>
                            <% endif; %>
                        <% endforeach; %>
                    <% endif; %>
                        
                    <% if ($groupedFields['date']) : %>
                        <% foreach ($groupedFields['date'] as $field) : %>
                            <% if (!in_array($field, ['created', 'modified', 'updated'])) : %>
                                <dt><%= "<%= __('" . Inflector::humanize($field) . "') %>" %></dt>
                                <dd>
                                    <?= h($<%= $singularVar %>-><%= $field %>) ?>
                                </dd>
                                <% endif; %>
                        <% endforeach; %>
                    <% endif; %>
                        
                    <% if ($groupedFields['boolean']) : %>
                        <% foreach ($groupedFields['boolean'] as $field) : %>
                            <dt><?= __('<%= Inflector::humanize($field) %>') ?></dt>
                            <dd>
                            <?= $<%= $singularVar %>-><%= $field %> ? __('Yes') : __('No'); ?>
                            </dd>
                        <% endforeach; %>
                    <% endif; %>
                        
                    <% if ($groupedFields['text']) : %>
                        <% foreach ($groupedFields['text'] as $field) : %>
                            <dt><?= __('<%= Inflector::humanize($field) %>') ?></dt>
                            <dd>
                            <?= $this->Text->autoParagraph(h($<%= $singularVar %>-><%= $field %>)); ?>
                            </dd>
                        <% endforeach; %>
                    <% endif; %>
                </dl>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

<%
$relations = $associations['HasMany'] + $associations['BelongsToMany'];
foreach ($relations as $alias => $details):
    $otherSingularVar = Inflector::variable($alias);
    $otherPluralHumanName = Inflector::humanize(Inflector::underscore($details['controller']));
    %>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                   <?= __('Related {0}', ['<%= $otherPluralHumanName %>']) ?>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive no-padding">

                <?php if (!empty($<%= $singularVar %>-><%= $details['property'] %>)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <% foreach ($details['fields'] as $field): %>
                                    <% if (in_array($field, ['created', 'modified'])) { continue; } %>

                                    <th>
                                    <%= Inflector::humanize($field) %>
                                    </th>
                                        
                                <% endforeach; %>
                                    
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($<%= $singularVar %>-><%= $details['property'] %> as $<%= $otherSingularVar %>): ?>
                                <tr>
                                    <% foreach ($details['fields'] as $field): %>
                                    <% if (in_array($field, ['created', 'modified'])) { continue; } %>

                                    <td>
                                    <?= h($<%= $otherSingularVar %>-><%= $field %>) ?>
                                    </td>
                                    <% endforeach; %>

                                    <% $otherPk = "\${$otherSingularVar}->{$details['primaryKey'][0]}"; %>
                                    <td class="actions">
                                    <?= $this->Html->link(__('Detalhes'), ['controller' => '<%= $details['controller'] %>', 'action' => 'view', <%= $otherPk %>], ['class'=>'btn btn-info btn-sm']) %>
                                    <?= $this->Html->link(__('Editar'), ['controller' => '<%= $details['controller'] %>', 'action' => 'edit', <%= $otherPk %>], ['class'=>'btn btn-warning btn-sm']) %>
                                    <?= $this->Form->postLink(__('Remover'), ['controller' => '<%= $details['controller'] %>', 'action' => 'delete', <%= $otherPk %>], ['confirm' => __('Are you sure you want to delete # {0}?', <%= $otherPk %>), 'class'=>'btn btn-danger btn-sm']) %>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
<% endforeach; %>
</section>
