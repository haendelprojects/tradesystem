<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\ExtractsTable;
use App\Model\Table\UsersTable;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 * @property ExtractsTable $Extracts
 */
class AccountController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'add']);
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
        $this->set('titlePage', 'Usuários');

        $this->loadModel('Users');
        $this->loadModel('Extracts');
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function extract()
    {

        $conditions = [
            'user_id' => $this->Auth->user('id')
        ];

        // Filtro Extrato
        $e_year = ($this->request->getQuery('year')) ? $this->request->getQuery('year') : date('Y');
        $e_month = ($this->request->getQuery('month')) ? $this->request->getQuery('month') : date('m');

        if ($e_year != 'all') {
            $conditions['YEAR(Extracts.date)'] = $e_year;
        }

        if ($e_month != 'all') {
            $conditions['MONTH(Extracts.date)'] = $e_month;
        }


        $extracts = $this->paginate(
            $this->Extracts->find('all')
                ->where($conditions));

        // BTC
        $sumExtractBtc = $this->Extracts->find('all')->where(['user_id' => $this->Auth->user('id')]);
        $sumExtractBtc = $sumExtractBtc->select(['sum' => $sumExtractBtc->func()->sum('cripto_coin')])->first();
        // RETIRADAS
        $sumExitBtc = $this->Extracts->find('all')->where(['user_id' => $this->Auth->user('id'), 'type' => 'WITHDRAWAL']);
        $sumExitBtc = $sumExitBtc->select(['sum' => $sumExitBtc->func()->sum('cripto_coin')])->first();
        // Operções
        $sumOperationBtc = $this->Extracts->find('all')->where(['user_id' => $this->Auth->user('id'), 'type IN' => ['OPERATION_EXIT', 'OPERATION_ENTRY']]);
        $sumOperationBtc = $sumOperationBtc->select(['sum' => $sumOperationBtc->func()->sum('cripto_coin')])->first();
        // REAL
        $sumExtractReal = $this->Extracts->find('all')->where(['user_id' => $this->Auth->user('id')]);
        $sumExtractReal = $sumExtractReal->select(['sum' => $sumExtractReal->func()->sum('real_coin')])->first();

        $user = $this->Extracts->Users->get($this->Auth->user('id'));
        $this->set(compact('extracts', 'user', 'sumExtractBtc', 'sumExtractReal', 'e_year', 'e_month', 'sumExitBtc', 'sumOperationBtc'));
        $this->set('_serialize', ['extracts']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function information()
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['Contracts', 'Configurations', 'Extracts'],
            'conditions' => [
                'access_client' => true
            ]
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }
}
