<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login','add']);

        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);
        $this->set('titlePage', 'Usuários');


//        if (!$this->Auth->user('access_admin')) {
//            if (!in_array($this->request->action, ['login', 'logout'])) {
//                $this->Flash->error('Você não tem acesso a este recurso');
//                $this->redirect(['controller' => 'dashboard']);
//            }
//        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Contracts']
        ];
        $users = $this->paginate($this->Users->find('all')
            ->where([
                'access_admin' => true
            ]));

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Contracts', 'Configurations', 'Extracts'],
            'conditions' => [
                'access_admin' => true
            ]
        ]);


        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The {0} has been saved.', 'User'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'User'));
            }
        }
        $contracts = $this->Users->Contracts->find('list', ['limit' => 200]);
        $this->set(compact('user', 'contracts'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
            'conditions' => [
                'access_admin' => true
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The {0} has been saved.', 'User'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'User'));
            }
        }
        $contracts = $this->Users->Contracts->find('list', ['limit' => 200]);
        $this->set(compact('user', 'contracts'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $user = $this->Users->get($id, [
            'conditions' => [
                'access_admin' => true
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $active = ($user->active) ? false : true;
            $user = $this->Users->patchEntity($user, ['active' => $active]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Salvo'));
            } else {
                $this->Flash->error(__('Falha, tente novamente'));
            }
        }
        return $this->redirect($this->referer());
    }

    public function login()
    {
        $this->viewBuilder()->setLayout('login');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('Email ou senha incorretos.'));
            }
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
