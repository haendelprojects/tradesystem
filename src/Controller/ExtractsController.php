<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Extracts Controller
 *
 * @property \App\Model\Table\ExtractsTable $Extracts
 *
 * @method \App\Model\Entity\Extract[] paginate($object = null, array $settings = [])
 */
class ExtractsController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        if (!$this->Auth->user('access_admin')) {
            $this->Flash->error('Você não tem acesso a este recurso');
            $this->redirect(['controller' => 'dashboard']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($userId = null)
    {
        $extracts = $this->paginate(
            $this->Extracts->find('all')
                ->where([
                    'user_id' => $userId
                ]));

        // BTC
        $sumExtractBtc = $this->Extracts->find('all')->where(['user_id' => $userId]);
        $sumExtractBtc = $sumExtractBtc->select(['sum' => $sumExtractBtc->func()->sum('cripto_coin')])->first();
        // RETIRADAS
        $sumExitBtc = $this->Extracts->find('all')->where(['user_id' => $userId, 'type' => 'WITHDRAWAL']);
        $sumExitBtc = $sumExitBtc->select(['sum' => $sumExitBtc->func()->sum('cripto_coin')])->first();
        // Operções
        $sumOperationBtc = $this->Extracts->find('all')->where(['user_id' => $userId, 'type IN' => ['OPERATION_EXIT', 'OPERATION_ENTRY']]);
        $sumOperationBtc = $sumOperationBtc->select(['sum' => $sumOperationBtc->func()->sum('cripto_coin')])->first();
        // REAL
        $sumExtractReal = $this->Extracts->find('all')->where(['user_id' => $userId]);
        $sumExtractReal = $sumExtractReal->select(['sum' => $sumExtractReal->func()->sum('real_coin')])->first();

        $user = $this->Extracts->Users->get($userId);
        $this->set(compact('extracts', 'user', 'sumExtractBtc', 'sumExtractReal', 'sumExitBtc', 'sumOperationBtc'));
        $this->set('_serialize', ['extracts']);
    }

    /**
     * View method
     *
     * @param string|null $id Extract id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $extract = $this->Extracts->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('extract', $extract);
        $this->set('_serialize', ['extract']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($userId, $type = 'DEPOSIT')
    {
        $extract = $this->Extracts->newEntity();

        if ($type == 'OPERATION_EXIT' || $type == 'WITHDRAWAL') {
            $sumExtractBtc = $this->Extracts->find('all')->where(['user_id' => $userId]);
            $sumExtractBtc = ($sumExtractBtc->select(['sum' => $sumExtractBtc->func()->sum('cripto_coin')])->first())->sum;
        }

        if ($type == 'OPERATION_ENTRY') {
            $lastExtract = $this->Extracts->find('all')->where(['user_id' => $userId])->last();
            $sumExtractBtc = (-1 * $lastExtract->cripto_coin) * $lastExtract->cripto_now_price_usd;
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['date'] = \DateTime::createFromFormat('d/m/Y', $data['date']);
            $data['user_id'] = $userId;

            if ($type == 'OPERATION_EXIT') {
                $data['cripto_coin'] = number_format(-1 * $sumExtractBtc, 8);
            }

            if ($type == 'WITHDRAWAL') {
                $data['cripto_coin'] = $this->request->getData('cripto_coin') * -1;
            }

            $extract = $this->Extracts->patchEntity($extract, $data);

            if ($this->Extracts->save($extract)) {
                $this->Flash->success(__('Salvo.', 'Extract'));
                return $this->redirect(['action' => 'index', $userId, 'controller' => 'extracts']);
            } else {
                $this->Flash->error(__('Falha, tente novamente.', 'Extract'));
            }
        }


        $user = $this->Extracts->Users->get($userId);
        $this->set(compact('extract', 'user', 'type', 'sumExtractBtc'));
        $this->set('_serialize', ['extract']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Extract id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $extract = $this->Extracts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $extract = $this->Extracts->patchEntity($extract, $this->request->data);
            if ($this->Extracts->save($extract)) {
                $this->Flash->success(__('The {0} has been saved.', 'Extract'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Extract'));
            }
        }
        $users = $this->Extracts->Users->find('list', ['limit' => 200]);
        $this->set(compact('extract', 'users'));
        $this->set('_serialize', ['extract']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Extract id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $extract = $this->Extracts->get($id);
        if ($this->Extracts->delete($extract)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Extract'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Extract'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
